# Registro de Usuario
	curl -v -H 'Content-Type: application/json' -H 'Accept: application/json' -X POST http://0.0.0.0:3000/api/v1/auth -d '{"name": "admin","nickname": "admin","email": "admin@g.com","password": "12345678","password_confirmation": "12345678"}'

# Login Usuario
	curl -v -H 'Content-Type: application/json' -H 'Accept: application/json' -X POST http://localhost:3000/api/v1/auth/sign_in -d '{"nickname": "admin","password": "12345678"}'

	Return: 
		{
			"data":{
				"id":1,
				"email":"email@g.com",
				"provider":"email",
				"uid":"email@g.com",
				"allow_password_change":false,
				"nombre":null,
				"username":null,
				"idUserCreador":null,"image":null
			}
		}

# Para el envio del token
	curl -v -H 'Content-Type: application/json' -H 'Accept: application/json' -H 'access-token: EWm0Wi0PaXOF0ta6JuEOhQ' -H 'token-type: Bearer' -H 'client: ezDnOUfwFxYSpQW391-v4w' -H 'expiry: 1539474543' -H 'uid: email@g.com' -X GET http://0.0.0.0:3000/api/v1/usuarios/list

	header:{
		access-token: EWm0Wi0PaXOF0ta6JuEOhQ,
		token-type: Bearer,
		client: ezDnOUfwFxYSpQW391-v4w,
		expiry: 1539474543,
		uid: email@g.com
	}



# instalar gema pg mediante la instalacion local
sudo gem install pg -- --with-pg-config=/Library/PostgreSQL/10/bin/pg_config
sudo gem install pg -- --with-pg-include=/Library/PostgreSQL/10/include



curl -v -H 'Content-Type: application/json' -H 'Accept: application/json' -X POST http://0.0.0.0:1337/api/v1/recibos -d '{"usuario_id": 1, "concepto": "pagos facturas", "monto": 375, "id_referencia": 1, "tipo": "G", "estado": "A"}'


# poner ceros a las izquierda
("%08d" % 5)