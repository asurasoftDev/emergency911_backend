class RemoveCausasFromEmergencias < ActiveRecord::Migration[5.2]
  def change
    remove_reference :emergencia, :causa, foreign_key: true
  end
end
