class CreateExploraciones < ActiveRecord::Migration[5.2]
  def change
    create_table :exploraciones do |t|
      t.string :nombre
      t.string :tipo
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
