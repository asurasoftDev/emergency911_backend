class AddHorasAcumuladasToEmpelado < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :horas_acumuladas, :time
  	add_column :users, :horas_sobrantes, :time
  end
end
