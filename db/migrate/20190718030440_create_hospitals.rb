class CreateHospitals < ActiveRecord::Migration[5.2]
  def change
    create_table :hospitals do |t|
      t.string :nombre
      t.references :ciudad, foreign_key: true
      t.boolean :activo, :default => true
      t.boolean :tipo_publico, :default => true
      t.timestamps
    end
    add_reference :ficha_clinicas, :hospitals, foreign_key: true
  end
end
