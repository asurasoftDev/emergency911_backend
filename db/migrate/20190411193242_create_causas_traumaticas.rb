class CreateCausasTraumaticas < ActiveRecord::Migration[5.2]
  def change
    create_table :causas_traumaticas do |t|
      t.references :trauma, foreign_key: true
      t.references :ficha_clinica, foreign_key: true
      t.string :descripcion
      t.string :causado_por
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
