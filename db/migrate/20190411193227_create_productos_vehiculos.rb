class CreateProductosVehiculos < ActiveRecord::Migration[5.2]
  def change
    create_table :productos_vehiculos do |t|
      t.references :vehiculo, foreign_key: true
      t.references :producto, foreign_key: true
      t.integer :cant_minima
      t.integer :cantidad
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
