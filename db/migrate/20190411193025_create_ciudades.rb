class CreateCiudades < ActiveRecord::Migration[5.2]
  def change
    create_table :ciudades do |t|
      t.string :nombre
      t.boolean :activo , :default => true

      t.timestamps
    end
  end
end
