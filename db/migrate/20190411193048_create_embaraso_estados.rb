class CreateEmbarasoEstados < ActiveRecord::Migration[5.2]
  def change
    create_table :embaraso_estados do |t|
      t.string :nombre
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
