class CreateTurnosVehiculos < ActiveRecord::Migration[5.2]
  def change
    create_table :turnos_vehiculos do |t|
      t.references :vehiculo, foreign_key: true
      t.references :turno, foreign_key: true
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
