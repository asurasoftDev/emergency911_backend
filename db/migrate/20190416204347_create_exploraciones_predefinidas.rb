class CreateExploracionesPredefinidas < ActiveRecord::Migration[5.2]
  def change
    create_table :exploraciones_predefinidas do |t|
      t.references :area_fisica, foreign_key: true
      t.references :exploracion, foreign_key: true
      t.boolean :activo

      t.timestamps
    end
  end
end
