class CreatePermisos < ActiveRecord::Migration[5.2]
  def change
    create_table :permisos do |t|
      t.string :nombre, unique: true
      t.boolean :activo, :default => true

      t.timestamps
    end
    add_index :permisos, :nombre, unique: true
  end
end
