class RemoveAreFisicaToExploracionesResultados < ActiveRecord::Migration[5.2]
  def change
    remove_reference :exploraciones_resultados, :area_fisica, foreign_key: true
    remove_reference :exploraciones_resultados, :exploracion, foreign_key: true
    add_reference :exploraciones_resultados, :exploracion_predefinida, foreign_key: true
  end
end
