class CreateEstadosObstetricos < ActiveRecord::Migration[5.2]
  def change
    create_table :estados_obstetricos do |t|
      t.references :obstetrico, foreign_key: true
      t.references :embaraso_estado, foreign_key: true
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
