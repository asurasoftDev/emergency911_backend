class AddValueToSignosEncontrados < ActiveRecord::Migration[5.2]
  def change
    add_column :signos_encontrados, :value, :string
  end
end
