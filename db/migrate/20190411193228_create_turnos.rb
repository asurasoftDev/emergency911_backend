class CreateTurnos < ActiveRecord::Migration[5.2]
  def change
    create_table :turnos do |t|
      t.references :user, foreign_key: true
      t.references :tanda, foreign_key: true
      t.boolean :activo, :default => true

      # t.references :vehiculo, foreign_key: true
      
      t.timestamps
    end
  end
end
