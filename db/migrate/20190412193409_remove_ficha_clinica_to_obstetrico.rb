class RemoveFichaClinicaToObstetrico < ActiveRecord::Migration[5.2]
  def change
  	# remove_reference :obstetricos, :ficha_clinica, foreign_key: true
  	add_reference :estados_obstetricos, :ficha_clinica, foreign_key: true 
  end
end
