class AddCodigoToEmergencia < ActiveRecord::Migration[5.2]
  def change
    add_column :emergencia, :codigo, :string
    add_column :emergencia, :estado, :string, :default => 'C'
  end
end
