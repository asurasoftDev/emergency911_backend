class CreateTandas < ActiveRecord::Migration[5.2]
  def change
    create_table :tandas do |t|
      t.references :cargo, foreign_key: true
      t.references :dia, foreign_key: true
      t.time :hora_inicio
      t.time :hora_fin
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
