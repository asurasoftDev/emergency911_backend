class AddCantUtilizadaToProductosConsumidos < ActiveRecord::Migration[5.2]
  def change
    add_column :productos_consumidos, :cant_utilizada, :integer
  end
end