class CreateOringesProbables < ActiveRecord::Migration[5.2]
  def change
    create_table :oringes_probables do |t|
      t.references :causa_clinica, foreign_key: true
      t.references :ficha_clinica, foreign_key: true
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
