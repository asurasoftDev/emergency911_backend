class CreateProductos < ActiveRecord::Migration[5.2]
  def change
    create_table :productos do |t|
      t.string :nombre
      t.integer :cant_minima
      t.boolean :activo, :default => true
      t.integer :cantidad, :default => 0

      t.timestamps
    end
  end
end
