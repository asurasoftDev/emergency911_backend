class CreateCompras < ActiveRecord::Migration[5.2]
  def change
    create_table :compras do |t|
      t.float :costo
      t.integer :cantidad, :default => 0
      t.references :producto, foreign_key: true

      t.timestamps
    end
  end
end
