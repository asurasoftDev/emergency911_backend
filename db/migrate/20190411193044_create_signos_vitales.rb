class CreateSignosVitales < ActiveRecord::Migration[5.2]
  def change
    create_table :signos_vitales do |t|
      t.string :nombre
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
