class CreateObstetricos < ActiveRecord::Migration[5.2]
  def change
    create_table :obstetricos do |t|
      # t.references :ficha_clinica, foreign_key: true
      t.string :nombre
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
