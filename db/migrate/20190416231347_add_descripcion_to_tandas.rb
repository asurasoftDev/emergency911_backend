class AddDescripcionToTandas < ActiveRecord::Migration[5.2]
  def change
    add_column :tandas, :descripcion, :string
  end
end
