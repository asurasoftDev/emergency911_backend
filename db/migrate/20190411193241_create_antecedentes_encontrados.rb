class CreateAntecedentesEncontrados < ActiveRecord::Migration[5.2]
  def change
    create_table :antecedentes_encontrados do |t|
      t.references :antecedente, foreign_key: true
      t.references :ficha_clinica, foreign_key: true
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
