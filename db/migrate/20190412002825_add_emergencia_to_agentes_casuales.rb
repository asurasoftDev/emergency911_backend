class AddEmergenciaToAgentesCasuales < ActiveRecord::Migration[5.2]
  def change
    add_reference :agentes_casuales, :emergencia, foreign_key: true 
  end
end
