class CreateVehiculosFichasHistoriales < ActiveRecord::Migration[5.2]
  def change
    create_table :vehiculos_fichas_historiales do |t|
      t.references :ficha_clinica, foreign_key: true
      # t.references :turno_vehiculo, foreign_key: true
      # t.references :turno, foreign_key: true
      t.boolean :activo, :default => true
      t.boolean :entregado_completado, :default => false
      t.float :km_inicial_vehiculo, :default => 0.0
      t.float :km_final_vehiculo, :default => 0.0

      t.timestamps
    end
  end
end
