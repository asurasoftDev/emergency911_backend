class RenameOldProductosCosumidosToProductosConsumidos < ActiveRecord::Migration[5.2]
  def change
    rename_table :productos_cosumidos, :productos_consumidos
  end
end
