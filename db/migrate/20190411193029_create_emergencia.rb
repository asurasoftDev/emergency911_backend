class CreateEmergencia < ActiveRecord::Migration[5.2]
  def change
    create_table :emergencia do |t|
      t.references :ciudad, foreign_key: true
      t.string :sector
      t.string :lugar_del_evento
      t.string :descripcion
      t.string :telefono_infomante
      t.float :lat, :null =>  false
      t.float :long, :null =>  false
      t.references :causa, foreign_key: true
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
