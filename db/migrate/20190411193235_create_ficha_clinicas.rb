class CreateFichaClinicas < ActiveRecord::Migration[5.2]
  def change
    create_table :ficha_clinicas do |t|
      t.references :persona, foreign_key: true
      t.references :emergencia, foreign_key: true
      # t.string :hospital_destino
      t.string :medico_hospital
      t.string :diagnostico_presuntivo
      t.string :observacion
      t.string :triage_color
      t.string :estado
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
