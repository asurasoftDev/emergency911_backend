class CreateEmergenciaTipos < ActiveRecord::Migration[5.2]
  def change
    create_table :emergencia_tipos do |t|
      t.string :nombre
      t.string :descripcion
      t.boolean :activo

      t.timestamps
    end
  end
end
