class CreateCargos < ActiveRecord::Migration[5.2]
  def change
    create_table :cargos do |t|
      t.string :nombre
      t.integer :valor_del_dia_en_horas
      t.integer :cant_dias_libres
      t.boolean :activo, :default => true

      t.timestamps
    end
    add_index :cargos, :nombre, unique: true
  end
end
