class CreatePermisoAcciones < ActiveRecord::Migration[5.2]
  def change
    create_table :permiso_acciones do |t|
      t.string :nombre, :null =>  false
      t.boolean :activo, :default => true
      t.references :permiso, foreign_key: true, null: false

      t.timestamps
    end
  end
end
