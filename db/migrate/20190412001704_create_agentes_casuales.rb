class CreateAgentesCasuales < ActiveRecord::Migration[5.2]
  def change
    create_table :agentes_casuales do |t|
      t.references :ficha_clinica, foreign_key: true
      t.references :causa, foreign_key: true
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
