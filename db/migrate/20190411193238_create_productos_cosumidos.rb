class CreateProductosCosumidos < ActiveRecord::Migration[5.2]
  def change
    create_table :productos_cosumidos do |t|
      t.references :producto_vehiculo, foreign_key: true
      t.references :vehiculo_ficha_historial, foreign_key: true
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
