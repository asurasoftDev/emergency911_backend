class CreateExploracionesResultados < ActiveRecord::Migration[5.2]
  def change
    create_table :exploraciones_resultados do |t|
      t.references :exploracion, foreign_key: true
      t.references :area_fisica, foreign_key: true
      t.references :ficha_clinica, foreign_key: true
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
