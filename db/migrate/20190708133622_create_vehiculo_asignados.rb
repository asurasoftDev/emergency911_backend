class CreateVehiculoAsignados < ActiveRecord::Migration[5.2]
  def change
    remove_reference :ficha_clinicas, :emergencia, foreign_key: true
    create_table :vehiculo_asignados do |t|
      t.references :turnos_vehiculo, foreign_key: true
      t.references :emergencia, foreign_key: true
      t.boolean :activo, :default => true
      
      t.timestamps
    end
    add_reference :ficha_clinicas, :vehiculo_asignado, foreign_key: true
  end
end
