class AddUsersToCargos < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :cargo, foreign_key: true
    add_reference :users, :persona, foreign_key: true , unique:true
  end
end
