class CreatePersonas < ActiveRecord::Migration[5.2]
  def change
    create_table :personas do |t|
      t.references :ciudad, foreign_key: true
      t.string :nombres
      t.string :apellidos
      t.string :cedula
      t.integer :edad
      t.string :sexo
      t.string :telefono_1
      t.string :telefono_2
      t.string :numero_de_seguro
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
