class AddEmergeciaTipoToEmergencia < ActiveRecord::Migration[5.2]
  def change
    add_reference :emergencia, :emergencia_tipo, foreign_key: true
  end
end
