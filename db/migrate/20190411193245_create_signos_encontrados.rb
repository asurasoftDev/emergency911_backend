class CreateSignosEncontrados < ActiveRecord::Migration[5.2]
  def change
    create_table :signos_encontrados do |t|
      t.references :signo_vital, foreign_key: true
      t.references :ficha_clinica, foreign_key: true
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
