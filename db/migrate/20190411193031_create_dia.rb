class CreateDia < ActiveRecord::Migration[5.2]
  def change
    create_table :dia do |t|
      t.string :nombre
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
