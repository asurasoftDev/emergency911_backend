class CreatePacientesPertenencia < ActiveRecord::Migration[5.2]
  def change
    create_table :pacientes_pertenencia do |t|
      t.references :ficha_clinica, foreign_key: true
      t.string :quien_recibe
      t.string :descripcion
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
