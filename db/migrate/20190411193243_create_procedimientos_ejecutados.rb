class CreateProcedimientosEjecutados < ActiveRecord::Migration[5.2]
  def change
    create_table :procedimientos_ejecutados do |t|
      t.references :ficha_clinica, foreign_key: true
      t.references :procedimiento, foreign_key: true
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
