class CreateRolPermisoAcciones < ActiveRecord::Migration[5.2]
  def change
    create_table :rol_permiso_acciones do |t|
      t.boolean :activo, :default => true
      t.references :permiso_accione, foreign_key: true, null: false
      t.references :role, foreign_key: true, null: false

      t.timestamps
    end
  end
end
