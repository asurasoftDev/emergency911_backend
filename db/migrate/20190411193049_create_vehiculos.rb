class CreateVehiculos < ActiveRecord::Migration[5.2]
  def change
    create_table :vehiculos do |t|
      t.string :marca
      t.string :modelo
      t.string :placa
      t.string :anio
      t.string :alias
      t.datetime :ultimo_restablecimiento_de_invetario
      t.string :estado
      t.boolean :activo, :default => true

      t.timestamps
    end
  end
end
