# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_25_134426) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "antecedentes", force: :cascade do |t|
    t.string "nombre"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "antecedentes_encontrados", force: :cascade do |t|
    t.bigint "antecedente_id"
    t.bigint "ficha_clinica_id"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["antecedente_id"], name: "index_antecedentes_encontrados_on_antecedente_id"
    t.index ["ficha_clinica_id"], name: "index_antecedentes_encontrados_on_ficha_clinica_id"
  end

  create_table "areas_fisicas", force: :cascade do |t|
    t.string "nombre"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cargos", force: :cascade do |t|
    t.string "nombre"
    t.integer "valor_del_dia_en_horas"
    t.integer "cant_dias_libres"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nombre"], name: "index_cargos_on_nombre", unique: true
  end

  create_table "causas_clinicas", force: :cascade do |t|
    t.string "nombre"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "causas_traumaticas", force: :cascade do |t|
    t.bigint "trauma_id"
    t.bigint "ficha_clinica_id"
    t.string "descripcion"
    t.string "causado_por"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ficha_clinica_id"], name: "index_causas_traumaticas_on_ficha_clinica_id"
    t.index ["trauma_id"], name: "index_causas_traumaticas_on_trauma_id"
  end

  create_table "ciudades", force: :cascade do |t|
    t.string "nombre"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "compras", force: :cascade do |t|
    t.float "costo"
    t.integer "cantidad", default: 0
    t.bigint "producto_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["producto_id"], name: "index_compras_on_producto_id"
    t.index ["user_id"], name: "index_compras_on_user_id"
  end

  create_table "dia", force: :cascade do |t|
    t.string "nombre"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "alias"
  end

  create_table "embaraso_estados", force: :cascade do |t|
    t.string "nombre"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "emergencia", force: :cascade do |t|
    t.bigint "ciudad_id"
    t.string "sector"
    t.string "lugar_del_evento"
    t.string "descripcion"
    t.string "telefono_infomante"
    t.float "lat", null: false
    t.float "long", null: false
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "codigo"
    t.string "estado", default: "C"
    t.bigint "emergencia_tipo_id"
    t.index ["ciudad_id"], name: "index_emergencia_on_ciudad_id"
    t.index ["emergencia_tipo_id"], name: "index_emergencia_on_emergencia_tipo_id"
  end

  create_table "emergencia_tipos", force: :cascade do |t|
    t.string "nombre"
    t.string "descripcion"
    t.boolean "activo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "estados_obstetricos", force: :cascade do |t|
    t.bigint "obstetrico_id"
    t.bigint "embaraso_estado_id"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "ficha_clinica_id"
    t.index ["embaraso_estado_id"], name: "index_estados_obstetricos_on_embaraso_estado_id"
    t.index ["ficha_clinica_id"], name: "index_estados_obstetricos_on_ficha_clinica_id"
    t.index ["obstetrico_id"], name: "index_estados_obstetricos_on_obstetrico_id"
  end

  create_table "exploraciones", force: :cascade do |t|
    t.string "nombre"
    t.string "tipo"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "exploraciones_predefinidas", force: :cascade do |t|
    t.bigint "area_fisica_id"
    t.bigint "exploracion_id"
    t.boolean "activo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["area_fisica_id"], name: "index_exploraciones_predefinidas_on_area_fisica_id"
    t.index ["exploracion_id"], name: "index_exploraciones_predefinidas_on_exploracion_id"
  end

  create_table "exploraciones_resultados", force: :cascade do |t|
    t.bigint "ficha_clinica_id"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "exploracion_predefinida_id"
    t.index ["exploracion_predefinida_id"], name: "index_exploraciones_resultados_on_exploracion_predefinida_id"
    t.index ["ficha_clinica_id"], name: "index_exploraciones_resultados_on_ficha_clinica_id"
  end

  create_table "ficha_clinicas", force: :cascade do |t|
    t.bigint "persona_id"
    t.string "medico_hospital"
    t.string "diagnostico_presuntivo"
    t.string "observacion"
    t.string "triage_color"
    t.string "estado"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "vehiculo_asignado_id"
    t.bigint "hospitals_id"
    t.index ["hospitals_id"], name: "index_ficha_clinicas_on_hospitals_id"
    t.index ["persona_id"], name: "index_ficha_clinicas_on_persona_id"
    t.index ["vehiculo_asignado_id"], name: "index_ficha_clinicas_on_vehiculo_asignado_id"
  end

  create_table "hospitals", force: :cascade do |t|
    t.string "nombre"
    t.bigint "ciudad_id"
    t.boolean "activo", default: true
    t.boolean "tipo_publico", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ciudad_id"], name: "index_hospitals_on_ciudad_id"
  end

  create_table "obstetricos", force: :cascade do |t|
    t.string "nombre"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oringes_probables", force: :cascade do |t|
    t.bigint "causa_clinica_id"
    t.bigint "ficha_clinica_id"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["causa_clinica_id"], name: "index_oringes_probables_on_causa_clinica_id"
    t.index ["ficha_clinica_id"], name: "index_oringes_probables_on_ficha_clinica_id"
  end

  create_table "pacientes_pertenencia", force: :cascade do |t|
    t.bigint "ficha_clinica_id"
    t.string "quien_recibe"
    t.string "descripcion"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ficha_clinica_id"], name: "index_pacientes_pertenencia_on_ficha_clinica_id"
  end

  create_table "permiso_acciones", force: :cascade do |t|
    t.string "nombre", null: false
    t.boolean "activo", default: true
    t.bigint "permiso_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["permiso_id"], name: "index_permiso_acciones_on_permiso_id"
  end

  create_table "permisos", force: :cascade do |t|
    t.string "nombre"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nombre"], name: "index_permisos_on_nombre", unique: true
  end

  create_table "personas", force: :cascade do |t|
    t.bigint "ciudad_id"
    t.string "nombres"
    t.string "apellidos"
    t.string "cedula"
    t.integer "edad"
    t.string "sexo"
    t.string "telefono_1"
    t.string "telefono_2"
    t.string "numero_de_seguro"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ciudad_id"], name: "index_personas_on_ciudad_id"
  end

  create_table "procedimientos", force: :cascade do |t|
    t.string "nombre"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "procedimientos_ejecutados", force: :cascade do |t|
    t.bigint "ficha_clinica_id"
    t.bigint "procedimiento_id"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ficha_clinica_id"], name: "index_procedimientos_ejecutados_on_ficha_clinica_id"
    t.index ["procedimiento_id"], name: "index_procedimientos_ejecutados_on_procedimiento_id"
  end

  create_table "productos", force: :cascade do |t|
    t.string "nombre"
    t.integer "cant_minima"
    t.boolean "activo", default: true
    t.integer "cantidad", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "productos_consumidos", force: :cascade do |t|
    t.bigint "producto_vehiculo_id"
    t.bigint "vehiculo_ficha_historial_id"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "cant_utilizada"
    t.index ["producto_vehiculo_id"], name: "index_productos_consumidos_on_producto_vehiculo_id"
    t.index ["vehiculo_ficha_historial_id"], name: "index_productos_consumidos_on_vehiculo_ficha_historial_id"
  end

  create_table "productos_vehiculos", force: :cascade do |t|
    t.bigint "vehiculo_id"
    t.bigint "producto_id"
    t.integer "cant_minima"
    t.integer "cantidad"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["producto_id"], name: "index_productos_vehiculos_on_producto_id"
    t.index ["vehiculo_id"], name: "index_productos_vehiculos_on_vehiculo_id"
  end

  create_table "rol_permiso_acciones", force: :cascade do |t|
    t.boolean "activo", default: true
    t.bigint "permiso_accione_id", null: false
    t.bigint "role_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["permiso_accione_id"], name: "index_rol_permiso_acciones_on_permiso_accione_id"
    t.index ["role_id"], name: "index_rol_permiso_acciones_on_role_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "signos_encontrados", force: :cascade do |t|
    t.bigint "signo_vital_id"
    t.bigint "ficha_clinica_id"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "value"
    t.index ["ficha_clinica_id"], name: "index_signos_encontrados_on_ficha_clinica_id"
    t.index ["signo_vital_id"], name: "index_signos_encontrados_on_signo_vital_id"
  end

  create_table "signos_vitales", force: :cascade do |t|
    t.string "nombre"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tandas", force: :cascade do |t|
    t.bigint "cargo_id"
    t.bigint "dia_id"
    t.time "hora_inicio"
    t.time "hora_fin"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "descripcion"
    t.index ["cargo_id"], name: "index_tandas_on_cargo_id"
    t.index ["dia_id"], name: "index_tandas_on_dia_id"
  end

  create_table "traumas", force: :cascade do |t|
    t.string "nombre"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "turnos", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "tanda_id"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tanda_id"], name: "index_turnos_on_tanda_id"
    t.index ["user_id"], name: "index_turnos_on_user_id"
  end

  create_table "turnos_vehiculos", force: :cascade do |t|
    t.bigint "vehiculo_id"
    t.bigint "turno_id"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["turno_id"], name: "index_turnos_vehiculos_on_turno_id"
    t.index ["vehiculo_id"], name: "index_turnos_vehiculos_on_vehiculo_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean "allow_password_change", default: false
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "nickname"
    t.string "image"
    t.string "email"
    t.json "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "cargo_id"
    t.bigint "persona_id"
    t.time "horas_acumuladas"
    t.time "horas_sobrantes"
    t.index ["cargo_id"], name: "index_users_on_cargo_id"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["persona_id"], name: "index_users_on_persona_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  create_table "vehiculo_asignados", force: :cascade do |t|
    t.bigint "turnos_vehiculo_id"
    t.bigint "emergencia_id"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["emergencia_id"], name: "index_vehiculo_asignados_on_emergencia_id"
    t.index ["turnos_vehiculo_id"], name: "index_vehiculo_asignados_on_turnos_vehiculo_id"
  end

  create_table "vehiculos", force: :cascade do |t|
    t.string "marca"
    t.string "modelo"
    t.string "placa"
    t.string "anio"
    t.string "alias"
    t.datetime "ultimo_restablecimiento_de_invetario"
    t.string "estado"
    t.boolean "activo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "vehiculo_tipo"
  end

  create_table "vehiculos_fichas_historiales", force: :cascade do |t|
    t.bigint "ficha_clinica_id"
    t.boolean "activo", default: true
    t.boolean "entregado_completado", default: false
    t.float "km_inicial_vehiculo", default: 0.0
    t.float "km_final_vehiculo", default: 0.0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ficha_clinica_id"], name: "index_vehiculos_fichas_historiales_on_ficha_clinica_id"
  end

  add_foreign_key "antecedentes_encontrados", "antecedentes"
  add_foreign_key "antecedentes_encontrados", "ficha_clinicas"
  add_foreign_key "causas_traumaticas", "ficha_clinicas"
  add_foreign_key "causas_traumaticas", "traumas"
  add_foreign_key "compras", "productos"
  add_foreign_key "compras", "users"
  add_foreign_key "emergencia", "ciudades"
  add_foreign_key "emergencia", "emergencia_tipos"
  add_foreign_key "estados_obstetricos", "embaraso_estados"
  add_foreign_key "estados_obstetricos", "ficha_clinicas"
  add_foreign_key "estados_obstetricos", "obstetricos"
  add_foreign_key "exploraciones_predefinidas", "areas_fisicas"
  add_foreign_key "exploraciones_predefinidas", "exploraciones"
  add_foreign_key "exploraciones_resultados", "exploraciones_predefinidas"
  add_foreign_key "exploraciones_resultados", "ficha_clinicas"
  add_foreign_key "ficha_clinicas", "hospitals", column: "hospitals_id"
  add_foreign_key "ficha_clinicas", "personas"
  add_foreign_key "ficha_clinicas", "vehiculo_asignados"
  add_foreign_key "hospitals", "ciudades"
  add_foreign_key "oringes_probables", "causas_clinicas"
  add_foreign_key "oringes_probables", "ficha_clinicas"
  add_foreign_key "pacientes_pertenencia", "ficha_clinicas"
  add_foreign_key "permiso_acciones", "permisos"
  add_foreign_key "personas", "ciudades"
  add_foreign_key "procedimientos_ejecutados", "ficha_clinicas"
  add_foreign_key "procedimientos_ejecutados", "procedimientos"
  add_foreign_key "productos_consumidos", "productos_vehiculos"
  add_foreign_key "productos_consumidos", "vehiculos_fichas_historiales"
  add_foreign_key "productos_vehiculos", "productos"
  add_foreign_key "productos_vehiculos", "vehiculos"
  add_foreign_key "rol_permiso_acciones", "permiso_acciones"
  add_foreign_key "rol_permiso_acciones", "roles"
  add_foreign_key "signos_encontrados", "ficha_clinicas"
  add_foreign_key "signos_encontrados", "signos_vitales"
  add_foreign_key "tandas", "cargos"
  add_foreign_key "tandas", "dia", column: "dia_id"
  add_foreign_key "turnos", "tandas"
  add_foreign_key "turnos", "users"
  add_foreign_key "turnos_vehiculos", "turnos"
  add_foreign_key "turnos_vehiculos", "vehiculos"
  add_foreign_key "users", "cargos"
  add_foreign_key "users", "personas"
  add_foreign_key "users_roles", "roles"
  add_foreign_key "users_roles", "users"
  add_foreign_key "vehiculo_asignados", "emergencia", column: "emergencia_id"
  add_foreign_key "vehiculo_asignados", "turnos_vehiculos", column: "turnos_vehiculo_id"
  add_foreign_key "vehiculos_fichas_historiales", "ficha_clinicas"
end
