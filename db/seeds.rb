# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
if Dia.find_by_nombre( 'Domingo' ).nil?
   Dia.create([
      { nombre: 'Domingo',   activo: true, alias: 'Dom' }, 
      { nombre: 'Lunes',     activo: true, alias: 'Lun' },
      { nombre: 'Martes',    activo: true, alias: 'Mar' },
      { nombre: 'Miércoles', activo: true, alias: 'Mie' },
      { nombre: 'Jueves',    activo: true, alias: 'Jue' },
      { nombre: 'Viernes',   activo: true, alias: 'Vie' },
      { nombre: 'Sábado',    activo: true, alias: 'Sab' }
   ])

   Cargo.create([
      { nombre: 'Medico',   activo: true },
      { nombre: 'Medico Regulador',   activo: true },
      { nombre: 'Chofer',   activo: true }, 
      { nombre: 'Camillero', activo: true }
   ])

   Tanda.create([
      { cargo_id: 3, dia_id: 1, hora_inicio: '14:00', hora_fin:'18:00' },
      { cargo_id: 3, dia_id: 2, hora_inicio: '14:00', hora_fin:'18:00' },
      { cargo_id: 3, dia_id: 3, hora_inicio: '14:00', hora_fin:'18:00' },
      { cargo_id: 3, dia_id: 4, hora_inicio: '14:00', hora_fin:'18:00' },
      { cargo_id: 3, dia_id: 5, hora_inicio: '14:00', hora_fin:'18:00' },
      { cargo_id: 3, dia_id: 6, hora_inicio: '14:00', hora_fin:'18:00' },
      { cargo_id: 3, dia_id: 7, hora_inicio: '14:00', hora_fin:'18:00' }
      # { cargo_id: 2, dia_id: 2, hora_inicio: '14:00', hora_fin:'18:00' },
      # { cargo_id: 3, dia_id: 3, hora_inicio: '14:00', hora_fin:'18:00' },
      # { cargo_id: 4, dia_id: 4, hora_inicio: '14:00', hora_fin:'18:00' },
      # { cargo_id: 2, dia_id: 6, hora_inicio: '14:00', hora_fin:'18:00' },
      # { cargo_id: 3, dia_id: 7, hora_inicio: '14:00', hora_fin:'18:00' }
   ])


   Ciudad.create([
      { nombre: 'La Vega',   activo: true },
      { nombre: 'Puerto Plata',   activo: true },
      { nombre: 'Santiago',   activo: true },
      { nombre: 'Monseñor Nouel',   activo: true },
      { nombre: 'Azua',   activo: true },
      { nombre: 'Pedernales',   activo: true },
      { nombre: 'Peravia',   activo: true },
      { nombre: 'Samaná',   activo: true },
      { nombre: 'Distrito Nacional',   activo: true },
      { nombre: 'Santiago Rodríguez',   activo: true },
      { nombre: 'Santo Domingo',   activo: true },
      { nombre: 'San José de Ocoa',   activo: true },
      { nombre: 'San Cristóbal',   activo: true },
      { nombre: 'Higuei',   activo: true },
      { nombre: 'San Juan',   activo: true },
      { nombre: 'San Pedro de Macorís',   activo: true },
      { nombre: 'Sánchez Ramírez',   activo: true },
      { nombre: 'Neiba',   activo: true },
      { nombre: 'Barahona',   activo: true },
      { nombre: 'Dajabón',   activo: true },
      { nombre: 'Duarte',   activo: true },
      { nombre: 'Elías Piña',   activo: true },
      { nombre: 'El Seibo',   activo: true },
      { nombre: 'Espaillat',   activo: true },
      { nombre: 'Hato Mayor',   activo: true },
      { nombre: 'Hermanas Mirabal',   activo: true },
      { nombre: 'Independencia',   activo: true },
      { nombre: 'La Altagracia',   activo: true },
      { nombre: 'La Romana',   activo: true },
      { nombre: 'Monte Cristi',   activo: true },
      { nombre: 'Monte Plata',   activo: true },
      { nombre: 'María Trinidad Sánchez',   activo: true }
   ])
end
if Hospital.find_by_nombre( 'Prof. Morillo King' ).nil?
   Hospital.create([
      { ciudad_id: Ciudad.first.id, nombre:'Prof. Morillo King' }
   ])
end

if User.find_by_email( 'admin@g.com' ).nil?
   Persona.create([
      {
         ciudad_id: Ciudad.first.id,
         nombres: 'Admin',
         apellidos: "Admin",
         cedula: '15987423641',
         edad: 18,
         sexo: 'M',
         # cargo: 1,
         telefono_1: '',
         telefono_2: '',
         numero_de_seguro: ''
      }
   ])

   if Persona.find_by_nombres( 'JUANA' ).nil?
      Persona.create([
         {edad: 24, ciudad_id: 1 ,nombres: 'JUANA',apellidos:'ROSADO', cedula:'402-0000001-6',sexo:'M', telefono_1:'829-000-0001', telefono_2:'8090-573-0001', numero_de_seguro:'101', activo:true},
         {edad: 24, ciudad_id: 1 ,nombres: 'PEDRO',apellidos:'CACERES', cedula:'402-0000002-6',sexo:'M', telefono_1:'829-000-0002', telefono_2:'809-573-0002', numero_de_seguro:'102', activo:true},
         {edad: 25, ciudad_id: 1 ,nombres: 'MARIA',apellidos:'LA VECINA', cedula:'402-0000003-6',sexo:'M', telefono_1:'829-000-0003', telefono_2:'809-573-0003', numero_de_seguro:'103', activo:true},
         {edad: 21, ciudad_id: 2 ,nombres: 'PABLO',apellidos:'RUIZ', cedula:'402-0000004-6',sexo:'M', telefono_1:'829-000-0004', telefono_2:'809-573-0004', numero_de_seguro:'104', activo:true},
         {edad: 19, ciudad_id: 3 ,nombres: 'MANUEL',apellidos:'VELOZ', cedula:'402-0000004-6',sexo:'M', telefono_1:'829-000-0004', telefono_2:'809-573-0004', numero_de_seguro:'104', activo:true},
         {edad: 15, ciudad_id: 2 ,nombres: 'SECRETO',apellidos:'QUEZADA', cedula:'402-0000004-6',sexo:'M', telefono_1:'829-000-0004', telefono_2:'809-573-0004', numero_de_seguro:'104', activo:true},
         {edad: 16, ciudad_id: 2 ,nombres: 'CHIMBALA',apellidos:'REYES', cedula:'402-0000004-6',sexo:'M', telefono_1:'829-000-0004', telefono_2:'809-573-0004', numero_de_seguro:'104', activo:true},
         {edad: 16, ciudad_id: 4 ,nombres: 'EL ALFA',apellidos:'EL JEFE', cedula:'402-0000004-6',sexo:'M', telefono_1:'829-000-0004', telefono_2:'809-573-0004', numero_de_seguro:'104', activo:true},
         {edad: 18, ciudad_id: 2 ,nombres: 'BAD BUNNY',apellidos:'BB', cedula:'402-0000004-6',sexo:'M', telefono_1:'829-000-0004', telefono_2:'809-573-0004', numero_de_seguro:'104', activo:true},
         {edad: 30, ciudad_id: 2 ,nombres: 'ARCANGEL',apellidos:'LA MARAVILLA', cedula:'402-0000004-6',sexo:'M', telefono_1:'829-000-0004', telefono_2:'809-573-0004', numero_de_seguro:'104', activo:true},
         {edad: 41, ciudad_id: 3 ,nombres: 'YEMEL',apellidos:'TU MELOZO', cedula:'402-0000004-6',sexo:'M', telefono_1:'829-000-0004', telefono_2:'809-573-0004', numero_de_seguro:'104', activo:true}
      ])
   end

   u = User.create([
         {
            password: '12345678',
            password_confirmation: '12345678',
            email: 'admin@g.com',
            nickname: 'admin',
            cargo_id: 3,
            persona_id: 1
         },
         {
            password: '12345678',
            password_confirmation: '12345678',
            email: 'persona1@g.com',
            nickname: 'juan chofer 02',
            cargo_id: 3,
            persona_id: 2
         },
         {
            password: '12345678',
            password_confirmation: '12345678',
            email: 'persona2@g.com',
            nickname: 'persona2',
            cargo_id: 2,
            persona_id: 3
         },
         {
            password: '12345678',
            password_confirmation: '12345678',
            email: 'persona3@g.com',
            nickname: 'persona3',
            cargo_id: 4,
            persona_id: 4
         }
      ]
   )

end 

if Turno.find_by_id( 1 ).nil?
   Turno.create([
      {tanda_id:1 , user_id:1},
      {tanda_id:2 , user_id:1},
      {tanda_id:3 , user_id:1},
      {tanda_id:4 , user_id:1},
      {tanda_id:5 , user_id:1},
      {tanda_id:6 , user_id:1},
      {tanda_id:7 , user_id:1},
      
      {tanda_id:1 , user_id:2},
      {tanda_id:2 , user_id:2},
      {tanda_id:3 , user_id:2},
      {tanda_id:4 , user_id:2},
      {tanda_id:5 , user_id:2},
      {tanda_id:6 , user_id:2},
      {tanda_id:7 , user_id:2}
   ])
end

if EmbarasoEstado.find_by_nombre( 'Trabajo de parto' ).nil?
   EmbarasoEstado.create([
      {nombre: 'Trabajo de parto'},
      {nombre: 'Sangrado vaginal'},
      {nombre: 'Fuente Rota'},
      {nombre: 'Tiempo Gestación'}
   ])
end

if Obstetrico.find_by_nombre( 'Gesta' ).nil?
   Obstetrico.create([
      {nombre: 'Gesta' },
      {nombre: 'Paridad'},
      {nombre: 'Aborto'},
      {nombre: 'Natural'},
      {nombre: 'Cesárea'}
   ])
end





if Trauma.find_by_nombre( 'Metabólico' ).nil?
   Trauma.create([{ nombre: 'Metabólico' } ,
      { nombre: 'Motocicleta' } ,
      { nombre: 'Arma Blanca' } ,
      { nombre: 'Producto Quimico' } ,
      { nombre: 'Maquinaria' } ,
      { nombre: 'Herramienta' } ,
      { nombre: 'Vehiculo de Motor' } ,
      { nombre: 'Sustancia caliente' } ,
      { nombre: 'Sustancia Toxica' } ,
      { nombre: 'Animal' } ,
      { nombre: 'Explosión' } ,
      { nombre: 'Electricidad' } ,
      { nombre: 'Ser Humano' }
   ])
end

if Antecedente.find_by_nombre( 'MHA' ).nil?
   Antecedente.create([
      {nombre: 'MHA'},
      {nombre: 'DC'},
      {nombre: 'IC'},
      {nombre: 'IR'},
      {nombre: 'CA'},
      {nombre: 'ASMA'},
      {nombre: 'EPILEPSIA'},
      {nombre: 'EMARAZO'},
      {nombre: 'ALERGIA'},
      {nombre: 'VERTIGO'},
      {nombre: 'MEDICAMENTOS'},
      {nombre: 'HIPOTENSION ARTERIAL'},
      {nombre: 'OSTEO'},
      {nombre: 'OTRO'}
   ])
end

if CausasClinica.find_by_nombre( 'Neurológia' ).nil?
   CausasClinica.create([
      {nombre: 'Neurológia'},
      {nombre: 'Psiquiátrico'},
      {nombre: 'Cardiovascular'},
      {nombre: 'Respiratorio'},
      {nombre: 'Matabólico'},
      {nombre: 'Digestiva'},
      {nombre: 'Urogenital'},
      {nombre: 'Pediátrica'},
      {nombre: 'Gineco Obstétrica'},
      {nombre: 'Psico-emotiva'},
      {nombre: 'Musculo Esquelétca'},
      {nombre: 'Infecciosa'},
      {nombre: 'Oncológicas'},
      {nombre: 'Otros'}
   ])
end

# EXPLORACION PRIMARIA
if Exploracion.find_by_nombre( 'Cálida' ).nil?
   Exploracion.create([
      {nombre: 'Cálida'},
      {nombre: 'Caliente'},
      {nombre: 'Fría'},
      {nombre: 'Sudorosa'},
      {nombre: 'Seca'},
      {nombre: 'Cianótica'},
      {nombre: 'Hematoma'},
      {nombre: 'Hundimiento'},
      {nombre: 'Penetrante'},
      {nombre: 'Escoriación'},
      {nombre: 'Perfo. ojo'},
      {nombre: 'Anisocoria'},
      {nombre: 'Miosis'},
      {nombre: 'Midriasis'},
      {nombre: 'Desvió traquea'},
      {nombre: 'Enfisema subc'},
      {nombre: 'Injugitación Venoyugular'},
      {nombre: 'Tórax Inestable'},
      {nombre: 'Crepitación'},
      {nombre: 'Disminución de Ruidos'},
      {nombre: 'Disminución de Ruidos Cardiacos'},
      {nombre: 'Murmullo Vesicular Ausente'},
      {nombre: 'Estertores'}
   ])
end

# EXPLORACION FISCICA (SECUNDARIA)
if Exploracion.find_by_nombre( 'Dolor' ).nil?
  Exploracion.create([
      {nombre: 'Dolor'},
      {nombre: 'Distención'},
      {nombre: 'Evisceración'},
      {nombre: 'Rigido'},
      {nombre: 'Inestabilidad'},
      {nombre: 'Deformidad'},
      {nombre: 'Hermiplejia'},
      {nombre: 'Hermiparesia'},
      {nombre: 'Paraplejia'},
      {nombre: 'Tetraplejia'},

      {nombre: 'Contusión'},
      {nombre: 'Abrasiones'},
      {nombre: 'Herida'},
      {nombre: 'Fractura'},
      {nombre: 'Quemadura'},
      {nombre: 'Laceraciones'},
      {nombre: 'Hemorragia'},
      {nombre: 'Amputación'}
   ])
end
if Producto.find_by_nombre( 'apósitos' ).nil?
   Producto.create([
      {cantidad: 10,cant_minima: 5,nombre: 'apósitos', activo:true},
      {cantidad: 10,cant_minima: 5,nombre: 'alcohol de 96º', activo:true},
      {cantidad: 10,cant_minima: 5,nombre: 'tiritas', activo:true},
      {cantidad: 10,cant_minima: 5,nombre: 'suero', activo:true},
      {cantidad: 10,cant_minima: 5,nombre: 'guantes', activo:true},
      {cantidad: 10,cant_minima: 5,nombre: 'suturas', activo:true}
   ])
end
if AreasFisica.find_by_nombre( 'Piel' ).nil?
   AreasFisica.create([
      {nombre: 'Piel'},
      {nombre: 'Cabeza'},
      {nombre: 'Cara'},
      {nombre: 'Cuello'},
      {nombre: 'Tórax'},
      {nombre: 'Abdomen'},
      {nombre: 'Pelvis'},
      {nombre: 'Columna'},
      {nombre: 'Miembros'}
   ])
end

if EmergenciaTipo.find_by_nombre('Incendio').nil?
   EmergenciaTipo.create([
      {nombre:'Incendio',descripcion:'flamas de fuego no controladas'},
      {nombre:'Inundación',descripcion:'Abundancia de agua en un espacio no apto para tener esa cantidad de agua'},
      {nombre:'Accidente de Transito',descripcion:'Chocar contra otro vehiculo o perder el control de vehiculo y volcar el vehiculo'},
      {nombre:'Accidente aereo',descripcion:'caida de un transporte aereo '}
   ])
end



# if Emergencia.find_by_id( 1 ).nil?
#    Emergencia.create([
#       { emergencia_tipo_id: 1, ciudad_id: 1, sector:'Villa Lora', lugar_del_evento:'Casa de Lucho', descripcion:'Rodo por las escaleras', telefono_infomante:'829-920-0762', activo:true, lat: 19.217663817171868, long: -70.52512541413306},
#       { emergencia_tipo_id: 1, ciudad_id: 1, sector:'Arenoso1', lugar_del_evento:'Casa de JUNIOR', descripcion:'Rodo por DEL TECHO', telefono_infomante:'829-920-6270', activo:true, lat: 19.217663817171868, long: -70.52512541413306},
#       { emergencia_tipo_id: 1, ciudad_id: 2, sector:'Arenoso2', lugar_del_evento:'Casa de Pedro', descripcion:'Rodo por en el CONTEN', telefono_infomante:'829-920-6270', activo:true, lat: 19.217663817171868, long: -70.52512541413306},
#       { emergencia_tipo_id: 1, ciudad_id: 1, sector:'Arenoso1', lugar_del_evento:'Casa de Manuel', descripcion:'CHOCO EN LA PASOLITA', telefono_infomante:'829-920-6270', activo:true, lat: 19.217663817171868, long: -70.52512541413306},
#       { emergencia_tipo_id: 1, ciudad_id: 3, sector:'Arenoso3', lugar_del_evento:'Casa de Carlos', descripcion:'CHOCO EN JEEP', telefono_infomante:'829-920-6270', activo:true, lat: 19.217663817171868, long: -70.52512541413306},
#       { emergencia_tipo_id: 1, ciudad_id: 4, sector:'Arenoso4', lugar_del_evento:'Casa de 384', descripcion:'Rodo por DEL TECHO', telefono_infomante:'829-920-6270', activo:true, lat: 19.217663817171868, long: -70.52512541413306},
#       { emergencia_tipo_id: 1, ciudad_id: 2, sector:'Arenoso2', lugar_del_evento:'Casa de Domingo', descripcion:'Rodo por DEL TECHO', telefono_infomante:'829-920-6270', activo:true, lat: 19.217663817171868, long: -70.52512541413306},
#       { emergencia_tipo_id: 1, ciudad_id: 2, sector:'Arenoso2', lugar_del_evento:'Casa de Pablo', descripcion:'Rodo por DEL TECHO', telefono_infomante:'829-920-6270', activo:true, lat: 19.217663817171868, long: -70.52512541413306},
#       { emergencia_tipo_id: 1, ciudad_id: 1, sector:'Arenoso1', lugar_del_evento:'Casa de Daniel', descripcion:'Rodo por DEL TECHO', telefono_infomante:'829-920-6270', activo:true, lat: 19.217663817171868, long: -70.52512541413306}
#    ])
# end


# if AntecedentesEncontrado.find_by_id( 1 ).nil?
#    puts "="*60
#    puts "AntecedentesEncontrado #{AntecedentesEncontrado.all().to_json}"
#    puts "="*60
#    AntecedentesEncontrado.create([
#       {id:  1,ficha_clinica_id: 1, antecedente_id: 1, activo: true}
#    ])
# end

if ExploracionesPredefinida.find_by_area_fisica_id( 1 ).nil?
   # ExploracionesPredefinida.create([
      # ====== PIEL =====
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(1, 1, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(2, 1, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(3, 1, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(4, 1, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(5, 1, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(6, 1, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      # ====== CABEZA =====
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(7, 2, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(8, 2, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(9, 2, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(10, 2, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      # ====== CARA =====
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(11, 3, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(12, 3, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(13, 3, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(14, 3, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      # ====== CUELLO =====
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(15, 4, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(16, 4, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(17, 4, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      # ====== TORAX =====
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(18, 5, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(19, 5, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(20, 5, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(21, 5, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(22, 5, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(23, 5, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      # ====== ABDOMEN =====
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(24, 6, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(25, 6, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(26, 6, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(27, 6, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      # ====== PELVIS =====
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(24, 7, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(28, 7, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      # ====== COLUMNA =====
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(24, 8, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(29, 8, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      # ====== MIEMBROS =====
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(30, 9, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(31, 9, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(32, 9, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
      ActiveRecord::Base.connection.exec_query("INSERT INTO public.exploraciones_predefinidas (  exploracion_id, area_fisica_id, activo, created_at, updated_at) VALUES(33, 9, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
   #    {exploracion_id: 1 ,area_fisica_id: 1 , activo:true },
   #    {exploracion_id: 2 ,area_fisica_id: 1 , activo:true },
   #    {exploracion_id: 3 ,area_fisica_id: 1 , activo:true },
   #    {exploracion_id: 4 ,area_fisica_id: 1 , activo:true },
   #    {exploracion_id: 5 ,area_fisica_id: 1 , activo:true },
   #    {exploracion_id: 6 ,area_fisica_id: 1 , activo:true }
   # # ])
end




if SignosVitale.find_by_nombre( 'FR' ).nil?
   SignosVitale.create([
      {nombre: 'FR'},
      {nombre: 'TA'},
      {nombre: 'FC'},
      {nombre: 'SaO2'},
      {nombre: 'Temperatura'},
      {nombre: 'Glicemia'}
   ])
end




if Procedimiento.find_by_nombre( 'Verificación Manual' ).nil?
   Procedimiento.create([
      {nombre: 'Verificación Manual'},
      {nombre: 'Verificación Mecanica'},
      {nombre: 'Oxigenoterapia L /min'},
      {nombre: 'Fluidoterapia.Volumen'},
      {nombre: 'Medicación'},
      {nombre: 'Punción Cricotiroidea'},
      {nombre: 'Intubación'},
      {nombre: 'Vendaje oclusivo de Tórax'},
      {nombre: 'Inmobilización de Extremidades'},
      {nombre: 'Descompresión Tórax'},
      {nombre: 'Sondaje vesical'},
      {nombre: 'Sondaje Gástrico'},
      {nombre: 'Sutura/curación'},
      {nombre: 'Taponamiento nasal'},
      {nombre: 'Hemostasia'},
      {nombre: 'Dren Toracico'},
      {nombre: 'Acceso Venoso Central'},
      {nombre: 'Acceso Venoso Perifecico'},
      {nombre: 'Acceso Venoso Intraoseo'},
      {nombre: 'Cricotiroidoctamia'},
      {nombre: 'Colocación de Canula Laringea'},
      {nombre: 'Inmobilización de Externidades'},
      {nombre: 'Uso de Torniquete'}
   ])
end


if Vehiculo.find_by_placa( '1234567890' ).nil?
   Vehiculo.create([
      {
         marca: 'Mercedes-Benz',
         modelo: 'Sprinter 212 D',
         placa: '1234567890',
         anio: '2000',
         alias: 'MB Sprinter 212 D',
         estado: 'D',
         vehiculo_tipo: 'A'
      },
      {
         marca: 'Bremach',
         modelo: '4x4',
         placa: '1324576890',
         anio: '2005',
         alias: 'Bremach 4x4',
         estado: 'D',
         vehiculo_tipo: 'A'
      },
      {
         marca: 'Volkswagen',
         modelo: 'TRANSPORTER 2.5 TDi',
         placa: '1357624680',
         anio: '2005',
         alias: 'Volkswagen TRANSPORTER 2.5 TDi',
         estado: 'D',
         vehiculo_tipo: 'A'
      },
      {
         marca: 'Ford',
         modelo: 'F450',
         placa: '1598742365',
         anio: '2008',
         alias: 'Ford F450',
         estado: 'D',
         vehiculo_tipo: 'A'
      },
      {
         marca: 'Toyota',
         modelo: 'HZJ78L 4x4',
         placa: '7896421347',
         anio: '2017',
         alias: 'Toyota HZJ78L 4x4',
         estado: 'D',
         vehiculo_tipo: 'A'
      }
   ])
end

# if TurnosVehiculo.find_by_id( 1 ).nil?
#    TurnosVehiculo.create([
#       {turno_id:1 , vehiculo_id:1},
#       {turno_id:2 , vehiculo_id:1},
#       {turno_id:3 , vehiculo_id:1},
#       {turno_id:4 , vehiculo_id:1},
#       {turno_id:5 , vehiculo_id:1},
#       {turno_id:6 , vehiculo_id:1},
#       {turno_id:7 , vehiculo_id:1}
#    ])
# end

# if VehiculoAsignado.find_by_id( 1 ).nil?
#    puts "|=>#{ Emergencia.last.to_json}"
#    puts "|=>"*30
#    VehiculoAsignado.create([
#       { emergencia_id: 1, turnos_vehiculo_id: 1}
#    ])
# end

# if FichaClinica.find_by_id( 1 ).nil?
#    puts "|=>"*60
#    FichaClinica.create([
#       { vehiculo_asignado_id: 1 , persona_id: 1, hospital_destino:  'MORILLO KING', medico_hospital: 'PABLO VIDAL', diagnostico_presuntivo: 'PIE ROTO', observacion: 'NECESITARA YESO', triage_color: 'V', estado: 'A', activo:true},
#       { vehiculo_asignado_id: 1 , persona_id: 2, hospital_destino:  'MORILLO KING', medico_hospital: 'PABLO VIDAL', diagnostico_presuntivo: 'PIE ROTO', observacion: 'NECESITARA YESO', triage_color: 'V', estado: 'A', activo:true},
#       { vehiculo_asignado_id: 1 , persona_id: 3, hospital_destino:  'MORILLO KING', medico_hospital: 'PABLO VIDAL', diagnostico_presuntivo: 'PIE ROTO', observacion: 'NECESITARA YESO', triage_color: 'V', estado: 'A', activo:true},
#       { vehiculo_asignado_id: 1 , persona_id: 4, hospital_destino:  'MORILLO KING', medico_hospital: 'PABLO VIDAL', diagnostico_presuntivo: 'PIE ROTO', observacion: 'NECESITARA YESO', triage_color: 'V', estado: 'A', activo:true},
#       { vehiculo_asignado_id: 1 , persona_id: 5, hospital_destino:  'MORILLO KING', medico_hospital: 'PABLO VIDAL', diagnostico_presuntivo: 'PIE ROTO', observacion: 'NECESITARA YESO', triage_color: 'V', estado: 'A', activo:true},
#       { vehiculo_asignado_id: 1 , persona_id: 6, hospital_destino:  'MORILLO KING', medico_hospital: 'PABLO VIDAL', diagnostico_presuntivo: 'PIE ROTO', observacion: 'NECESITARA YESO', triage_color: 'V', estado: 'A', activo:true},
#       { vehiculo_asignado_id: 1 , persona_id: 7, hospital_destino:  'MORILLO KING', medico_hospital: 'PABLO VIDAL', diagnostico_presuntivo: 'PIE ROTO', observacion: 'NECESITARA YESO', triage_color: 'V', estado: 'A', activo:true},
#       { vehiculo_asignado_id: 1 , persona_id: 8, hospital_destino:  'MORILLO KING', medico_hospital: 'PABLO VIDAL', diagnostico_presuntivo: 'PIE ROTO', observacion: 'NECESITARA YESO', triage_color: 'V', estado: 'A', activo:true},
#       { vehiculo_asignado_id: 1 , persona_id: 9, hospital_destino:  'MORILLO KING', medico_hospital: 'PABLO VIDAL', diagnostico_presuntivo: 'PIE ROTO', observacion: 'NECESITARA YESO', triage_color: 'V', estado: 'A', activo:true},
#       { vehiculo_asignado_id: 1 , persona_id: 10, hospital_destino:  'MORILLO KING', medico_hospital: 'PABLO VIDAL', diagnostico_presuntivo: 'PIE ROTO', observacion: 'NECESITARA YESO', triage_color: 'V', estado: 'A', activo:true},
#       { vehiculo_asignado_id: 1 , persona_id: 1, hospital_destino:  'MORILLO KING', medico_hospital: 'PABLO VIDAL', diagnostico_presuntivo: 'PIE ROTO', observacion: 'NECESITARA YESO', triage_color: 'V', estado: 'A', activo:true},
#       { vehiculo_asignado_id: 1 , persona_id: 1, hospital_destino:  'MORILLO KING', medico_hospital: 'PABLO VIDAL', diagnostico_presuntivo: 'PIE ROTO', observacion: 'NECESITARA YESO', triage_color: 'V', estado: 'A', activo:true},
#       { vehiculo_asignado_id: 1 , persona_id: 1, hospital_destino:  'MORILLO KING', medico_hospital: 'PABLO VIDAL', diagnostico_presuntivo: 'PIE ROTO', observacion: 'NECESITARA YESO', triage_color: 'V', estado: 'A', activo:true}
#    ])
# end










puts "creacion de permisos => ", :json => Permiso.create(
	[
		{ activo: true, nombre: 'Emergencias' },
		{ activo: true, nombre: 'Mis Emergencias' },
		{ activo: true, nombre: 'Calendario' },
      { activo: true, nombre: 'Compras' },
		{ activo: true, nombre: 'Cargos' },
		{ activo: true, nombre: 'Personas' },
		{ activo: true, nombre: 'Tandas' },
		{ activo: true, nombre: 'Turnos' },
		{ activo: true, nombre: 'Vehiculos' },
		{ activo: true, nombre: 'Productos' },
		{ activo: true, nombre: 'Productos Vehiculos' },
		{ activo: true, nombre: 'Turnos Vehiculos' },
		{ activo: true, nombre: 'Hospitales' },
		{ activo: true, nombre: 'Roles' },
		{ activo: true, nombre: 'Reportes' }
	]
)

# ========================== Emergencias ========================== #
puts "creacion de Emergencias => ", :json => PermisoAccione.create([
	{nombre: 'Ver listado Emergencias', permiso_id: Permiso.find_by_nombre('Emergencias').id},
	{nombre: 'Crear Emergencia', permiso_id: Permiso.find_by_nombre('Emergencias').id},
	{nombre: 'Eliminar Emergencia', permiso_id: Permiso.find_by_nombre('Emergencias').id}
])

# ========================== Mis Emergencias ========================== #
puts "creacion de Mis Emergencias => ", :json => PermisoAccione.create([
	{nombre: 'Ver listado de Mis Emergencias', permiso_id: Permiso.find_by_nombre('Mis Emergencias').id},
	{nombre: 'Agregar Ficha Paciente', permiso_id: Permiso.find_by_nombre('Mis Emergencias').id},
	{nombre: 'Actualizar Ficha Paciente', permiso_id: Permiso.find_by_nombre('Mis Emergencias').id},
	{nombre: 'Ver Ubicacion Emergencia', permiso_id: Permiso.find_by_nombre('Mis Emergencias').id}
])

# ========================== Calendario ========================== #
puts "creacion de Calendario => ", :json => PermisoAccione.create([
	{nombre: 'Ver Calendario', permiso_id: Permiso.find_by_nombre('Calendario').id}
])

# ========================== Compras ========================== #
puts "creacion de Compras => ", :json => PermisoAccione.create([
	{nombre: 'Crear Compra', permiso_id: Permiso.find_by_nombre('Compras').id}
])

# ========================== Cargos ========================== #
puts "creacion de Cargos => ", :json => PermisoAccione.create([
	{nombre: 'Ver listado Cargos', permiso_id: Permiso.find_by_nombre('Cargos').id},
	{nombre: 'Ver Cargo', permiso_id: Permiso.find_by_nombre('Cargos').id},
	{nombre: 'Crear Cargo', permiso_id: Permiso.find_by_nombre('Cargos').id},
	{nombre: 'Actualizar Cargo', permiso_id: Permiso.find_by_nombre('Cargos').id},
	{nombre: 'Eliminar Cargo', permiso_id: Permiso.find_by_nombre('Cargos').id}
])

# ========================== Personas ========================== #
puts "creacion de Personas => ", :json => PermisoAccione.create([
	{nombre: 'Ver listado Personas', permiso_id: Permiso.find_by_nombre('Personas').id},
	{nombre: 'Ver Persona', permiso_id: Permiso.find_by_nombre('Personas').id},
	{nombre: 'Crear Persona', permiso_id: Permiso.find_by_nombre('Personas').id},
	{nombre: 'Actualizar Persona', permiso_id: Permiso.find_by_nombre('Personas').id},
	{nombre: 'Eliminar Persona', permiso_id: Permiso.find_by_nombre('Personas').id}
])

# ========================== Tandas ========================== #
puts "creacion de Tandas => ", :json => PermisoAccione.create([
	{nombre: 'Ver listado Tandas', permiso_id: Permiso.find_by_nombre('Tandas').id},
	{nombre: 'Ver Tanda', permiso_id: Permiso.find_by_nombre('Tandas').id},
	{nombre: 'Crear Tanda', permiso_id: Permiso.find_by_nombre('Tandas').id},
	{nombre: 'Actualizar Tanda', permiso_id: Permiso.find_by_nombre('Tandas').id},
	{nombre: 'Eliminar Tanda', permiso_id: Permiso.find_by_nombre('Tandas').id}
])

# ========================== Turnos ========================== #
puts "creacion de Turnos => ", :json => PermisoAccione.create([
	{nombre: 'Ver listado Turnos', permiso_id: Permiso.find_by_nombre('Turnos').id},
	{nombre: 'Ver Turno', permiso_id: Permiso.find_by_nombre('Turnos').id},
	{nombre: 'Crear Turno', permiso_id: Permiso.find_by_nombre('Turnos').id},
	{nombre: 'Actualizar Turno', permiso_id: Permiso.find_by_nombre('Turnos').id},
	{nombre: 'Eliminar Turno', permiso_id: Permiso.find_by_nombre('Turnos').id}
])

# ========================== Vehiculos ========================== #
puts "creacion de Vehiculos => ", :json => PermisoAccione.create([
	{nombre: 'Ver listado Vehiculos', permiso_id: Permiso.find_by_nombre('Vehiculos').id},
	{nombre: 'Ver Vehiculo', permiso_id: Permiso.find_by_nombre('Vehiculos').id},
	{nombre: 'Crear Vehiculo', permiso_id: Permiso.find_by_nombre('Vehiculos').id},
	{nombre: 'Actualizar Vehiculo', permiso_id: Permiso.find_by_nombre('Vehiculos').id},
	{nombre: 'Eliminar Vehiculo', permiso_id: Permiso.find_by_nombre('Vehiculos').id}
])

# ========================== Productos ========================== #
puts "creacion de Productos => ", :json => PermisoAccione.create([
	{nombre: 'Ver listado Productos', permiso_id: Permiso.find_by_nombre('Productos').id},
	{nombre: 'Ver Producto', permiso_id: Permiso.find_by_nombre('Productos').id},
	{nombre: 'Crear Producto', permiso_id: Permiso.find_by_nombre('Productos').id},
	{nombre: 'Actualizar Producto', permiso_id: Permiso.find_by_nombre('Productos').id},
	{nombre: 'Eliminar Producto', permiso_id: Permiso.find_by_nombre('Productos').id}
])

# ========================== Productos Vehiculos ========================== #
puts "creacion de Productos Vehiculos => ", :json => PermisoAccione.create([
	{nombre: 'Ver listado Productos Vehiculos', permiso_id: Permiso.find_by_nombre('Productos Vehiculos').id},
	{nombre: 'Asignar Productos a Vehiculos', permiso_id: Permiso.find_by_nombre('Productos Vehiculos').id}
])

# ========================== Turnos Vehiculos ========================== #
puts "creacion de Turnos Vehiculos => ", :json => PermisoAccione.create([
   {nombre: 'Ver listado Turnos de Vehiculos', permiso_id: Permiso.find_by_nombre('Turnos Vehiculos').id},
   {nombre: 'Asignar Vehiculos a Turnos', permiso_id: Permiso.find_by_nombre('Turnos Vehiculos').id},
   {nombre: 'Actualizar Turnos de Vehiculos', permiso_id: Permiso.find_by_nombre('Turnos Vehiculos').id}
])
   
# ========================== Hospitales ========================== #
puts "creacion de Hospitales => ", :json => PermisoAccione.create([
	{nombre: 'Ver listado Hospitales', permiso_id: Permiso.find_by_nombre('Hospitales').id},
	{nombre: 'Ver Hospital', permiso_id: Permiso.find_by_nombre('Hospitales').id},
	{nombre: 'Crear Hospital', permiso_id: Permiso.find_by_nombre('Hospitales').id},
	{nombre: 'Actualizar Hospital', permiso_id: Permiso.find_by_nombre('Hospitales').id},
	{nombre: 'Eliminar Hospital', permiso_id: Permiso.find_by_nombre('Hospitales').id}
])

# ========================== Roles ========================== #
puts "creacion de Roles => ", :json => PermisoAccione.create([
	{nombre: 'Ver listado Roles', permiso_id: Permiso.find_by_nombre('Roles').id},
	{nombre: 'Ver Rol', permiso_id: Permiso.find_by_nombre('Roles').id},
	{nombre: 'Crear Rol', permiso_id: Permiso.find_by_nombre('Roles').id},
	{nombre: 'Actualizar Rol', permiso_id: Permiso.find_by_nombre('Roles').id},
	{nombre: 'Eliminar Rol', permiso_id: Permiso.find_by_nombre('Roles').id}
])

# ========================== Reportes ========================== #
puts "creacion de Reportes => ", :json => PermisoAccione.create([
	{nombre: 'Edad Promedio por Ciudad', permiso_id: Permiso.find_by_nombre('Reportes').id},
	{nombre: 'Emergencia Dia y noche', permiso_id: Permiso.find_by_nombre('Reportes').id},
	{nombre: 'Empleados por Cargo', permiso_id: Permiso.find_by_nombre('Reportes').id},
	{nombre: 'Promedio de Accidentado por ciudad', permiso_id: Permiso.find_by_nombre('Reportes').id},
	{nombre: 'Insumos por emergencia', permiso_id: Permiso.find_by_nombre('Reportes').id},
	{nombre: 'Compras por Ciudad', permiso_id: Permiso.find_by_nombre('Reportes').id},
	{nombre: 'Cantidad de producto por emergencia', permiso_id: Permiso.find_by_nombre('Reportes').id},
	{nombre: 'Segmentar por periodos emergencia', permiso_id: Permiso.find_by_nombre('Reportes').id},
	{nombre: 'Productos mas consumidos', permiso_id: Permiso.find_by_nombre('Reportes').id},
	{nombre: 'Ambulancias mas utilizadas', permiso_id: Permiso.find_by_nombre('Reportes').id},
	{nombre: 'Productos mas consumidos por emergencia', permiso_id: Permiso.find_by_nombre('Reportes').id},
	{nombre: 'Pacientes fallecidos por ciudad', permiso_id: Permiso.find_by_nombre('Reportes').id}
])


user = User.find_by_nickname("admin")

puts "user = >", :json => user

user.add_role "ADMIN"

allPermisos = ApplicationRecord.allPermisos

allPermisos.each do |key_permiso, key_value|
	puts "permiso => ", key_value
	key_value.each do |key_action, value|
		puts "accion permiso => ", key_action == "name"
      unless key_action == "name"
         jValue = value.to_json 
         puts "34657890 permiso => ", key_action
         permiso_accione = PermisoAccione.find_by_nombre(value)
			unless permiso_accione.nil?
            puts "34657890 permiso_accione => ", permiso_accione.to_json
				RolPermisoAccione.create({
					role_id: Role.find_by_name('ADMIN').id,
					permiso_accione_id: permiso_accione.id
            })
			end
		end
	end
end