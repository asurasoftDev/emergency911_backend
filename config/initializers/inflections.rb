# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format. Inflections
# are locale specific, and you may define rules for as many different
# locales as you wish. All of these examples are active by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end

ActiveSupport::Inflector.inflections(:en) do |inflect|
	inflect.irregular 'ciudad', 'ciudades'
	inflect.irregular 'exploracion', 'exploraciones'
	# inflect.irregular 'ficha_clinica', 'fichas_clinicas'
	inflect.irregular 'turno_vehiculo', 'turnos_vehiculos'
	inflect.irregular 'producto_vehiculo', 'productos_vehiculos'
	inflect.irregular 'vehiculo_ficha_historial', 'vehiculos_fichas_historiales'
	inflect.irregular 'embaraso_estado', 'embaraso_estados'
	inflect.irregular 'producto_consumido', 'productos_consumidos'
	inflect.irregular 'antecedentes_encontrado', 'antecedentes_encontrados'
	inflect.irregular 'causa_traumatica', 'causas_traumaticas'
	inflect.irregular 'causa_clinica', 'causas_clinicas'
	# inflect.irregular 'ficha_clinica', 'fichas_clinicas'
	inflect.irregular 'procedimiento_ejecutado', 'procedimientos_ejecutados'
	inflect.irregular 'exploracione_resultado', 'exploracions_resultados'
	inflect.irregular 'area_fisica', 'areas_fisicas'
	inflect.irregular 'signo_encontrado', 'signos_encontrados'
	inflect.irregular 'vehiculo_asginado', 'vehiculos_asginados'
	inflect.irregular 'signo_vital', 'signos_vitales'
	inflect.irregular 'oringe_probable', 'oringes_probables'
	inflect.irregular 'exploracion_predefinida', 'exploraciones_predefinidas'
	inflect.irregular 'permiso_accione', 'permiso_acciones'
	inflect.irregular 'rol_permiso_accione', 'rol_permiso_acciones'
end
# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.acronym 'RESTful'
# end
