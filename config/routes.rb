Rails.application.routes.draw do
  resources :rol_permiso_acciones
  resources :permiso_acciones
  resources :vehiculo_asignados
  resources :emergencia_tipos
	scope :api, defaults: { format: 'json' } do
		scope :v1 do
      resources :permisos
      resources :hospitals
      resources :roles
      resources :personas
      resources :ciudades
      resources :oringes_probables
      resources :signos_encontrados
      resources :exploraciones_resultados
      resources :procedimientos_ejecutados
      resources :causas_traumaticas
      resources :antecedentes_encontrados
      resources :estados_obstetricos
      resources :obstetricos
      resources :productos_consumidos
      resources :vehiculos_fichas_historiales
      resources :pacientes_pertenencia
      resources :fichas_clinicas
      resources :turnos_vehiculos
      resources :turnos
      mount_devise_token_auth_for 'User', at: 'auth', controllers: {
			    sessions:  'devise_token_auth/sessions',
			    registrations:  'devise_token_auth/registrations',
			    token_validations:  'devise_token_auth/token_validations'
			}
      resources :productos_vehiculos
      resources :productos
      resources :vehiculos
      resources :embaraso_estados
      resources :traumas
      resources :antecedentes
      resources :causas_clinicas
      resources :signos_vitales
      resources :procedimientos
      resources :exploraciones
      resources :areas_fisicas
      resources :tandas
      resources 'dia', at: 'dias'
      resources :cargos
      resources :emergencia
      resources :causas
      resources :ciudads
      resources :agentes_casuales
      resources :compras
      resources :exploraciones_predefinidas
      resources :vehiculo_asignados
      resources :emergencia_tipos

      get    'productos_de_mi_vehiculo/:id'          => 'productos_vehiculos#productos_de_mi_vehiculo'
      get    'emergencias/:fecha1/:fecha2'           => 'emergencia#emergenciasPorFecha'
      get    'emergencys/:id'                        => 'emergencia#detalleDeEmergencia'
      get    'finalizar_emergencia/:id'              => 'emergencia#finalizarEmergencia'
      delete 'emergencys/:id'                        => 'emergencia#deleteEmergencia'
      get    'emergencys'                            => 'emergencia#detalleDeEmergencia'
      get    'mis_emergencias'                       => 'emergencia#emergenciasActivasDeAmbulancia'
      get    'plantilla_1/:id/:vehiculo_asignado_id' => 'fichas_clinicas#plantilla'
      get    'plantilla_1/:vehiculo_asignado_id'     => 'fichas_clinicas#plantilla'
      post   'plantilla_llena'                       => 'fichas_clinicas#crear'
      get    'plantilla_llena/:id'                   => 'fichas_clinicas#actualizar'
      get    'report_1'                              => 'reportes#reportPacientePorGeneroPorCiudad'
      get    'report_2'                              => 'reportes#reportEmpleadosPorCargo'
      get    'report_3'                              => 'reportes#reportEdadPromedioDePacientesPorCiudad'
      get    'report_4/:fecha1/:fecha2'              => 'reportes#cantidadProductoEmergencia'
      get    'report_5/:fecha1/:fecha2'              => 'reportes#reportEmergenciasPorDiaYPorNoche'
      get    'report_6/:fecha1/:fecha2'              => 'reportes#cantidadProductoPorTipoDeEmergencia'
      get    'report_7/:tipo/:fecha1/:fecha2'        => 'reportes#emergenciaPorFrecuencia'
      get    'report_8/:fecha1/:fecha2'              => 'reportes#reportComprasPorCiudadPorFecha'
      get    'report_9/:fecha1/:fecha2'              => 'reportes#productosMasConsumidos'
      get    'report_10/:fecha1/:fecha2'             => 'reportes#ambulanciaMasUtilizadas'
      get    'report_11/:fecha1/:fecha2'             => 'reportes#productosMasConsumidosPorTipoDeEmergencia'
      get    'report_12/:fecha1/:fecha2'             => 'reportes#pacientesFallecidosPorCiudad'
      get    'report_13/:fecha1/:fecha2'             => 'reportes#hospitalesConMasAccidentadosIngreados'
      get    'report_14/:fecha1/:fecha2'             => 'reportes#pacientesVivosEntregados'
      get    'vehiculos_disponibles'                 => 'emergencia#vehiculosDisponiblesParaEmergencias'
      get    'usuarios'                              => 'welcomes#list'
      get    'plantilla'                             => 'welcomes#plantilla'
      get    'usuarios/:id'                          => 'welcomes#getUsuario'
      get    'turnos_empleado'                       => 'welcomes#turnoGroupEmpleado'
      post   'turnos/eliminar_por_usuario'           => 'turnos#destroyTurno'

      get 'turnos_disponibles' => 'turnos#turnos_disponibles'
      get 'vehiculos_turnos'   => 'vehiculos#vehiculos_turnos'
      get 'calendario'         => 'dia#calendario'

      get 'vehiculos/:id/turnos' => 'vehiculos#turnos'
      get 'last_record'          => 'reportes#lastRecordTables'
    end
	end
end
