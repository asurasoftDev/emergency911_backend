require 'test_helper'

class ExploracionesResultadosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @exploraciones_resultado = exploraciones_resultados(:one)
  end

  test "should get index" do
    get exploraciones_resultados_url, as: :json
    assert_response :success
  end

  test "should create exploraciones_resultado" do
    assert_difference('ExploracionesResultado.count') do
      post exploraciones_resultados_url, params: { exploraciones_resultado: { activo: @exploraciones_resultado.activo, area_fisica_id: @exploraciones_resultado.area_fisica_id, exploracion_id: @exploraciones_resultado.exploracion_id, ficha_clinica_id: @exploraciones_resultado.ficha_clinica_id } }, as: :json
    end

    assert_response 201
  end

  test "should show exploraciones_resultado" do
    get exploraciones_resultado_url(@exploraciones_resultado), as: :json
    assert_response :success
  end

  test "should update exploraciones_resultado" do
    patch exploraciones_resultado_url(@exploraciones_resultado), params: { exploraciones_resultado: { activo: @exploraciones_resultado.activo, area_fisica_id: @exploraciones_resultado.area_fisica_id, exploracion_id: @exploraciones_resultado.exploracion_id, ficha_clinica_id: @exploraciones_resultado.ficha_clinica_id } }, as: :json
    assert_response 200
  end

  test "should destroy exploraciones_resultado" do
    assert_difference('ExploracionesResultado.count', -1) do
      delete exploraciones_resultado_url(@exploraciones_resultado), as: :json
    end

    assert_response 204
  end
end
