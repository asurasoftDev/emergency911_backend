require 'test_helper'

class TraumasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @trauma = traumas(:one)
  end

  test "should get index" do
    get traumas_url, as: :json
    assert_response :success
  end

  test "should create trauma" do
    assert_difference('Trauma.count') do
      post traumas_url, params: { trauma: { activo: @trauma.activo, nombre: @trauma.nombre } }, as: :json
    end

    assert_response 201
  end

  test "should show trauma" do
    get trauma_url(@trauma), as: :json
    assert_response :success
  end

  test "should update trauma" do
    patch trauma_url(@trauma), params: { trauma: { activo: @trauma.activo, nombre: @trauma.nombre } }, as: :json
    assert_response 200
  end

  test "should destroy trauma" do
    assert_difference('Trauma.count', -1) do
      delete trauma_url(@trauma), as: :json
    end

    assert_response 204
  end
end
