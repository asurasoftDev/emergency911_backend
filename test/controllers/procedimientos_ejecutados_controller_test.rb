require 'test_helper'

class ProcedimientosEjecutadosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @procedimiento_ejecutado = procedimientos_ejecutados(:one)
  end

  test "should get index" do
    get procedimientos_ejecutados_url, as: :json
    assert_response :success
  end

  test "should create procedimiento_ejecutado" do
    assert_difference('ProcedimientosEjecutado.count') do
      post procedimientos_ejecutados_url, params: { procedimiento_ejecutado: { activo: @procedimiento_ejecutado.activo, ficha_clinica_id: @procedimiento_ejecutado.ficha_clinica_id, procedimiento_id: @procedimiento_ejecutado.procedimiento_id } }, as: :json
    end

    assert_response 201
  end

  test "should show procedimiento_ejecutado" do
    get procedimiento_ejecutado_url(@procedimiento_ejecutado), as: :json
    assert_response :success
  end

  test "should update procedimiento_ejecutado" do
    patch procedimiento_ejecutado_url(@procedimiento_ejecutado), params: { procedimiento_ejecutado: { activo: @procedimiento_ejecutado.activo, ficha_clinica_id: @procedimiento_ejecutado.ficha_clinica_id, procedimiento_id: @procedimiento_ejecutado.procedimiento_id } }, as: :json
    assert_response 200
  end

  test "should destroy procedimiento_ejecutado" do
    assert_difference('ProcedimientosEjecutado.count', -1) do
      delete procedimiento_ejecutado_url(@procedimiento_ejecutado), as: :json
    end

    assert_response 204
  end
end
