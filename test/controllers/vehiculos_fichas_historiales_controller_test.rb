require 'test_helper'

class VehiculosFichasHistorialesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @vehiculo_ficha_historial = vehiculos_fichas_historiales(:one)
  end

  test "should get index" do
    get vehiculos_fichas_historiales_url, as: :json
    assert_response :success
  end

  test "should create vehiculo_ficha_historial" do
    assert_difference('VehiculosFichasHistoriale.count') do
      post vehiculos_fichas_historiales_url, params: { vehiculo_ficha_historial: { activo: @vehiculo_ficha_historial.activo, ficha_clinica_id: @vehiculo_ficha_historial.ficha_clinica_id, turno_vehiculo_id: @vehiculo_ficha_historial.turno_vehiculo_id } }, as: :json
    end

    assert_response 201
  end

  test "should show vehiculo_ficha_historial" do
    get vehiculo_ficha_historial_url(@vehiculo_ficha_historial), as: :json
    assert_response :success
  end

  test "should update vehiculo_ficha_historial" do
    patch vehiculo_ficha_historial_url(@vehiculo_ficha_historial), params: { vehiculo_ficha_historial: { activo: @vehiculo_ficha_historial.activo, ficha_clinica_id: @vehiculo_ficha_historial.ficha_clinica_id, turno_vehiculo_id: @vehiculo_ficha_historial.turno_vehiculo_id } }, as: :json
    assert_response 200
  end

  test "should destroy vehiculo_ficha_historial" do
    assert_difference('VehiculosFichasHistoriale.count', -1) do
      delete vehiculo_ficha_historial_url(@vehiculo_ficha_historial), as: :json
    end

    assert_response 204
  end
end
