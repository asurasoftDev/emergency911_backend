require 'test_helper'

class VehiculosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @vehiculo = vehiculos(:one)
  end

  test "should get index" do
    get vehiculos_url, as: :json
    assert_response :success
  end

  test "should create vehiculo" do
    assert_difference('Vehiculo.count') do
      post vehiculos_url, params: { vehiculo: { activo: @vehiculo.activo, alias: @vehiculo.alias, anio: @vehiculo.anio, estado: @vehiculo.estado, marca: @vehiculo.marca, modelo: @vehiculo.modelo, placa: @vehiculo.placa, ultimo_restablecimiento_de_invetario: @vehiculo.ultimo_restablecimiento_de_invetario } }, as: :json
    end

    assert_response 201
  end

  test "should show vehiculo" do
    get vehiculo_url(@vehiculo), as: :json
    assert_response :success
  end

  test "should update vehiculo" do
    patch vehiculo_url(@vehiculo), params: { vehiculo: { activo: @vehiculo.activo, alias: @vehiculo.alias, anio: @vehiculo.anio, estado: @vehiculo.estado, marca: @vehiculo.marca, modelo: @vehiculo.modelo, placa: @vehiculo.placa, ultimo_restablecimiento_de_invetario: @vehiculo.ultimo_restablecimiento_de_invetario } }, as: :json
    assert_response 200
  end

  test "should destroy vehiculo" do
    assert_difference('Vehiculo.count', -1) do
      delete vehiculo_url(@vehiculo), as: :json
    end

    assert_response 204
  end
end
