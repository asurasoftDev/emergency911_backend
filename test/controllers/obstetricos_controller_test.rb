require 'test_helper'

class ObstetricosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @obstetrico = obstetricos(:one)
  end

  test "should get index" do
    get obstetricos_url, as: :json
    assert_response :success
  end

  test "should create obstetrico" do
    assert_difference('Obstetrico.count') do
      post obstetricos_url, params: { obstetrico: { activo: @obstetrico.activo, ficha_clinica_id: @obstetrico.ficha_clinica_id, fuente_rota: @obstetrico.fuente_rota, sangrado_vaginal: @obstetrico.sangrado_vaginal, tiempo_gestacion_en_semanas: @obstetrico.tiempo_gestacion_en_semanas, trabajo_de_parto: @obstetrico.trabajo_de_parto } }, as: :json
    end

    assert_response 201
  end

  test "should show obstetrico" do
    get obstetrico_url(@obstetrico), as: :json
    assert_response :success
  end

  test "should update obstetrico" do
    patch obstetrico_url(@obstetrico), params: { obstetrico: { activo: @obstetrico.activo, ficha_clinica_id: @obstetrico.ficha_clinica_id, fuente_rota: @obstetrico.fuente_rota, sangrado_vaginal: @obstetrico.sangrado_vaginal, tiempo_gestacion_en_semanas: @obstetrico.tiempo_gestacion_en_semanas, trabajo_de_parto: @obstetrico.trabajo_de_parto } }, as: :json
    assert_response 200
  end

  test "should destroy obstetrico" do
    assert_difference('Obstetrico.count', -1) do
      delete obstetrico_url(@obstetrico), as: :json
    end

    assert_response 204
  end
end
