require 'test_helper'

class AntecedentesEncontradosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @antecedente_encontrado = antecedentes_encontrados(:one)
  end

  test "should get index" do
    get antecedentes_encontrados_url, as: :json
    assert_response :success
  end

  test "should create antecedente_encontrado" do
    assert_difference('AntecedentesEncontrado.count') do
      post antecedentes_encontrados_url, params: { antecedente_encontrado: { activo: @antecedente_encontrado.activo, antecedente_id: @antecedente_encontrado.antecedente_id, ficha_clinica_id: @antecedente_encontrado.ficha_clinica_id } }, as: :json
    end

    assert_response 201
  end

  test "should show antecedente_encontrado" do
    get antecedente_encontrado_url(@antecedente_encontrado), as: :json
    assert_response :success
  end

  test "should update antecedente_encontrado" do
    patch antecedente_encontrado_url(@antecedente_encontrado), params: { antecedente_encontrado: { activo: @antecedente_encontrado.activo, antecedente_id: @antecedente_encontrado.antecedente_id, ficha_clinica_id: @antecedente_encontrado.ficha_clinica_id } }, as: :json
    assert_response 200
  end

  test "should destroy antecedente_encontrado" do
    assert_difference('AntecedentesEncontrado.count', -1) do
      delete antecedente_encontrado_url(@antecedente_encontrado), as: :json
    end

    assert_response 204
  end
end
