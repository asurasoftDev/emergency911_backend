require 'test_helper'

class ProductosCosumidosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @producto_cosumido = productos_cosumidos(:one)
  end

  test "should get index" do
    get productos_cosumidos_url, as: :json
    assert_response :success
  end

  test "should create producto_cosumido" do
    assert_difference('ProductosCosumido.count') do
      post productos_cosumidos_url, params: { producto_cosumido: { activo: @producto_cosumido.activo, producto_vehiculo_id: @producto_cosumido.producto_vehiculo_id, vehiculo_ficha_historial_id: @producto_cosumido.vehiculo_ficha_historial_id } }, as: :json
    end

    assert_response 201
  end

  test "should show producto_cosumido" do
    get producto_cosumido_url(@producto_cosumido), as: :json
    assert_response :success
  end

  test "should update producto_cosumido" do
    patch producto_cosumido_url(@producto_cosumido), params: { producto_cosumido: { activo: @producto_cosumido.activo, producto_vehiculo_id: @producto_cosumido.producto_vehiculo_id, vehiculo_ficha_historial_id: @producto_cosumido.vehiculo_ficha_historial_id } }, as: :json
    assert_response 200
  end

  test "should destroy producto_cosumido" do
    assert_difference('ProductosCosumido.count', -1) do
      delete producto_cosumido_url(@producto_cosumido), as: :json
    end

    assert_response 204
  end
end
