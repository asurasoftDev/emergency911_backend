require 'test_helper'

class FichasClinicasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ficha_clinica = fichas_clinicas(:one)
  end

  test "should get index" do
    get fichas_clinicas_url, as: :json
    assert_response :success
  end

  test "should create ficha_clinica" do
    assert_difference('FichasClinica.count') do
      post fichas_clinicas_url, params: { ficha_clinica: { activo: @ficha_clinica.activo, diagnostico_presuntivo: @ficha_clinica.diagnostico_presuntivo, emergencia_id: @ficha_clinica.emergencia_id, estado: @ficha_clinica.estado, medico_hospital: @ficha_clinica.medico_hospital, observacion: @ficha_clinica.observacion, persona_id: @ficha_clinica.persona_id, triage_color: @ficha_clinica.triage_color } }, as: :json
    end

    assert_response 201
  end

  test "should show ficha_clinica" do
    get ficha_clinica_url(@ficha_clinica), as: :json
    assert_response :success
  end

  test "should update ficha_clinica" do
    patch ficha_clinica_url(@ficha_clinica), params: { ficha_clinica: { activo: @ficha_clinica.activo, diagnostico_presuntivo: @ficha_clinica.diagnostico_presuntivo, emergencia_id: @ficha_clinica.emergencia_id, estado: @ficha_clinica.estado, medico_hospital: @ficha_clinica.medico_hospital, observacion: @ficha_clinica.observacion, persona_id: @ficha_clinica.persona_id, triage_color: @ficha_clinica.triage_color } }, as: :json
    assert_response 200
  end

  test "should destroy ficha_clinica" do
    assert_difference('FichasClinica.count', -1) do
      delete ficha_clinica_url(@ficha_clinica), as: :json
    end

    assert_response 204
  end
end
