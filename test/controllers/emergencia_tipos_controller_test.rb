require 'test_helper'

class EmergenciaTiposControllerTest < ActionDispatch::IntegrationTest
  setup do
    @emergencia_tipo = emergencia_tipos(:one)
  end

  test "should get index" do
    get emergencia_tipos_url, as: :json
    assert_response :success
  end

  test "should create emergencia_tipo" do
    assert_difference('EmergenciaTipo.count') do
      post emergencia_tipos_url, params: { emergencia_tipo: { activo: @emergencia_tipo.activo, descripcion: @emergencia_tipo.descripcion, nombre: @emergencia_tipo.nombre } }, as: :json
    end

    assert_response 201
  end

  test "should show emergencia_tipo" do
    get emergencia_tipo_url(@emergencia_tipo), as: :json
    assert_response :success
  end

  test "should update emergencia_tipo" do
    patch emergencia_tipo_url(@emergencia_tipo), params: { emergencia_tipo: { activo: @emergencia_tipo.activo, descripcion: @emergencia_tipo.descripcion, nombre: @emergencia_tipo.nombre } }, as: :json
    assert_response 200
  end

  test "should destroy emergencia_tipo" do
    assert_difference('EmergenciaTipo.count', -1) do
      delete emergencia_tipo_url(@emergencia_tipo), as: :json
    end

    assert_response 204
  end
end
