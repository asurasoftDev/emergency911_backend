require 'test_helper'

class ExploracionesPredefinidasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @exploraciones_predefinida = exploraciones_predefinidas(:one)
  end

  test "should get index" do
    get exploraciones_predefinidas_url, as: :json
    assert_response :success
  end

  test "should create exploraciones_predefinida" do
    assert_difference('ExploracionesPredefinida.count') do
      post exploraciones_predefinidas_url, params: { exploraciones_predefinida: { activo: @exploraciones_predefinida.activo, area_fisica_id: @exploraciones_predefinida.area_fisica_id, exploracion_id: @exploraciones_predefinida.exploracion_id } }, as: :json
    end

    assert_response 201
  end

  test "should show exploraciones_predefinida" do
    get exploraciones_predefinida_url(@exploraciones_predefinida), as: :json
    assert_response :success
  end

  test "should update exploraciones_predefinida" do
    patch exploraciones_predefinida_url(@exploraciones_predefinida), params: { exploraciones_predefinida: { activo: @exploraciones_predefinida.activo, area_fisica_id: @exploraciones_predefinida.area_fisica_id, exploracion_id: @exploraciones_predefinida.exploracion_id } }, as: :json
    assert_response 200
  end

  test "should destroy exploraciones_predefinida" do
    assert_difference('ExploracionesPredefinida.count', -1) do
      delete exploraciones_predefinida_url(@exploraciones_predefinida), as: :json
    end

    assert_response 204
  end
end
