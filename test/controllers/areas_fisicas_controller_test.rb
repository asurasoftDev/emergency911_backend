require 'test_helper'

class AreasFisicasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @area_fisica = areas_fisicas(:one)
  end

  test "should get index" do
    get areas_fisicas_url, as: :json
    assert_response :success
  end

  test "should create area_fisica" do
    assert_difference('AreasFisica.count') do
      post areas_fisicas_url, params: { area_fisica: { activo: @area_fisica.activo, nombre: @area_fisica.nombre } }, as: :json
    end

    assert_response 201
  end

  test "should show area_fisica" do
    get area_fisica_url(@area_fisica), as: :json
    assert_response :success
  end

  test "should update area_fisica" do
    patch area_fisica_url(@area_fisica), params: { area_fisica: { activo: @area_fisica.activo, nombre: @area_fisica.nombre } }, as: :json
    assert_response 200
  end

  test "should destroy area_fisica" do
    assert_difference('AreasFisica.count', -1) do
      delete area_fisica_url(@area_fisica), as: :json
    end

    assert_response 204
  end
end
