require 'test_helper'

class SignosVitalesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @signo_vital = signos_vitales(:one)
  end

  test "should get index" do
    get signos_vitales_url, as: :json
    assert_response :success
  end

  test "should create signo_vital" do
    assert_difference('SignosVitale.count') do
      post signos_vitales_url, params: { signo_vital: { activo: @signo_vital.activo, nombre: @signo_vital.nombre } }, as: :json
    end

    assert_response 201
  end

  test "should show signo_vital" do
    get signo_vital_url(@signo_vital), as: :json
    assert_response :success
  end

  test "should update signo_vital" do
    patch signo_vital_url(@signo_vital), params: { signo_vital: { activo: @signo_vital.activo, nombre: @signo_vital.nombre } }, as: :json
    assert_response 200
  end

  test "should destroy signo_vital" do
    assert_difference('SignosVitale.count', -1) do
      delete signo_vital_url(@signo_vital), as: :json
    end

    assert_response 204
  end
end
