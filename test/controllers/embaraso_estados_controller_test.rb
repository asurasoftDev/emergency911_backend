require 'test_helper'

class EmbarasoEstadosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @embaraso_estado = embaraso_estados(:one)
  end

  test "should get index" do
    get embaraso_estados_url, as: :json
    assert_response :success
  end

  test "should create embaraso_estado" do
    assert_difference('EmbarasoEstado.count') do
      post embaraso_estados_url, params: { embaraso_estado: { activo: @embaraso_estado.activo, nombre: @embaraso_estado.nombre } }, as: :json
    end

    assert_response 201
  end

  test "should show embaraso_estado" do
    get embaraso_estado_url(@embaraso_estado), as: :json
    assert_response :success
  end

  test "should update embaraso_estado" do
    patch embaraso_estado_url(@embaraso_estado), params: { embaraso_estado: { activo: @embaraso_estado.activo, nombre: @embaraso_estado.nombre } }, as: :json
    assert_response 200
  end

  test "should destroy embaraso_estado" do
    assert_difference('EmbarasoEstado.count', -1) do
      delete embaraso_estado_url(@embaraso_estado), as: :json
    end

    assert_response 204
  end
end
