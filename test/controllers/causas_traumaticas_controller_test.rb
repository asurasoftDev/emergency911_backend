require 'test_helper'

class CausasTraumaticasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @causa_traumatica = causas_traumaticas(:one)
  end

  test "should get index" do
    get causas_traumaticas_url, as: :json
    assert_response :success
  end

  test "should create causa_traumatica" do
    assert_difference('CausasTraumatica.count') do
      post causas_traumaticas_url, params: { causa_traumatica: { activo: @causa_traumatica.activo, causado_por: @causa_traumatica.causado_por, descripcion: @causa_traumatica.descripcion, ficha_clinica_id: @causa_traumatica.ficha_clinica_id, trauma_id: @causa_traumatica.trauma_id } }, as: :json
    end

    assert_response 201
  end

  test "should show causa_traumatica" do
    get causa_traumatica_url(@causa_traumatica), as: :json
    assert_response :success
  end

  test "should update causa_traumatica" do
    patch causa_traumatica_url(@causa_traumatica), params: { causa_traumatica: { activo: @causa_traumatica.activo, causado_por: @causa_traumatica.causado_por, descripcion: @causa_traumatica.descripcion, ficha_clinica_id: @causa_traumatica.ficha_clinica_id, trauma_id: @causa_traumatica.trauma_id } }, as: :json
    assert_response 200
  end

  test "should destroy causa_traumatica" do
    assert_difference('CausasTraumatica.count', -1) do
      delete causa_traumatica_url(@causa_traumatica), as: :json
    end

    assert_response 204
  end
end
