require 'test_helper'

class PermisoAccionesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @permiso_accione = permiso_acciones(:one)
  end

  test "should get index" do
    get permiso_acciones_url, as: :json
    assert_response :success
  end

  test "should create permiso_accione" do
    assert_difference('PermisoAccione.count') do
      post permiso_acciones_url, params: { permiso_accione: { activo: @permiso_accione.activo, nombre: @permiso_accione.nombre, permiso_id: @permiso_accione.permiso_id } }, as: :json
    end

    assert_response 201
  end

  test "should show permiso_accione" do
    get permiso_accione_url(@permiso_accione), as: :json
    assert_response :success
  end

  test "should update permiso_accione" do
    patch permiso_accione_url(@permiso_accione), params: { permiso_accione: { activo: @permiso_accione.activo, nombre: @permiso_accione.nombre, permiso_id: @permiso_accione.permiso_id } }, as: :json
    assert_response 200
  end

  test "should destroy permiso_accione" do
    assert_difference('PermisoAccione.count', -1) do
      delete permiso_accione_url(@permiso_accione), as: :json
    end

    assert_response 204
  end
end
