require 'test_helper'

class OringesProbablesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @oringe_probable = oringes_probables(:one)
  end

  test "should get index" do
    get oringes_probables_url, as: :json
    assert_response :success
  end

  test "should create oringe_probable" do
    assert_difference('OringesProbable.count') do
      post oringes_probables_url, params: { oringe_probable: { activo: @oringe_probable.activo, causa_clinica_id: @oringe_probable.causa_clinica_id, ficha_clinica_id: @oringe_probable.ficha_clinica_id } }, as: :json
    end

    assert_response 201
  end

  test "should show oringe_probable" do
    get oringe_probable_url(@oringe_probable), as: :json
    assert_response :success
  end

  test "should update oringe_probable" do
    patch oringe_probable_url(@oringe_probable), params: { oringe_probable: { activo: @oringe_probable.activo, causa_clinica_id: @oringe_probable.causa_clinica_id, ficha_clinica_id: @oringe_probable.ficha_clinica_id } }, as: :json
    assert_response 200
  end

  test "should destroy oringe_probable" do
    assert_difference('OringesProbable.count', -1) do
      delete oringe_probable_url(@oringe_probable), as: :json
    end

    assert_response 204
  end
end
