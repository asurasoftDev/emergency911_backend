require 'test_helper'

class TurnosVehiculosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @turno_vehiculo = turnos_vehiculos(:one)
  end

  test "should get index" do
    get turnos_vehiculos_url, as: :json
    assert_response :success
  end

  test "should create turno_vehiculo" do
    assert_difference('TurnosVehiculo.count') do
      post turnos_vehiculos_url, params: { turno_vehiculo: { activo: @turno_vehiculo.activo, turno_id: @turno_vehiculo.turno_id, vehiculo_id: @turno_vehiculo.vehiculo_id } }, as: :json
    end

    assert_response 201
  end

  test "should show turno_vehiculo" do
    get turno_vehiculo_url(@turno_vehiculo), as: :json
    assert_response :success
  end

  test "should update turno_vehiculo" do
    patch turno_vehiculo_url(@turno_vehiculo), params: { turno_vehiculo: { activo: @turno_vehiculo.activo, turno_id: @turno_vehiculo.turno_id, vehiculo_id: @turno_vehiculo.vehiculo_id } }, as: :json
    assert_response 200
  end

  test "should destroy turno_vehiculo" do
    assert_difference('TurnosVehiculo.count', -1) do
      delete turno_vehiculo_url(@turno_vehiculo), as: :json
    end

    assert_response 204
  end
end
