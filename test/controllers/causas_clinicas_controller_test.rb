require 'test_helper'

class CausasClinicasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @causa_clinica = causas_clinicas(:one)
  end

  test "should get index" do
    get causas_clinicas_url, as: :json
    assert_response :success
  end

  test "should create causa_clinica" do
    assert_difference('CausasClinica.count') do
      post causas_clinicas_url, params: { causa_clinica: { activo: @causa_clinica.activo, nombre: @causa_clinica.nombre } }, as: :json
    end

    assert_response 201
  end

  test "should show causa_clinica" do
    get causa_clinica_url(@causa_clinica), as: :json
    assert_response :success
  end

  test "should update causa_clinica" do
    patch causa_clinica_url(@causa_clinica), params: { causa_clinica: { activo: @causa_clinica.activo, nombre: @causa_clinica.nombre } }, as: :json
    assert_response 200
  end

  test "should destroy causa_clinica" do
    assert_difference('CausasClinica.count', -1) do
      delete causa_clinica_url(@causa_clinica), as: :json
    end

    assert_response 204
  end
end
