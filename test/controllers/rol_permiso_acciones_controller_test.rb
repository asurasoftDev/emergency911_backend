require 'test_helper'

class RolPermisoAccionesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rol_permiso_accione = rol_permiso_acciones(:one)
  end

  test "should get index" do
    get rol_permiso_acciones_url, as: :json
    assert_response :success
  end

  test "should create rol_permiso_accione" do
    assert_difference('RolPermisoAccione.count') do
      post rol_permiso_acciones_url, params: { rol_permiso_accione: { activo: @rol_permiso_accione.activo, permiso_id: @rol_permiso_accione.permiso_id, role_id: @rol_permiso_accione.role_id } }, as: :json
    end

    assert_response 201
  end

  test "should show rol_permiso_accione" do
    get rol_permiso_accione_url(@rol_permiso_accione), as: :json
    assert_response :success
  end

  test "should update rol_permiso_accione" do
    patch rol_permiso_accione_url(@rol_permiso_accione), params: { rol_permiso_accione: { activo: @rol_permiso_accione.activo, permiso_id: @rol_permiso_accione.permiso_id, role_id: @rol_permiso_accione.role_id } }, as: :json
    assert_response 200
  end

  test "should destroy rol_permiso_accione" do
    assert_difference('RolPermisoAccione.count', -1) do
      delete rol_permiso_accione_url(@rol_permiso_accione), as: :json
    end

    assert_response 204
  end
end
