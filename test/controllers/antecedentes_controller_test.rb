require 'test_helper'

class AntecedentesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @antecedente = antecedentes(:one)
  end

  test "should get index" do
    get antecedentes_url, as: :json
    assert_response :success
  end

  test "should create antecedente" do
    assert_difference('Antecedente.count') do
      post antecedentes_url, params: { antecedente: { activo: @antecedente.activo, nombre: @antecedente.nombre } }, as: :json
    end

    assert_response 201
  end

  test "should show antecedente" do
    get antecedente_url(@antecedente), as: :json
    assert_response :success
  end

  test "should update antecedente" do
    patch antecedente_url(@antecedente), params: { antecedente: { activo: @antecedente.activo, nombre: @antecedente.nombre } }, as: :json
    assert_response 200
  end

  test "should destroy antecedente" do
    assert_difference('Antecedente.count', -1) do
      delete antecedente_url(@antecedente), as: :json
    end

    assert_response 204
  end
end
