require 'test_helper'

class VehiculoAsignadosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @vehiculo_asignado = vehiculo_asignados(:one)
  end

  test "should get index" do
    get vehiculo_asignados_url, as: :json
    assert_response :success
  end

  test "should create vehiculo_asignado" do
    assert_difference('VehiculoAsignado.count') do
      post vehiculo_asignados_url, params: { vehiculo_asignado: { activo: @vehiculo_asignado.activo, emergencia_id: @vehiculo_asignado.emergencia_id, turno_vehiculo_id: @vehiculo_asignado.turno_vehiculo_id } }, as: :json
    end

    assert_response 201
  end

  test "should show vehiculo_asignado" do
    get vehiculo_asignado_url(@vehiculo_asignado), as: :json
    assert_response :success
  end

  test "should update vehiculo_asignado" do
    patch vehiculo_asignado_url(@vehiculo_asignado), params: { vehiculo_asignado: { activo: @vehiculo_asignado.activo, emergencia_id: @vehiculo_asignado.emergencia_id, turno_vehiculo_id: @vehiculo_asignado.turno_vehiculo_id } }, as: :json
    assert_response 200
  end

  test "should destroy vehiculo_asignado" do
    assert_difference('VehiculoAsignado.count', -1) do
      delete vehiculo_asignado_url(@vehiculo_asignado), as: :json
    end

    assert_response 204
  end
end
