require 'test_helper'

class TandasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tanda = tandas(:one)
  end

  test "should get index" do
    get tandas_url, as: :json
    assert_response :success
  end

  test "should create tanda" do
    assert_difference('Tanda.count') do
      post tandas_url, params: { tanda: { activo: @tanda.activo, cargo_id: @tanda.cargo_id, dia_id: @tanda.dia_id, hora_fin: @tanda.hora_fin, hora_inicio: @tanda.hora_inicio } }, as: :json
    end

    assert_response 201
  end

  test "should show tanda" do
    get tanda_url(@tanda), as: :json
    assert_response :success
  end

  test "should update tanda" do
    patch tanda_url(@tanda), params: { tanda: { activo: @tanda.activo, cargo_id: @tanda.cargo_id, dia_id: @tanda.dia_id, hora_fin: @tanda.hora_fin, hora_inicio: @tanda.hora_inicio } }, as: :json
    assert_response 200
  end

  test "should destroy tanda" do
    assert_difference('Tanda.count', -1) do
      delete tanda_url(@tanda), as: :json
    end

    assert_response 204
  end
end
