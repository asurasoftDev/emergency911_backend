require 'test_helper'

class AgentesCasualesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @agentes_casuale = agentes_casuales(:one)
  end

  test "should get index" do
    get agentes_casuales_url, as: :json
    assert_response :success
  end

  test "should create agentes_casuale" do
    assert_difference('AgentesCasuale.count') do
      post agentes_casuales_url, params: { agentes_casuale: { activo: @agentes_casuale.activo, causa_id: @agentes_casuale.causa_id, ficha_clinica_id: @agentes_casuale.ficha_clinica_id } }, as: :json
    end

    assert_response 201
  end

  test "should show agentes_casuale" do
    get agentes_casuale_url(@agentes_casuale), as: :json
    assert_response :success
  end

  test "should update agentes_casuale" do
    patch agentes_casuale_url(@agentes_casuale), params: { agentes_casuale: { activo: @agentes_casuale.activo, causa_id: @agentes_casuale.causa_id, ficha_clinica_id: @agentes_casuale.ficha_clinica_id } }, as: :json
    assert_response 200
  end

  test "should destroy agentes_casuale" do
    assert_difference('AgentesCasuale.count', -1) do
      delete agentes_casuale_url(@agentes_casuale), as: :json
    end

    assert_response 204
  end
end
