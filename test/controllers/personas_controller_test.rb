require 'test_helper'

class PersonasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @persona = personas(:one)
  end

  test "should get index" do
    get personas_url, as: :json
    assert_response :success
  end

  test "should create persona" do
    assert_difference('Persona.count') do
      post personas_url, params: { persona: { activo: @persona.activo, apellidos: @persona.apellidos, cedula: @persona.cedula, ciudad_id: @persona.ciudad_id, edad: @persona.edad, nombres: @persona.nombres, numero_de_seguro: @persona.numero_de_seguro, sexo: @persona.sexo, telefono_1: @persona.telefono_1, telefono_2: @persona.telefono_2 } }, as: :json
    end

    assert_response 201
  end

  test "should show persona" do
    get persona_url(@persona), as: :json
    assert_response :success
  end

  test "should update persona" do
    patch persona_url(@persona), params: { persona: { activo: @persona.activo, apellidos: @persona.apellidos, cedula: @persona.cedula, ciudad_id: @persona.ciudad_id, edad: @persona.edad, nombres: @persona.nombres, numero_de_seguro: @persona.numero_de_seguro, sexo: @persona.sexo, telefono_1: @persona.telefono_1, telefono_2: @persona.telefono_2 } }, as: :json
    assert_response 200
  end

  test "should destroy persona" do
    assert_difference('Persona.count', -1) do
      delete persona_url(@persona), as: :json
    end

    assert_response 204
  end
end
