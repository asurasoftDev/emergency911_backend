require 'test_helper'

class EstadosObstetricosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @estados_obstetrico = estados_obstetricos(:one)
  end

  test "should get index" do
    get estados_obstetricos_url, as: :json
    assert_response :success
  end

  test "should create estados_obstetrico" do
    assert_difference('EstadosObstetrico.count') do
      post estados_obstetricos_url, params: { estados_obstetrico: { activo: @estados_obstetrico.activo, embaraso_estado_id: @estados_obstetrico.embaraso_estado_id, obstetrico_id: @estados_obstetrico.obstetrico_id } }, as: :json
    end

    assert_response 201
  end

  test "should show estados_obstetrico" do
    get estados_obstetrico_url(@estados_obstetrico), as: :json
    assert_response :success
  end

  test "should update estados_obstetrico" do
    patch estados_obstetrico_url(@estados_obstetrico), params: { estados_obstetrico: { activo: @estados_obstetrico.activo, embaraso_estado_id: @estados_obstetrico.embaraso_estado_id, obstetrico_id: @estados_obstetrico.obstetrico_id } }, as: :json
    assert_response 200
  end

  test "should destroy estados_obstetrico" do
    assert_difference('EstadosObstetrico.count', -1) do
      delete estados_obstetrico_url(@estados_obstetrico), as: :json
    end

    assert_response 204
  end
end
