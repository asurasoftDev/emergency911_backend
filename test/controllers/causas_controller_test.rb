require 'test_helper'

class CausasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @causa = causas(:one)
  end

  test "should get index" do
    get causas_url, as: :json
    assert_response :success
  end

  test "should create causa" do
    assert_difference('Causa.count') do
      post causas_url, params: { causa: { activo: @causa.activo, nombre: @causa.nombre } }, as: :json
    end

    assert_response 201
  end

  test "should show causa" do
    get causa_url(@causa), as: :json
    assert_response :success
  end

  test "should update causa" do
    patch causa_url(@causa), params: { causa: { activo: @causa.activo, nombre: @causa.nombre } }, as: :json
    assert_response 200
  end

  test "should destroy causa" do
    assert_difference('Causa.count', -1) do
      delete causa_url(@causa), as: :json
    end

    assert_response 204
  end
end
