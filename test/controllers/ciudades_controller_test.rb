require 'test_helper'

class CiudadesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ciudad = ciudades(:one)
  end

  test "should get index" do
    get ciudades_url, as: :json
    assert_response :success
  end

  test "should create ciudad" do
    assert_difference('Ciudad.count') do
      post ciudades_url, params: { ciudad: { activo: @ciudad.activo, nombre: @ciudad.nombre } }, as: :json
    end

    assert_response 201
  end

  test "should show ciudad" do
    get ciudad_url(@ciudad), as: :json
    assert_response :success
  end

  test "should update ciudad" do
    patch ciudad_url(@ciudad), params: { ciudad: { activo: @ciudad.activo, nombre: @ciudad.nombre } }, as: :json
    assert_response 200
  end

  test "should destroy ciudad" do
    assert_difference('Ciudad.count', -1) do
      delete ciudad_url(@ciudad), as: :json
    end

    assert_response 204
  end
end
