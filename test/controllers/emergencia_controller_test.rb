require 'test_helper'

class EmergenciaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @emergencium = emergencia(:one)
  end

  test "should get index" do
    get emergencia_url, as: :json
    assert_response :success
  end

  test "should create emergencium" do
    assert_difference('Emergencium.count') do
      post emergencia_url, params: { emergencium: { activo: @emergencium.activo, causa_id: @emergencium.causa_id, ciudad_id: @emergencium.ciudad_id, descripcion: @emergencium.descripcion, lugar_del_evento: @emergencium.lugar_del_evento, sector: @emergencium.sector, telefono_infomante: @emergencium.telefono_infomante } }, as: :json
    end

    assert_response 201
  end

  test "should show emergencium" do
    get emergencium_url(@emergencium), as: :json
    assert_response :success
  end

  test "should update emergencium" do
    patch emergencium_url(@emergencium), params: { emergencium: { activo: @emergencium.activo, causa_id: @emergencium.causa_id, ciudad_id: @emergencium.ciudad_id, descripcion: @emergencium.descripcion, lugar_del_evento: @emergencium.lugar_del_evento, sector: @emergencium.sector, telefono_infomante: @emergencium.telefono_infomante } }, as: :json
    assert_response 200
  end

  test "should destroy emergencium" do
    assert_difference('Emergencium.count', -1) do
      delete emergencium_url(@emergencium), as: :json
    end

    assert_response 204
  end
end
