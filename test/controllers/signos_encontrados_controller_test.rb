require 'test_helper'

class SignosEncontradosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @signo_encontrado = signos_encontrados(:one)
  end

  test "should get index" do
    get signos_encontrados_url, as: :json
    assert_response :success
  end

  test "should create signo_encontrado" do
    assert_difference('SignosEncontrado.count') do
      post signos_encontrados_url, params: { signo_encontrado: { activo: @signo_encontrado.activo, ficha_clinica_id: @signo_encontrado.ficha_clinica_id, signo_vital_id: @signo_encontrado.signo_vital_id } }, as: :json
    end

    assert_response 201
  end

  test "should show signo_encontrado" do
    get signo_encontrado_url(@signo_encontrado), as: :json
    assert_response :success
  end

  test "should update signo_encontrado" do
    patch signo_encontrado_url(@signo_encontrado), params: { signo_encontrado: { activo: @signo_encontrado.activo, ficha_clinica_id: @signo_encontrado.ficha_clinica_id, signo_vital_id: @signo_encontrado.signo_vital_id } }, as: :json
    assert_response 200
  end

  test "should destroy signo_encontrado" do
    assert_difference('SignosEncontrado.count', -1) do
      delete signo_encontrado_url(@signo_encontrado), as: :json
    end

    assert_response 204
  end
end
