require 'test_helper'

class TurnosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @turno = turnos(:one)
  end

  test "should get index" do
    get turnos_url, as: :json
    assert_response :success
  end

  test "should create turno" do
    assert_difference('Turno.count') do
      post turnos_url, params: { turno: { activo: @turno.activo, tanda_id: @turno.tanda_id, user_id: @turno.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show turno" do
    get turno_url(@turno), as: :json
    assert_response :success
  end

  test "should update turno" do
    patch turno_url(@turno), params: { turno: { activo: @turno.activo, tanda_id: @turno.tanda_id, user_id: @turno.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy turno" do
    assert_difference('Turno.count', -1) do
      delete turno_url(@turno), as: :json
    end

    assert_response 204
  end
end
