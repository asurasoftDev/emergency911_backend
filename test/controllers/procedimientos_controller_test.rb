require 'test_helper'

class ProcedimientosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @procedimiento = procedimientos(:one)
  end

  test "should get index" do
    get procedimientos_url, as: :json
    assert_response :success
  end

  test "should create procedimiento" do
    assert_difference('Procedimiento.count') do
      post procedimientos_url, params: { procedimiento: { activo: @procedimiento.activo, nombre: @procedimiento.nombre } }, as: :json
    end

    assert_response 201
  end

  test "should show procedimiento" do
    get procedimiento_url(@procedimiento), as: :json
    assert_response :success
  end

  test "should update procedimiento" do
    patch procedimiento_url(@procedimiento), params: { procedimiento: { activo: @procedimiento.activo, nombre: @procedimiento.nombre } }, as: :json
    assert_response 200
  end

  test "should destroy procedimiento" do
    assert_difference('Procedimiento.count', -1) do
      delete procedimiento_url(@procedimiento), as: :json
    end

    assert_response 204
  end
end
