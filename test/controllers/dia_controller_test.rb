require 'test_helper'

class DiaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @dium = dia(:one)
  end

  test "should get index" do
    get dia_url, as: :json
    assert_response :success
  end

  test "should create dium" do
    assert_difference('Dium.count') do
      post dia_url, params: { dium: { activo: @dium.activo, nombre: @dium.nombre } }, as: :json
    end

    assert_response 201
  end

  test "should show dium" do
    get dium_url(@dium), as: :json
    assert_response :success
  end

  test "should update dium" do
    patch dium_url(@dium), params: { dium: { activo: @dium.activo, nombre: @dium.nombre } }, as: :json
    assert_response 200
  end

  test "should destroy dium" do
    assert_difference('Dium.count', -1) do
      delete dium_url(@dium), as: :json
    end

    assert_response 204
  end
end
