require 'test_helper'

class ExploracionesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @exploracion = exploraciones(:one)
  end

  test "should get index" do
    get exploraciones_url, as: :json
    assert_response :success
  end

  test "should create exploracion" do
    assert_difference('Exploracion.count') do
      post exploraciones_url, params: { exploracion: { activo: @exploracion.activo, nombre: @exploracion.nombre, tipo: @exploracion.tipo } }, as: :json
    end

    assert_response 201
  end

  test "should show exploracion" do
    get exploracion_url(@exploracion), as: :json
    assert_response :success
  end

  test "should update exploracion" do
    patch exploracion_url(@exploracion), params: { exploracion: { activo: @exploracion.activo, nombre: @exploracion.nombre, tipo: @exploracion.tipo } }, as: :json
    assert_response 200
  end

  test "should destroy exploracion" do
    assert_difference('Exploracion.count', -1) do
      delete exploracion_url(@exploracion), as: :json
    end

    assert_response 204
  end
end
