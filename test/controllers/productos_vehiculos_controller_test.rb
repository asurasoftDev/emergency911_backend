require 'test_helper'

class ProductosVehiculosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @producto_vehiculo = productos_vehiculos(:one)
  end

  test "should get index" do
    get productos_vehiculos_url, as: :json
    assert_response :success
  end

  test "should create producto_vehiculo" do
    assert_difference('ProductosVehiculo.count') do
      post productos_vehiculos_url, params: { producto_vehiculo: { activo: @producto_vehiculo.activo, cant_actual: @producto_vehiculo.cant_actual, cant_minima: @producto_vehiculo.cant_minima, producto_id: @producto_vehiculo.producto_id, vehiculo_id: @producto_vehiculo.vehiculo_id } }, as: :json
    end

    assert_response 201
  end

  test "should show producto_vehiculo" do
    get producto_vehiculo_url(@producto_vehiculo), as: :json
    assert_response :success
  end

  test "should update producto_vehiculo" do
    patch producto_vehiculo_url(@producto_vehiculo), params: { producto_vehiculo: { activo: @producto_vehiculo.activo, cant_actual: @producto_vehiculo.cant_actual, cant_minima: @producto_vehiculo.cant_minima, producto_id: @producto_vehiculo.producto_id, vehiculo_id: @producto_vehiculo.vehiculo_id } }, as: :json
    assert_response 200
  end

  test "should destroy producto_vehiculo" do
    assert_difference('ProductosVehiculo.count', -1) do
      delete producto_vehiculo_url(@producto_vehiculo), as: :json
    end

    assert_response 204
  end
end
