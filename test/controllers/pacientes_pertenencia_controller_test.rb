require 'test_helper'

class PacientesPertenenciaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pacientes_pertenencium = pacientes_pertenencia(:one)
  end

  test "should get index" do
    get pacientes_pertenencia_url, as: :json
    assert_response :success
  end

  test "should create pacientes_pertenencium" do
    assert_difference('PacientesPertenencium.count') do
      post pacientes_pertenencia_url, params: { pacientes_pertenencium: { activo: @pacientes_pertenencium.activo, descripcion: @pacientes_pertenencium.descripcion, ficha_clinica_id: @pacientes_pertenencium.ficha_clinica_id, quien_recibe: @pacientes_pertenencium.quien_recibe } }, as: :json
    end

    assert_response 201
  end

  test "should show pacientes_pertenencium" do
    get pacientes_pertenencium_url(@pacientes_pertenencium), as: :json
    assert_response :success
  end

  test "should update pacientes_pertenencium" do
    patch pacientes_pertenencium_url(@pacientes_pertenencium), params: { pacientes_pertenencium: { activo: @pacientes_pertenencium.activo, descripcion: @pacientes_pertenencium.descripcion, ficha_clinica_id: @pacientes_pertenencium.ficha_clinica_id, quien_recibe: @pacientes_pertenencium.quien_recibe } }, as: :json
    assert_response 200
  end

  test "should destroy pacientes_pertenencium" do
    assert_difference('PacientesPertenencium.count', -1) do
      delete pacientes_pertenencium_url(@pacientes_pertenencium), as: :json
    end

    assert_response 204
  end
end
