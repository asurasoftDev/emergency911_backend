

-- EDAD PROPEDIO DE PACIENTES POR CIUDAD
SELECT c.NOMBRE, (SUM(p.edad))/(COUNT(p.id)) as edad_promedio, COUNT(p.id) as total_pacientes
FROM ciudades as c
INNER JOIN personas as p
ON p.ciudad_id = c.id
GROUP BY  c.nombre;

-- PACIENTES POR GENERO POR CIUDAD
SELECT p.sexo, COUNT(p.sexo) as pacientes , c.nombre
FROM ciudades as c
INNER JOIN personas as p
ON p.ciudad_id = c.id
GROUP BY  c.nombre,p.sexo;

-- PROMEDIO PACIENTES POR GENERO POR CIUDAD


-- TOTAL DE EMPLEADOS POR CARGOS 
SELECT emp.cargo_id, COUNT(emp.cargo_id) as empleados , c.nombre
FROM cargos as c
INNER JOIN users as emp
ON emp.cargo_id = c.id
GROUP BY  c.nombre,emp.cargo_id;