class Tanda < ApplicationRecord
  belongs_to :cargo
  belongs_to :dia
  
  attribute :dia

  def self.validar(oTanda, nCargo, lUpdate=false)
    cWhere = ''
    nDiffHours = 4
    nDiffHours = 0 if ENV['RAILS_ENV'] == "production"
    if lUpdate
      puts :json => oTanda
      # puts :json => oTanda.id
      cWhere = " and t.id <> #{oTanda['id']}"
    end
    # return ActiveRecord::Base.connection.exec_query("select * from tandas t where t.cargo_id = #{nCargo} and t.dia_id = #{oTanda['dia_id']} and ('#{oTanda['hora_inicio']}'::time >= (t.hora_inicio + interval '#{nDiffHours}' HOUR) and '#{oTanda['hora_inicio']}'::time <= (t.hora_fin + interval '#{nDiffHours}' HOUR) ) and ('#{oTanda['hora_fin']}'::time > (t.hora_inicio + interval '#{nDiffHours}' HOUR) and '#{oTanda['hora_fin']}'::time <= (t.hora_fin + interval '#{nDiffHours}' HOUR) )").length > 0
    return ActiveRecord::Base.connection.exec_query("select * from tandas t where t.cargo_id = #{nCargo} and t.dia_id = #{oTanda['dia_id']} and ('#{oTanda['hora_inicio']}'::time >= (t.hora_inicio + interval '#{nDiffHours}' HOUR) and '#{oTanda['hora_inicio']}'::time <= (t.hora_fin + interval '#{nDiffHours}' HOUR) ) and ('#{oTanda['hora_fin']}'::time > (t.hora_inicio + interval '#{nDiffHours}' HOUR) and '#{oTanda['hora_fin']}'::time <= (t.hora_fin + interval '#{nDiffHours}' HOUR) )#{cWhere}").length > 0
  end

end
