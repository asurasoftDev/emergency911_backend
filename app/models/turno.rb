class Turno < ApplicationRecord
  belongs_to :user
  belongs_to :tanda
  
  attribute :tanda
  attribute :user

  def self.validar( tanda_id, user_id)
    self.where({user_id: user_id, tanda_id: tanda_id, activo: true}).length > 0
  end

  def self.getTurnosVehiculo( vehiculo_id )
    cWhere = ""
    unless vehiculo_id.nil?
      cWhere = "where tv.vehiculo_id = #{vehiculo_id}"
    end
    
    return ActiveRecord::Base.connection.exec_query("select t.*, ta.cargo_id, ta.dia_id from turnos t inner join tandas ta on ta.id = t.tanda_id where t.id in (select tv.id from turnos_vehiculos tv #{cWhere})")
  end

end
