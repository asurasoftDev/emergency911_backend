class TurnosVehiculo < ApplicationRecord
  belongs_to :vehiculo
  belongs_to :turno

  attribute :turno

  def self.validar(turno_id)
    self.where("turno_id = #{turno_id}").length > 0
  end

end
