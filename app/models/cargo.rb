class Cargo < ApplicationRecord
    has_many :users
    has_many :tandas
    validates :nombre, presence: {:message => "no puede estar vacio."}, uniqueness: { case_sensitive: false, :message => "ya existe"}
end
