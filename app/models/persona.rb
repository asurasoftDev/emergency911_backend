class Persona < ApplicationRecord
  belongs_to :ciudad

  has_one :user
  has_one :cargo, :through => :user#, foreign_key: "cargo_id"
  # has_one :cargo, foreign_key: "cargo_id"
end
