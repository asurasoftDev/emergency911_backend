class Vehiculo < ApplicationRecord
    has_many :turnos_vehiculos
    has_many :productos_vehiculo

    validates :modelo, presence: {:message => "no puede estar vacio."}
    validates :marca, presence: {:message => "no puede estar vacio."}
    validates :anio, presence: {:message => "no puede estar vacio."}
    validates :placa, presence: {:message => "no puede estar vacio."}, uniqueness: { case_sensitive: false, :message => "ya existe"}

    attribute :productos_vehiculo
end
