class Dia < ApplicationRecord
    validates :nombre, presence: {:message => "no puede estar vacio."}, uniqueness: { case_sensitive: false, :message => "ya existe"}
end
