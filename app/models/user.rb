# frozen_string_literal: true

class User < ActiveRecord::Base
  rolify
	extend Devise::Models
  belongs_to :cargo, optional: true
  belongs_to :persona, optional: true
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User

  validates :nickname, presence: {:message => "no puede estar vacio."}, uniqueness: { case_sensitive: false, :message => "ya existe"}
  validates :email, presence: false

  attribute :roles
  
  has_many :turnos
  has_many :tandas, :through => :turnos

  attribute :cargo
  attribute :persona
end
