class Producto < ApplicationRecord
  validates :nombre, presence: {:message => "no puede estar vacio."}, uniqueness: { case_sensitive: false, :message => "ya existe"}

  def change_cantidad(nCantidad)
    self.cantidad += nCantidad
    self.save 
  end
end
