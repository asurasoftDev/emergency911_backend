class ExploracionesResultado < ApplicationRecord
  belongs_to :exploracion
  belongs_to :area_fisica
  belongs_to :ficha_clinica
end
