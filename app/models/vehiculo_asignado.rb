class VehiculoAsignado < ApplicationRecord
  has_many :ficha_clinica, dependent: :destroy
  belongs_to :turnos_vehiculo
  belongs_to :emergencia

  attribute :turnos_vehiculo
end
