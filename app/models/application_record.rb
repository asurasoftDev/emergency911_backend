class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def self.getAction(sController, sAction)
  	action = self.allPermisos

    unless action[:"#{sController}"].nil?
      unless action[:"#{sController}"][:"#{sAction}"].nil?
          return {permiso: action[:"#{sController}"][:name], accion: action[:"#{sController}"][:"#{sAction}"]}
      else
        return nil
      end
    else
      return nil
    end
  end

# # ========================== Emergencias ========================== #
# puts "creacion de Emergencias => ", :json => PermisoAccione.create([
# 	{nombre: 'Ver listado Emergencias', permiso_id: Permiso.find_by_nombre('Emergencias').id},
# 	{nombre: 'Crear Emergencia', permiso_id: Permiso.find_by_nombre('Emergencias').id},
# 	{nombre: 'Eliminar Emergencia', permiso_id: Permiso.find_by_nombre('Emergencias').id}
# ])

# # ========================== Mis Emergencias ========================== #
# puts "creacion de Mis Emergencias => ", :json => PermisoAccione.create([
# 	{nombre: 'Ver listado de Mis Emergencias', permiso_id: Permiso.find_by_nombre('Mis Emergencias').id},
# 	{nombre: 'Agregar Ficha Paciente', permiso_id: Permiso.find_by_nombre('Mis Emergencias').id},
# 	{nombre: 'Ver Ubicacion Emergencia', permiso_id: Permiso.find_by_nombre('Mis Emergencias').id}
# ])

  def self.allPermisos
    {
      # fichas_clinicas: {
      #   name:"Mis Emergencias",
      #   crear: 'Crear Ficha Paciente',
      #   actualizar: 'Actualizar Ficha'
      # },
      emergencias: {
        name:                           "Mis Emergencias",
        emergenciasActivasDeAmbulancia: 'Ver listado de Mis Emergencias',
	  		detalleDeEmergencia:            'Ver listado Emergencias',
        create:                         'Crear Emergencia',
        delete:                         'Eliminar Emergencia'
      },
  		roles: {
        name:'Roles',
	  		index:   'Ver listado Roles', 
        show:    'Ver Rol', 
        create:  'Crear Rol', 
        update:  'Actualizar Rol', 
        delete:  'Eliminar Rol', 
      },
      hospitales: {
        name:'Hospitales',
        index:   'Ver listado Hospitales', 
        show:    'Ver Hospital', 
        create:  'Crear Hospital', 
        update:  'Actualizar Hospital', 
        delete:  'Eliminar Hospital', 
      },
      productos: {
        name:'Productos',
        index:   'Ver listado Productos', 
        show:    'Ver Producto', 
        create:  'Crear Producto', 
        update:  'Actualizar Producto', 
        delete:  'Eliminar Producto', 
      },
      productos_vehiculos: {
        name:'Productos Vehiculos',
        index:   'Ver listado Productos Vehiculos', 
        create:    'Asignar Productos a Vehiculos'
      },
      productos_vehiculos: {
        name:'Turnos de Vehiculos',
        index:   'Ver listado Turnos de Vehiculos', 
        create:  'Asignar Vehiculos a Turnos', 
        # create:  'Actualizar Turnos de Vehiculos', 
      },
      vehiculos: {
        name:'Vehiculos',
        index:   'Ver listado Vehiculos', 
        show:    'Ver Vehiculo', 
        create:  'Crear Vehiculo', 
        update:  'Actualizar Vehiculo', 
        delete:  'Eliminar Vehiculo'
      },
      turnos: {
        name:'Turnos',
        index:   'Ver listado Turnos', 
        show:    'Ver Turno', 
        create:  'Crear Turno', 
        update:  'Actualizar Turno', 
        delete:  'Eliminar Turno'
      },
      tandas: {
        name:'Tandas',
        index:   'Ver listado Tandas', 
        show:    'Ver Tanda', 
        create:  'Crear Tanda', 
        update:  'Actualizar Tanda', 
        delete:  'Eliminar Tanda', 
      },
      personas: {
        name:'Personas',
        index:   'Ver listado Personas', 
        show:    'Ver Persona', 
        create:  'Crear Persona', 
        update:  'Actualizar Persona', 
        delete:  'Eliminar Persona', 
      },
      cargos: {
        name:'Cargos',
        index:   'Ver listado Cargos', 
        show:    'Ver Cargo', 
        create:  'Crear Cargo', 
        update:  'Actualizar Cargo', 
        delete:  'Eliminar Cargo', 
      },
      compras: {
        name:'Compras',
        create:  'Crear Compra', 
      },
      dia: {
        name:'Calendario',
        calendario:  'Ver Calendario', 
      },
      reportes: {
        name: 'Reportes',
        productosMasConsumidos:                    'Productos mas consumidos',
        emergenciaPorFrecuencia:                   'Segmentar por periodos emergencia',
        ambulanciaMasUtilizadas:                   'Ambulancias mas utilizadas',
        reportEmpleadosPorCargo:                   'Empleados por Cargo',
        cantidadProductoEmergencia:                'Insumos por emergencia',
        pacientesFallecidosPorCiudad:              'Pacientes fallecidos por ciudad',
        reportComprasPorCiudadPorFecha:            'Compras por ciudad',
        reportEmergenciasPorDiaYPorNoche:          'Emergencia Dia y noche',
        reportPacientePorGeneroPorCiudad:          'Promedio de Accidentado por ciudad',
        cantidadProductoPorTipoDeEmergencia:       'Cantidad de producto por emergencia',
        reportEdadPromedioDePacientesPorCiudad:    'Edad Promedio por Ciudad',
        productosMasConsumidosPorTipoDeEmergencia: 'Productos mas consumidos por emergencia'
      },
    }
  end
end