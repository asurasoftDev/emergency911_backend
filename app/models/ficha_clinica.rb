class FichaClinica < ApplicationRecord
  belongs_to :persona
  belongs_to :vehiculo_asignado

  has_many :antecedentes_encontrados
  has_many :causas_traumaticas
  has_many :exploraciones_resultados
  has_many :obstetricos
  has_many :oringes_probables
  has_many :pacientes_pertenencia
  has_many :procedimientos_ejecutados
  has_many :vehiculos_fichas_historiales
end
