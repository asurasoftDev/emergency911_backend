class Emergencia < ApplicationRecord
  belongs_to :ciudad
  belongs_to :emergencia_tipo

  has_many :vehiculo_asignados, dependent: :destroy

  accepts_nested_attributes_for :vehiculo_asignados, allow_destroy: true

  validates :lat, presence: {:message => "no puede estar vacio."}
  validates :long, presence: {:message => "no puede estar vacio."}


  attribute :emergencia_tipo
end
