# frozen_string_literal: true

module DeviseTokenAuth
  class TokenValidationsController < DeviseTokenAuth::ApplicationController
    skip_before_action :assert_is_devise_resource!, only: [:validate_token]
    before_action :set_user_by_token, only: [:validate_token]

    def validate_token
      # @resource will have been set by set_user_by_token concern
      if @resource
        puts "prueba token_validations => ", :json => @resource
        if @resource[:estado] == 'I'
          user = remove_instance_variable(:@resource) if @resource
          client_id = remove_instance_variable(:@client_id) if @client_id
          remove_instance_variable(:@token) if @token

          if user && client_id && user.tokens[client_id]
            user.tokens.delete(client_id)
            user.save!

            yield user if block_given?

            # render_destroy_success
            return render json: {msg: "Usuario desactivado, favor de comunicarse con el administrador del sistema." }, status: 401
          end
        else
          yield @resource if block_given?
          render_validate_token_success
        end
      else
        render_validate_token_error
      end
    end

    protected

    def getPermisos(id)
      rol_actiones = ActiveRecord::Base.connection.exec_query("select pa.id, p.nombre, pa.nombre as accion from rol_permiso_acciones rpa inner join roles r on r.id = rpa.role_id inner join users_roles ur on ur.role_id = r.id inner join permiso_acciones pa on pa.id = rpa.permiso_accione_id inner join permisos p on p.id = pa.permiso_id where ur.user_id = #{id} and rpa.activo = true group by p.id, pa.id, ur.user_id")
  
      permisos = []
      rol_actiones.each do |action|
        existe = false
        permisos.each do |permiso|
          if permiso[:nombre] == action['nombre']
            existe = true
            permiso[:acciones].push({id:action['id'], nombre:action['accion']})
          end
        end
  
        unless existe
          # permisos.push({'nombre': action['nombre'], 'accion': [action['accion']]})
          permisos.push({'nombre': action['nombre'], 'acciones': [{id:action['id'], nombre:action['accion']}]})
        end
      end
      return permisos
    end

    def render_validate_token_success
      datos =  {
        success: true,
        data: resource_data(resource_json: @resource.token_validation_response)
      }
      datos[:data][:permisos] = getPermisos(datos[:data]['id'])
      puts "render_validate_token_success => ", datos.to_json
      render json: datos
    end

    def render_validate_token_error
      render_error(401, I18n.t('devise_token_auth.token_validations.invalid'))
    end
  end
end
