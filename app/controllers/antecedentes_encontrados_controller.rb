class AntecedentesEncontradosController < ApplicationController
  before_action :set_antecedente_encontrado, only: [:show, :update, :destroy]

  # GET /antecedentes_encontrados
  def index
    @antecedentes_encontrados = AntecedentesEncontrado.all

    render json: @antecedentes_encontrados
  end

  # GET /antecedentes_encontrados/1
  def show
    render json: @antecedente_encontrado
  end

  # POST /antecedentes_encontrados
  def create
    @antecedente_encontrado = AntecedentesEncontrado.new(antecedente_encontrado_params)

    if @antecedente_encontrado.save
      render json: @antecedente_encontrado, status: :created, location: @antecedente_encontrado
    else
      render json: @antecedente_encontrado.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /antecedentes_encontrados/1
  def update
    if @antecedente_encontrado.update(antecedente_encontrado_params)
      render json: @antecedente_encontrado
    else
      render json: @antecedente_encontrado.errors, status: :unprocessable_entity
    end
  end

  # DELETE /antecedentes_encontrados/1
  def destroy
    @antecedente_encontrado.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_antecedente_encontrado
      @antecedente_encontrado = AntecedentesEncontrado.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def antecedente_encontrado_params
      params.require(:antecedente_encontrado).permit(:antecedente_id, :ficha_clinica_id, :activo)
    end
end
