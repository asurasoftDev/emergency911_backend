class EstadosObstetricosController < ApplicationController
  before_action :set_estados_obstetrico, only: [:show, :update, :destroy]

  # GET /estados_obstetricos
  def index
    @estados_obstetricos = EstadosObstetrico.all

    render json: @estados_obstetricos
  end

  # GET /estados_obstetricos/1
  def show
    render json: @estados_obstetrico
  end

  # POST /estados_obstetricos
  def create
    @estados_obstetrico = EstadosObstetrico.new(estados_obstetrico_params)

    if @estados_obstetrico.save
      render json: @estados_obstetrico, status: :created, location: @estados_obstetrico
    else
      render json: @estados_obstetrico.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /estados_obstetricos/1
  def update
    if @estados_obstetrico.update(estados_obstetrico_params)
      render json: @estados_obstetrico
    else
      render json: @estados_obstetrico.errors, status: :unprocessable_entity
    end
  end

  # DELETE /estados_obstetricos/1
  def destroy
    @estados_obstetrico.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_estados_obstetrico
      @estados_obstetrico = EstadosObstetrico.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def estados_obstetrico_params
      params.require(:estados_obstetrico).permit(:obstetrico_id, :embaraso_estado_id, :activo)
    end
end
