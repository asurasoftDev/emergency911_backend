class PersonasController < ApplicationController
  before_action :set_persona, only: [:show, :update, :destroy]

  # GET /personas
  def index
    # @personas = []
    @personas = Persona.all
    # Persona.all.each do |persona|
    #   @personas.push({
    #     "ciudad_id": persona.ciudad_id,
    #     "nombres": persona.nombres,
    #     "apellidos": persona.apellidos,
    #     "cedula": persona.cedula,
    #     "edad": persona.edad,
    #     "sexo": persona.sexo,
    #     "telefono_1": persona.telefono_1,
    #     "telefono_2": persona.telefono_2,
    #     "numero_de_seguro": persona.numero_de_seguro,
    #     "activo": persona.activo
    #   })
    #   unless persona.user.nil?
    #     @personas[@personas.length-1]["cargo"] = Cargo.find_by_id(persona.user.cargo_id),
    #     @personas[@personas.length-1]["user"]  = persona.user
    #   end

    # end

    render json: @personas
  end

  # GET /personas/1
  def show
    puts "==="*30
    puts "===|@persona|"
    puts "#{@persona.to_json}"
    puts "==="*30
    persona_cargo = @persona.cargo
    persona_ciudad = @persona.ciudad
    oPersona = @persona.attributes
    oUser = User.find_by_persona_id(oPersona['id'])
    oPersona['roles'] = []
    if(!oUser.nil?)
      puts "==="*30
      puts "===|oUser|"
      puts "#{oUser.to_json}"
      puts "==="*30
      roles = ActiveRecord::Base.connection.exec_query("SELECT * FROM users_roles WHERE user_id=#{oUser['id']} ") #Role.where(user_id:oUser[:id])
      roles.each do | rol |
        oPersona['roles'].push(Role.find_by_id(rol['role_id']))
      end
      oPersona['cargo'] = persona_cargo
      oPersona['username'] = oUser.nickname
    end
    oPersona['ciudad'] = persona_ciudad
    # render json: @persona
    render json: oPersona
  end

  # POST /personas
  def create
    @persona = Persona.new(persona_params)

    if @persona.save
      unless params[:cargo_id].nil?
       crearUsuario
      else
        render json: {msg: "Persona registrada correctamente.", data:@persona}
      end
    else
      render json: {errors: @persona.errors, msg: "Error registrando la persona."}, status: 401
    end
  end

  def crearUsuario
    # username = @persona.nombres.split(" ")[0][0].downcase + @persona.apellidos.split(" ")[0].downcase
    username = @persona.nombres.gsub(" ",'_').downcase + @persona.apellidos.gsub(" ",'_').downcase
    # password = Devise.awesome_encrypting_method(params[:password])
    password = params[:password]
    puts "crearUsuario:username => ", username
    puts "crearUsuario:password => ", password
    user = User.new(uid: username, provider: "email", email: "#{username}@g.com", uid: username, cargo_id: params[:cargo_id], persona_id: @persona.id, nickname: params[:username], password: password, password_confirmation: password)
    # user.skip_confirmation!
    # user = User.create(nickname: username, provider: "nickname", uid: username, cargo_id: params[:cargo_id], persona_id: @persona.id)
    if user.save
      render json: {msg: "Empleado registrado correctamente."}
    else
      puts "crearUsuario:errors => ", user.errors.to_json
      render json: {errors: user.errors, msg: "Error registrando el empleado."}, status: 401
    end
    
  end

  # PATCH/PUT /personas/1
  def update
    lError = false
    aError = []
    if @persona.update(persona_params)
      puts "update persona 00"
      unless params[:cargo_id].nil?
        puts "update persona 22"
        user = User.find_by_persona_id(@persona.id)
        if user.nil?
          puts "update persona 44"
          crearUsuario
          return false
        else
          puts "update persona 55"
          if user.update({cargo_id: params[:cargo_id], password: params[:password], password_confirmation: params[:password]})
            sql = "DELETE FROM public.users_roles WHERE user_id=#{user[:id]};"
            ActiveRecord::Base.connection.exec_query(sql)
            puts "params[:roles] =>", params[:roles].to_json
            params[:roles].each do |role|
              sql = "INSERT INTO public.users_roles
              (user_id, role_id)
              VALUES(#{user[:id]}, #{role});
              "
              ActiveRecord::Base.connection.exec_query(sql)
            end

            lError = true
          else
            aError = user.errors
          end
        end
      else
        puts "update persona 33"
        lError = true
      end
    else
      puts "update persona 11"
      aError = @persona.errors
    end
    if lError
      render json: {msg: "Persona actulizada correctamente."}
    else
      render json: {errors: aError, msg: "Error al actulizar persona."}, status: 401
    end
  end

  # DELETE /personas/1
  def destroy
    # @persona.destroy

    if User.where({persona_id: @persona.id}).length > 0 || FichasClinica.where({persona_id: @persona.id}).length > 0
      render json: {msg: "otros registro dependen de esta persona, no puede ser eliminada"}, status: 401
    else
      if @persona.destroy
        render json: {msg: "Persona eliminada correctamente."}
      else
        render json: {error: "Error al eliminar persona."}, status: 401
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_persona
      @persona = Persona.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def persona_params
      params.require(:persona).permit(:ciudad_id, :nombres, :apellidos, :cedula, :edad, :sexo, :telefono_1, :telefono_2, :numero_de_seguro, :activo)
    end
end
