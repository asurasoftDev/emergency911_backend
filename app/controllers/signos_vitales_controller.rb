class SignosVitalesController < ApplicationController
  before_action :set_signo_vital, only: [:show, :update, :destroy]

  # GET /signos_vitales
  def index
    @signos_vitales = SignosVitale.all

    render json: @signos_vitales
  end

  # GET /signos_vitales/1
  def show
    render json: @signo_vital
  end

  # POST /signos_vitales
  def create
    @signo_vital = SignosVitale.new(signo_vital_params)

    if @signo_vital.save
      render json: @signo_vital, status: :created, location: @signo_vital
    else
      render json: @signo_vital.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /signos_vitales/1
  def update
    if @signo_vital.update(signo_vital_params)
      render json: @signo_vital
    else
      render json: @signo_vital.errors, status: :unprocessable_entity
    end
  end

  # DELETE /signos_vitales/1
  def destroy
    @signo_vital.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_signo_vital
      @signo_vital = SignosVitale.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def signo_vital_params
      params.require(:signo_vital).permit(:nombre, :activo)
    end
end
