class ComprasController < ApplicationController
  before_action :set_compra, only: [:show, :update, :destroy]

  # GET /compras
  def index
    @compras = Compra.all

    render json: @compras
  end

  def detalle_compra
    @compras = Compra.all
    aProductos = []
    aCompras = []
    @compras.each do |compra|
      oProducto = Producto.where(id: compra[:producto_id])
      aProductos.push(oProducto)
      oCompra = {}
      oCompra = compra.attributes
      aCompras.pus(oCompra)
    end
    render json: aCompras#@compras
  end

  # GET /compras/1
  def show
    render json: @compra
  end

  # POST /compras
  def create
    puts "compra_params => ", :json => compra_params[0]
    params["compras"].each do |compra|
      @compra = Compra.new({costo: compra['costo'].to_i, cantidad: compra['cantidad'].to_i, producto_id: compra['producto_id'],user_id:current_user.id})
      if compra['cantidad'].to_i > 0 && @compra.save
        producto = Producto.find_by_id(compra['producto_id'])
        producto.change_cantidad( compra['cantidad'].to_i )
        # producto.update({cantidad: (producto.cantidad || 0) + compra['cantidad'].to_i})
        # .update("cantidad = cantidad + #{compra['cantidad']}")
      else
        render json: {errors: @compra.errors, 'msg': 'error registando compra'}, status: 404
        return false
      end
    end

    render json: {msg: "Compras registadas correctamente."}
  end

  # PATCH/PUT /compras/1
  def update
    if @compra.update(compra_params)
      render json: @compra
    else
      render json: @compra.errors, status: :unprocessable_entity
    end
  end

  # DELETE /compras/1
  def destroy
    @compra.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_compra
      @compra = Compra.find_by_id(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def compra_params
      params.permit(:compra, keys: [:costo, :cantidad, :producto_id])
    end
end
