class TandasController < ApplicationController
  before_action :set_tanda, only: [:show, :update, :destroy]

  # GET /tandas
  def index
    @tandas = Tanda.all

    render json: @tandas
  end

  # GET /tandas/1
  def show
    render json: @tanda
  end

  # POST /tandas
  def create
    @lUpdate = false
    if validar()
      validar(true)
    end
  end

  # PATCH/PUT /tandas/1
  def update
    @lUpdate = true
    if validar()
      validar(true)
    end
  end

  # DELETE /tandas/1
  def destroy
    if Turno.where({tanda_id: @tanda.id}).length > 0
      render json: {msg: "otros registro dependen de esta tanda, no puede ser eliminada"}, status: 401
    else
      if @tanda.destroy
        render json: {msg: "Tanda eliminada correctamente."}
      else
        render json: {error: "Error al eliminar tanda."}
      end
    end
  end

  def validar(lCrear=false)
    cMsg1  = (@lUpdate ? "actualizadas" : "creada")
    cMsg2  = (@lUpdate ? "actualizando" : "creando")
    lResul = false
    tanda_params[:tandas].each do |tanda, index|
      unless lCrear
        if Tanda.validar(tanda, tanda_params[:cargo_id], @lUpdate)
          render json: {tanda: tanda, msg: "Conflicto con tanda existente."}, status: 300
          return false
        end
      else
        dataTemp = {
          cargo_id: tanda_params[:cargo_id], 
          descripcion: tanda_params[:descripcion], 
          activo: (tanda_params[:activo] || true), 
          dia_id: tanda['dia_id'], 
          hora_inicio: tanda['hora_inicio'], 
          hora_fin: tanda['hora_fin']
        }
        unless @lUpdate
          tandaTemp = Tanda.new(dataTemp)
          lResul = tandaTemp.save
        else
          tandaTemp = Tanda.find_by_id(tanda['id'])
          lResul = tandaTemp.update(dataTemp)
        end

        if lResul
          if index == (tanda_params[:tandas].length-1)
            render json: {msg: "Tandas #{cMsg1} correctamente."}, status: 200
          end
        else
          return render json: {errors: tandaTemp.errors, msg: "Error #{cMsg2} tandas."}, status: 404
        end
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tanda
      @tanda = Tanda.find_by_id(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def tanda_params
      params.require(:tanda).permit(:cargo_id, :activo, :descripcion, tandas: [:dia_id, :hora_inicio, :hora_fin, :id])
    end
end
