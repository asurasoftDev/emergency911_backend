class ObstetricosController < ApplicationController
  before_action :set_obstetrico, only: [:show, :update, :destroy]

  # GET /obstetricos
  def index
    @obstetricos = Obstetrico.all

    render json: @obstetricos
  end

  # GET /obstetricos/1
  def show
    render json: @obstetrico
  end

  # POST /obstetricos
  def create
    @obstetrico = Obstetrico.new(obstetrico_params)

    if @obstetrico.save
      render json: @obstetrico, status: :created, location: @obstetrico
    else
      render json: @obstetrico.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /obstetricos/1
  def update
    if @obstetrico.update(obstetrico_params)
      render json: @obstetrico
    else
      render json: @obstetrico.errors, status: :unprocessable_entity
    end
  end

  # DELETE /obstetricos/1
  def destroy
    @obstetrico.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_obstetrico
      @obstetrico = Obstetrico.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def obstetrico_params
      params.require(:obstetrico).permit(:ficha_clinica_id, :nombre, :activo)
    end
end
