class CausasClinicasController < ApplicationController
  before_action :set_causa_clinica, only: [:show, :update, :destroy]

  # GET /causas_clinicas
  def index
    @causas_clinicas = CausasClinica.all

    render json: @causas_clinicas
  end

  # GET /causas_clinicas/1
  def show
    render json: @causa_clinica
  end

  # POST /causas_clinicas
  def create
    @causa_clinica = CausasClinica.new(causa_clinica_params)

    if @causa_clinica.save
      render json: @causa_clinica, status: :created, location: @causa_clinica
    else
      render json: @causa_clinica.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /causas_clinicas/1
  def update
    if @causa_clinica.update(causa_clinica_params)
      render json: @causa_clinica
    else
      render json: @causa_clinica.errors, status: :unprocessable_entity
    end
  end

  # DELETE /causas_clinicas/1
  def destroy
    @causa_clinica.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_causa_clinica
      @causa_clinica = CausasClinica.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def causa_clinica_params
      params.require(:causa_clinica).permit(:nombre, :activo)
    end
end
