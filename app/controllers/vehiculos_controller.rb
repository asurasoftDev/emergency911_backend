class VehiculosController < ApplicationController
  before_action :set_vehiculo, only: [:show, :update, :destroy, :turnos]

  # GET /vehiculos
  def index
    getData()
    puts "@porductos_vehiculo_state => ", :json => @porductos_vehiculo_state
    render json: @porductos_vehiculo_state
  end

  # GET /vehiculos/:id/turnos
  def turnos
    # vehiculo = @vehiculo.attributes
    # vehiculo["turnos_vehiculos"] = getTurnosVehiculo(@vehiculo.id)
    
    # render json: vehiculo
    @turnosDisp = []
    vehiculo = @vehiculo.attributes
    
    Turno.getTurnosVehiculo(@vehiculo.id).each do |turno|
      # validarAgregar(turno)
      @turnosDisp.push(Turno.find_by_id(turno['id']))
    end

    vehiculo["turnos_vehiculos"] = @turnosDisp
    
    render json: vehiculo
  end

  def validarAgregar(lValidar=true, data)
    lExist = false
    lExistDia = false
    if @turnosDisp.length > 0
      @turnosDisp.each do |dia, index|
        @turnosDisp.push(Turno.find_by_id(data['id']))
      end
    end
  end

  def addDia(data)
    dia = Dia.find_by_id(data['dia_id'])
    oDia = dia.attributes
    oDia['turnos'] = [Turno.find_by_id(data['id'])]
    @turnosDisp.push(oDia)
  end

  # GET /vehiculos_turnos
  def vehiculos_turnos
    getData(true)
    render json: @porductos_vehiculo_state
  end

  def getData(lTurnos=false)
    @porductos_vehiculo_state = []
    Vehiculo.all.each do |vehiculo|
      veh = {}
      veh = vehiculo.attributes
      veh["porductos_vehiculo"] = ProductosVehiculo.where(vehiculo_id: vehiculo[:id])
      if lTurnos
        veh["turnos_vehiculos"] = getTurnosVehiculo(vehiculo[:id])
      end
      @porductos_vehiculo_state.push(veh)
    end
  end

  def getTurnosVehiculo(id)
    TurnosVehiculo.where(vehiculo_id: id)
  end

  # GET /vehiculos/1
  def show
    render json: @vehiculo
  end

  # POST /vehiculos
  def create
    @vehiculo = Vehiculo.new(vehiculo_params)

    if @vehiculo.save
      render json: @vehiculo, status: :created, location: @vehiculo
    else
      render json: @vehiculo.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /vehiculos/1
  def update
    if @vehiculo.update(vehiculo_params)
      render json: @vehiculo
    else
      render json: @vehiculo.errors, status: :unprocessable_entity
    end
  end

  # DELETE /vehiculos/1
  def destroy
    @vehiculo.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehiculo
      @vehiculo = Vehiculo.find_by_id(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def vehiculo_params
      params.require(:vehiculo).permit(:marca, :modelo, :placa, :anio, :alias, :ultimo_restablecimiento_de_invetario, :estado, :activo)
    end
end
