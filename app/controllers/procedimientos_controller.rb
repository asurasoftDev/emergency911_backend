class ProcedimientosController < ApplicationController
  before_action :set_procedimiento, only: [:show, :update, :destroy]

  # GET /procedimientos
  def index
    @procedimientos = Procedimiento.all

    render json: @procedimientos
  end

  # GET /procedimientos/1
  def show
    render json: @procedimiento
  end

  # POST /procedimientos
  def create
    @procedimiento = Procedimiento.new(procedimiento_params)

    if @procedimiento.save
      render json: @procedimiento, status: :created, location: @procedimiento
    else
      render json: @procedimiento.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /procedimientos/1
  def update
    if @procedimiento.update(procedimiento_params)
      render json: @procedimiento
    else
      render json: @procedimiento.errors, status: :unprocessable_entity
    end
  end

  # DELETE /procedimientos/1
  def destroy
    @procedimiento.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_procedimiento
      @procedimiento = Procedimiento.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def procedimiento_params
      params.require(:procedimiento).permit(:nombre, :activo)
    end
end
