class TurnosController < ApplicationController
  before_action :set_turno, only: [:show, :update, :destroy]

  # GET /turnos
  def index
    @turnos = Turno.all

    render json: @turnos
  end

  # GET /turnos/1
  def show
    render json: @turno
  end

  # GET /turnos_disponibles/:id_vehiculo
  def turnos_disponibles
    @turnosDisp = []
    # Turno.all.where('id NOT IN (?)', TurnosVehiculo.all.select(:turno_id)).each do |turno

    #   turnos
    # end
    turnos = ActiveRecord::Base.connection.exec_query("select t.*, ta.cargo_id, ta.dia_id from turnos t inner join tandas ta on ta.id = t.tanda_id where t.id not in (select tv.id from turnos_vehiculos tv)")

    turnos.each do |turno|
      validarAgregar(turno)
    end
    render json: @turnosDisp
    puts "day => ", Date.today.strftime("%A")
    puts "dia =>", I18n.t(Date.today.strftime("%A"), locale: :es)
  end

  def validarAgregar(lValidar=true, data)
    lExist = false
    lExistDia = false
    if @turnosDisp.length > 0
      @turnosDisp.each do |dia, index|
        if dia['id'] == data['dia_id']
          lExistDia = true
          dia['turnos'].each do |turno|
            if lValidar
              if turno['tanda_id'] == data['tanda_id'] && turno['user_id'] == data['user_id']
                lExist = true
              end
            end
          end
          unless lExist
            dia['turnos'].push(Turno.find_by_id(data['id']))
          end
        end
      end
      unless lExistDia
        addDia(data)
      end
    else
      addDia(data)
    end
  end

  def addDia(data)
    dia = Dia.find_by_id(data['dia_id'])
    oDia = dia.attributes
    oDia['turnos'] = [Turno.find_by_id(data['id'])]
    @turnosDisp.push(oDia)
  end

  # POST /turnos
  def create
    @datos_params = turno_params
    if validar()
      validar(true)
    end
  end

  # PATCH/PUT /turnos/1
  def update
    puts "turno_params on update => ", :json => params[:turno]
    data_insert = {activo: params[:turno][:activo]|| true, user_id: params[:turno][:user_id], tandas: []}
    data_delete = []
    params[:turno]["tandas"].each do |tanda, index|
      if tanda['accion'] == 'd'
        if TurnosVehiculo.where({turno_id: tanda['turno']}).length > 0
          render json: {msg: "otros registro dependen de este turno, no puede ser eliminado", status: :unprocessable_entity}, status: 401
          return
        else
          data_delete.push(tanda['turno'])
        end
      else
        if tanda['accion'] == 'i'
          @datos_params = {user_id: params[:turno][:user_id], tandas: [tanda['id']]}
          unless validar()
            return false
          else
            data_insert[:tandas].push(tanda['id'])
          end
        end
      end
    end
    
    data_delete.each do |turnoo|
      turnoTem = Turno.find_by_id(turnoo)
      turnoTem.destroy
    end

    puts "data_insert on update => ", :json => data_insert

    if data_insert[:tandas].length > 0
      @datos_params = data_insert
      validar(true, false)
    else
      render json: {msg: "Turnos actualizado correctamente."}, status: 200
    end
    # if @turno.update(turno_params)
    #   render json: @turno
    # else
    #   render json: @turno.errors, status: :unprocessable_entity
    # end
    # render json: []
  end

  # DELETE /turnos/1
  def destroy
    if TurnosVehiculo.where({turno_id: @turno.id}).length > 0
      render json: {msg: "otros registro dependen de este turno, no puede ser eliminado", status: :unprocessable_entity}, status: 401
    else
      if @turno.destroy
        render json: {msg: "Turno eliminado correctamente."}
      else
        render json: {error: "Error al eliminar turno."}, status: 401
      end
    end
  end

  def destroyTurno()
    aError = []
    Turno.where({user_id: turno_params[:user_id]}).each do |turno|
      if TurnosVehiculo.where({turno_id: turno.id}).length > 0
        unless turno.update({activo: false})
          aError.push(turno.errors)
        end
      else
        unless turno.destroy
          aError.push(turno.errors)
        end
      end
    end

    if aError.length > 0
      render json: {msg: "Error eliminando algunos turnos"}, status: 401
    else
      render json: {error: "Turnos eliminados correctamente."}
    end
  end

  def validar(lCrear=false, lMethodCreate=true)
    puts "params =>", :json => @datos_params
    @datos_params[:tandas].each do |tanda, index|
      unless lCrear
        if Turno.validar(tanda, @datos_params[:user_id])
          render json: {tanda: tanda, msg: "Ya existe un turno con esta tanda."}, status: 300
          return false
        end
      else
        turnoTemp = Turno.new({
          activo: (@datos_params[:activo] || true),
          tanda_id: tanda, 
          user_id: @datos_params[:user_id]
        })

        if turnoTemp.save
          if index == (@datos_params[:tandas].length-1)
            if lMethodCreate
              return render json: {msg: "Turnos creado correctamente."}, status: 200
            else
              return render json: {msg: "Turnos actualizado correctamente."}, status: 200
            end
          end
        else
          return render json: {errors: turnoTemp.errors, msg: "Error creando turnos."}, status: 404
        end
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_turno
      puts "|TURNO|=============================#{params[:id]}======"
      puts "#{Turno.find_by_id(params[:id]).to_json}"
      puts "=="*30
      @turno = Turno.find_by_id(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def turno_params
      params.require(:turno).permit(:id,:user_id, :activo, tandas: [])
    end
end
