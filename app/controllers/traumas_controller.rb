class TraumasController < ApplicationController
  before_action :set_trauma, only: [:show, :update, :destroy]

  # GET /traumas
  def index
    @traumas = Trauma.all

    render json: @traumas
  end

  # GET /traumas/1
  def show
    render json: @trauma
  end

  # POST /traumas
  def create
    @trauma = Trauma.new(trauma_params)

    if @trauma.save
      render json: @trauma, status: :created, location: @trauma
    else
      render json: @trauma.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /traumas/1
  def update
    if @trauma.update(trauma_params)
      render json: @trauma
    else
      render json: @trauma.errors, status: :unprocessable_entity
    end
  end

  # DELETE /traumas/1
  def destroy
    @trauma.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trauma
      @trauma = Trauma.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def trauma_params
      params.require(:trauma).permit(:nombre, :activo)
    end
end
