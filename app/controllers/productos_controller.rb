class ProductosController < ApplicationController
  before_action :set_producto, only: [:show, :update, :destroy]

  # GET /productos
  def index
    @productos = Producto.all

    render json: @productos
  end

  # GET /productos/1
  def show
    render json: @producto
  end

  # POST /productos
  def create
    @producto = Producto.new(producto_params)

    if @producto.save
      # render json: @producto, status: :created, location: @producto
      render json: {msg: "Producto registrado correctamente."}
    else
      render json: {error: @producto.errors, msg: "Error al registrar producto."}, status: 401
      # render json: @producto.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /productos/1
  def update
    if @producto.update(producto_params)
      # render json: @producto
      render json: {msg: "Producto actualizado correctamente."}
    else
      render json: {error: @producto.errors, msg: "Error al actualizar producto."}, status: 401
      # render json: @producto.errors, status: :unprocessable_entity
    end
  end

  # DELETE /productos/1
  def destroy
    # @producto.destroy

    if ProductsVehiculo.where({producto_id: @producto.id}).length > 0
      render json: {msg: "otros registro dependen de este producto, no puede ser eliminada"}, status: 401
    else
      if @persona.destroy
        render json: {msg: "Producto eliminado correctamente."}
      else
        render json: {error: @producto.errors, msg: "Error al eliminar producto."}, status: 401
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_producto
      @producto = Producto.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def producto_params
      params.require(:producto).permit(:nombre, :cant_minima, :activo)
    end
end
