class CargosController < ApplicationController
  before_action :set_cargo, only: [:show, :update, :destroy]

  # GET /cargos
  def index
    @cargos = Cargo.all

    render json: @cargos
  end

  # GET /cargos/1
  def show
    render json: @cargo
  end

  # POST /cargos
  def create
    @cargo = Cargo.new(cargo_params)

    if @cargo.save
      render json: {msg: "Cargo registado correctamente."}
      # render json: @cargo, status: :created, location: @cargo
    else
      render json: {errors: @cargo.errors, msg: "Error al registrar cargo."}
      # render json: @cargo.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /cargos/1
  def update
    if @cargo.update(cargo_params)
      render json: @cargo
    else
      render json: @cargo.errors, status: :unprocessable_entity
    end
  end

  # DELETE /cargos/1
  def destroy
    # @cargo.destroy

    if User.where({cargo_id: @cargo.id}).length > 0 || Tanda.where({cargo_id: @cargo.id}).length > 0
      render json: {msg: "otros registro dependen de este cargo, no puede ser eliminado", status: :unprocessable_entity}, status: 401
    else
      if @cargo.destroy
        render json: {msg: "Cargo eliminado correctamente."}
      else
        render json: {error: "Error al eliminar cargo."}
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cargo
      @cargo = Cargo.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cargo_params
      params.require(:cargo).permit(:nombre, :valor_del_dia_en_horas, :cant_dias_libres, :activo)
    end
end
