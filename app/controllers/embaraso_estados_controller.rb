class EmbarasoEstadosController < ApplicationController
  before_action :set_embaraso_estado, only: [:show, :update, :destroy]

  # GET /embaraso_estados
  def index
    @embaraso_estados = EmbarasoEstado.all

    render json: @embaraso_estados
  end

  # GET /embaraso_estados/1
  def show
    render json: @embaraso_estado
  end

  # POST /embaraso_estados
  def create
    @embaraso_estado = EmbarasoEstado.new(embaraso_estado_params)

    if @embaraso_estado.save
      render json: @embaraso_estado, status: :created, location: @embaraso_estado
    else
      render json: @embaraso_estado.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /embaraso_estados/1
  def update
    if @embaraso_estado.update(embaraso_estado_params)
      render json: @embaraso_estado
    else
      render json: @embaraso_estado.errors, status: :unprocessable_entity
    end
  end

  # DELETE /embaraso_estados/1
  def destroy
    @embaraso_estado.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_embaraso_estado
      @embaraso_estado = EmbarasoEstado.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def embaraso_estado_params
      params.require(:embaraso_estado).permit(:nombre, :activo)
    end
end
