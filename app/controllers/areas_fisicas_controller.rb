class AreasFisicasController < ApplicationController
  before_action :set_area_fisica, only: [:show, :update, :destroy]

  # GET /areas_fisicas
  def index
    @areas_fisicas = AreasFisica.all

    render json: @areas_fisicas
  end

  # GET /areas_fisicas/1
  def show
    render json: @area_fisica
  end

  # POST /areas_fisicas
  def create
    @area_fisica = AreasFisica.new(area_fisica_params)

    if @area_fisica.save
      render json: @area_fisica, status: :created, location: @area_fisica
    else
      render json: @area_fisica.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /areas_fisicas/1
  def update
    if @area_fisica.update(area_fisica_params)
      render json: @area_fisica
    else
      render json: @area_fisica.errors, status: :unprocessable_entity
    end
  end

  # DELETE /areas_fisicas/1
  def destroy
    @area_fisica.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_area_fisica
      @area_fisica = AreasFisica.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def area_fisica_params
      params.require(:area_fisica).permit(:nombre, :activo)
    end
end
