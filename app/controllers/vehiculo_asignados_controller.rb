class VehiculoAsignadosController < ApplicationController
  before_action :set_vehiculo_asignado, only: [:show, :update, :destroy]

  # GET /vehiculo_asignados
  def index
    @vehiculo_asignados = VehiculoAsignado.all

    render json: @vehiculo_asignados
  end

  # GET /vehiculo_asignados/1
  def show
    render json: @vehiculo_asignado
  end

  # POST /vehiculo_asignados
  def create
    @vehiculo_asignado = VehiculoAsignado.new(vehiculo_asignado_params)

    if @vehiculo_asignado.save
      render json: @vehiculo_asignado, status: :created, location: @vehiculo_asignado
    else
      render json: @vehiculo_asignado.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /vehiculo_asignados/1
  def update
    if @vehiculo_asignado.update(vehiculo_asignado_params)
      render json: @vehiculo_asignado
    else
      render json: @vehiculo_asignado.errors, status: :unprocessable_entity
    end
  end

  # DELETE /vehiculo_asignados/1
  def destroy
    @vehiculo_asignado.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehiculo_asignado
      @vehiculo_asignado = VehiculoAsignado.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def vehiculo_asignado_params
      params.require(:vehiculo_asignado).permit(:turno_vehiculo_id, :emergencia_id, :activo)
    end
end
