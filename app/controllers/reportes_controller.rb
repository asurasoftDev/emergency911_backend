class ReportesController < ApplicationController
#   before_action :set_emergencia, only: [:show, :update, :destroy]

  ## EDAD PROMEDIO DE PACIENTES POR CIUDAD
  def reportEdadPromedioDePacientesPorCiudad
    sql = "SELECT c.NOMBRE, (SUM(p.edad))/(COUNT(p.id)) as edad_promedio, COUNT(p.id) as total_pacientes
    FROM ciudades as c
    INNER JOIN personas as p
    ON p.ciudad_id = c.id
    GROUP BY  c.nombre;"
    data = ActiveRecord::Base.connection.execute(sql)
    puts " ------- #{data.to_json}"
    render json: data.to_json
  end

  ## PACIENTES POR GENERO POR CIUDAD
  def reportPacientePorGeneroPorCiudad
    sql = "SELECT p.sexo, COUNT(p.sexo) as pacientes , c.nombre
    FROM ciudades as c
    INNER JOIN personas as p
    ON p.ciudad_id = c.id
    GROUP BY  c.nombre,p.sexo;"
    data = ActiveRecord::Base.connection.execute(sql)
    puts " ------- #{data.to_json}"
    render json: data.to_json
  end

  ## EMPLEADOS POR CARGOS
  def reportEmpleadosPorCargo
    sql = "SELECT emp.cargo_id, COUNT(emp.cargo_id) as empleados , c.nombre
    FROM cargos as c
    INNER JOIN users as emp
    ON emp.cargo_id = c.id
    GROUP BY  c.nombre,emp.cargo_id;"
    data = ActiveRecord::Base.connection.execute(sql)
    puts " ------- #{data.to_json}"
    render json: data.to_json
  end

  ## EMPLEADOS POR CARGOS
  def cantidadProductoEmergencia
    puts "reporte_params 00 => ", :json => reporte_params
    sql = "select e.*, c.nombre ciudad ,  count(pc.id) variedad, coalesce(sum(pc.cant_utilizada),0) cantidad_productos from emergencia e
        inner join ciudades c on c.id = e.ciudad_id
        inner join vehiculo_asignados va on va.emergencia_id = e.id
        inner join ficha_clinicas fc on fc.vehiculo_asignado_id = va.id
        left join vehiculos_fichas_historiales vfh on vfh.ficha_clinica_id = fc.id
        left join productos_consumidos pc on pc.vehiculo_ficha_historial_id = vfh.id
        left join productos_vehiculos pv on pv.id = pc.producto_vehiculo_id
        where e.created_at::timestamp::date between '#{reporte_params[:fecha1]}' and '#{reporte_params[:fecha2]}'
        group by e.id, c.nombre"
        
    data = ActiveRecord::Base.connection.execute(sql)
    puts " ------- #{data.to_json}"
    render json: data.to_json
  end

  ## EMPLEADOS POR CARGOS
  def cantidadProductoPorTipoDeEmergencia
    puts "reporte_params 00 => ", :json => reporte_params
    sql = "select et.nombre tipo, c.nombre ciudad , count(pc.id) cantidad_productos from emergencia e
        inner join emergencia_tipos et on et.id = e.emergencia_tipo_id
        inner join ciudades c on c.id = e.ciudad_id
        inner join vehiculo_asignados va on va.emergencia_id = e.id
        inner join ficha_clinicas fc on fc.vehiculo_asignado_id = va.id
        left join vehiculos_fichas_historiales vfh on vfh.ficha_clinica_id = fc.id
        left join productos_consumidos pc on pc.vehiculo_ficha_historial_id = vfh.id
        left join productos_vehiculos pv on pv.id = pc.producto_vehiculo_id
        where e.created_at::timestamp::date between '#{reporte_params[:fecha1]}' and '#{reporte_params[:fecha2]}'
        group by et.id, c.nombre"
        
    data = ActiveRecord::Base.connection.execute(sql)
    puts " ------- #{data.to_json}"
    render json: data.to_json
  end

  ## PRODUCTOS MAS CONSUMIDOS
  def productosMasConsumidos
    puts "reporte_params 00 => ", :json => reporte_params
    sql = "select p.nombre, sum(pc.cant_utilizada) from productos_consumidos pc
            inner join productos_vehiculos pv on pv.id = pc.producto_vehiculo_id
            inner join productos p on p.id = pv.producto_id
            where pc.created_at::timestamp::date between '#{reporte_params[:fecha1]}' and '#{reporte_params[:fecha2]}'
            group by p.id
            order by sum(pc.cant_utilizada) desc limit 10"
        
    data = ActiveRecord::Base.connection.execute(sql)
    puts " ------- #{data.to_json}"
    render json: data.to_json
  end

  ## PROUDUCTOS MAS CONSUMIDOS POR TIPO_DE_EMERGENCIA
  def productosMasConsumidosPorTipoDeEmergencia
    aTipos = []
    tipos = EmergenciaTipo.all.where('id IN (?)', Emergencia.all.select(:emergencia_tipo_id) ).each do |tipo|
      oTipo = tipo.attributes
      sql = "select p.*, sum(pc.cant_utilizada) cant_utilizada from productos_consumidos pc
              inner join productos_vehiculos pv on pv.id = pc.producto_vehiculo_id
              inner join productos p on p.id = pv.producto_id
              inner join vehiculos_fichas_historiales vfh on vfh.id = pc.vehiculo_ficha_historial_id
              inner join ficha_clinicas fc on fc.id = vfh.ficha_clinica_id
              inner join vehiculo_asignados va on va.id = fc.vehiculo_asignado_id
              inner join emergencia e on e.id = va.emergencia_id
              inner join emergencia_tipos et on et.id = e.emergencia_tipo_id
              where et.id = #{oTipo["id"]} and pc.created_at::timestamp::date between '#{reporte_params[:fecha1]}' and '#{reporte_params[:fecha2]}'
              group by p.id
              order by sum(pc.cant_utilizada) desc
              limit 10"
      oTipo["productos_mas_consumidos"] = ActiveRecord::Base.connection.execute(sql)
      aTipos.push(oTipo)
    end
    
    render json: aTipos.to_json
  end

  ## AMBULANCIA MAS UTILIZADAS
  def ambulanciaMasUtilizadas
    puts "reporte_params 00 => ", :json => reporte_params
    sql = "select v.*, sum(va.id) cantidad from vehiculo_asignados va
            inner join turnos_vehiculos tv on tv.id = va.turnos_vehiculo_id
            inner join vehiculos v on v.id = tv.vehiculo_id
            where v.vehiculo_tipo = 'A' and va.created_at::timestamp::date between '#{reporte_params[:fecha1]}' and '#{reporte_params[:fecha2]}'
            group by v.id
            order by sum(va.id) desc limit 10"
        
    data = ActiveRecord::Base.connection.execute(sql)
    puts " ------- #{data.to_json}"
    render json: data.to_json
  end

  ## PACIENTES FALLECIDOS POR CIUDAD
  def pacientesFallecidosPorCiudad
    puts "reporte_params 00 => ", :json => reporte_params
    sql = "select c.*, count(fc.id) cantidad_pacientes_muertos from ficha_clinicas fc
            inner join vehiculo_asignados va on va.id = fc.vehiculo_asignado_id
            inner join emergencia e on e.id = va.emergencia_id
            inner join ciudades c on c.id = e.ciudad_id
            where lower(fc.triage_color) = 'negro' and fc.created_at::timestamp::date between '#{reporte_params[:fecha1]}' and '#{reporte_params[:fecha2]}'
            group by c.id
            order by count(fc.id) desc"
        
    data = ActiveRecord::Base.connection.execute(sql)
    puts " ------- #{data.to_json}"
    render json: data.to_json
  end

  ## HOSPITALES CON MAS FRECUENCIA DE ACCIDENTADOS INGREADOS
  def hospitalesConMasAccidentadosIngreados
    puts "reporte_params 00 => ", :json => reporte_params
    sql = "select h.*, count(fc.id) cantidad_pacientes from ficha_clinicas fc
            inner join hospitals h on h.id = fc.hospitals_id
            where fc.created_at::timestamp::date between '#{reporte_params[:fecha1]}' and '#{reporte_params[:fecha2]}'
            group by h.id
            order by count(fc.id) desc limit 10"
        
    data = ActiveRecord::Base.connection.execute(sql)
    puts " ------- #{data.to_json}"
    render json: data.to_json
  end

  ## PACIENTES VIVOS ENTREGADOS
  def pacientesVivosEntregados
    puts "reporte_params 00 => ", :json => reporte_params
    data = VehiculosFichasHistoriale.where("entregado_completado = true and created_at::timestamp::date between '#{params[:fecha1]}' and '#{params[:fecha2]}'").order(created_at: :desc).to_a
    puts " ------- #{data.to_json}"
    render json: data.to_json
  end

  def emergenciaPorFrecuencia
    nTipo = params[:tipo].to_i
    # sql = "select e.* from emergencia e 
    #       where e.created_at::timestamp::date between '#{params[:fecha1]}' and '#{params[:fecha2]}'"
    aData = Emergencia.all.where("created_at::timestamp::date between '#{params[:fecha1]}' and '#{params[:fecha2]}'").order(created_at: :desc).to_a
    hResult = {}
    aTipo = aData.group_by {|t| t.emergencia_tipo.nombre }
    aTipo.each do |key, oTipo|
      if nTipo >= 1 and nTipo <= 4
        hResult[key] = oTipo.group_by {|t| calFrecuencia( t ) }
      elsif nTipo == 5
        hResult[key] = oTipo.group_by {|t| t.created_at.year }
      end
    end
    puts " ------- #{hResult.to_json}"
    render json: hResult
  end
  
  def calFrecuencia( t )
    nMes = t.created_at.month.to_i
    hTipo = tipoFrecuencia(nMes)
    puts "hTipo => ", hTipo
    nGrupos = 12 / hTipo[:value]
    nInicio = 0
    loop do
      nAnterior = (if nInicio != 0 then nInicio-1 else 0 end)
      nInicio += 1
      if nMes > (nAnterior * hTipo[:value]) and nMes <= (nInicio * hTipo[:value])
        return hTipo[:text] + (if params[:tipo] == "1" then "" else " #{nInicio}" end )
      end
    end
  end

  def tipoFrecuencia(nMes)
    hTipos = {
      "1" => {value: 12, text: I18n.t("meses", locale: :es)[nMes]}, #mes
      "2" => {value: 3, text: "Trimestre"}, #trimestre
      "3" => {value: 4, text: "Cuatrimestre"}, #cuatrimestre
      "4" => {value: 6, text: "Semestre"}, #semestre
      "5" => {value: 1, text: "Anual"}, #anual
    }
    return hTipos[params[:tipo]] 
  end

    def reportComprasPorCiudadPorFecha
    compras_realizadas = []
    Compra.all.where("created_at between '#{params[:fecha1]}' and '#{params[:fecha2]}'").each do |compra, indix|
      usuario = User.find_by_id(compra.user_id)
      persona = Persona.find_by_id(usuario.persona_id)
      oCompra = {
        'persona_id' => usuario.persona_id,
        'user_id' => compra.user_id,
        'ciudad_id' => persona.ciudad_id,
        'compra_id' => compra.id,
        'producto' => compra.producto,
        'cantidad' => compra.cantidad,
        'costo' => compra.costo
      }
      compras_realizadas.push(oCompra)
      # puts "compra => ", compra.producto.to_json
    end
    ciudades = []
    Ciudad.all.each do |ciudad, indix|
      oCiudad = {
        'id' => ciudad[:id],
        'ciudad' => ciudad[:nombre],
        'total_en_compras':0,
        'compras'=>[]
      }
      compras_realizadas.each do |compra, indix|
        # puts "=====>#{compra['costo']} == #{oCiudad['total_en_compras']}"
        puts "=====> oCiudad ==> ", oCiudad.to_json
        puts "=====> compra ==> ", compra.to_json
        if(compra['ciudad_id'] == ciudad[:id])
          # _costo = compra['costo']
          # if(oCiudad['total_en_compras']==nil)
          #   oCiudad['total_en_compras'] = 0.0
          # end
          # if(compra['costo']==nil)
          #   compra['costo'] = 0.0
          # end
          oCiudad['total_en_compras'] = oCiudad['total_en_compras'].to_f + (compra['costo'].to_f || 0.0) *  compra['cantidad'].to_f # Float(oCiudad['total_en_compras']|| 0.0)
          oCiudad['compras'].push(compra)
        end
      end
      ciudades.push(oCiudad)
    end
    render json: ciudades.to_json
  end

    ## PACIENTES POR GENERO POR CIUDAD
  def reportPacientePorGeneroPorCiudad
    ciudades = []
    Ciudad.all().each do |ciudad,index|
      hombres = Persona.where({ciudad_id:ciudad[:id], sexo:'M'}).count
      mujeres = Persona.where({ciudad_id:ciudad[:id], sexo:'F'}).count

      # sql_manana = "SELECT count(id) as cantidad FROM emergencia where created_at::time <= '12:00:00' AND ciudad_id=#{ciudad[:id]} "
      # sql_tarde  = "SELECT count(id) as cantidad FROM emergencia where created_at::time >= '12:00:00' AND created_at::time <= '18:00:00' AND ciudad_id=#{ciudad[:id]} ";
      # sql_noche  = "SELECT count(id) as cantidad FROM emergencia where created_at::time >= '18:00:00' AND ciudad_id=#{ciudad[:id]} "
      
      # query_1 = ActiveRecord::Base.connection.execute(sql_manana)[0]
      # query_2 = ActiveRecord::Base.connection.execute(sql_tarde)[0]
      # query_3 = ActiveRecord::Base.connection.execute(sql_noche)[0]

      tipos = []
      tipo_de_emergencia_max_frecuente = ''
      cant_tipo_de_emergencia_max_frecuente = 0
      EmergenciaTipo.all().each do | tipo,indx|
        cantidad = Emergencia.where({ciudad_id:ciudad[:id], emergencia_tipo_id:tipo[:id]}).count
        oTipo = {
          'nombre'=>tipo.nombre,
          'cantidad'=>cantidad
        }
        if(cant_tipo_de_emergencia_max_frecuente < cantidad)
          cant_tipo_de_emergencia_max_frecuente = cantidad
          tipo_de_emergencia_max_frecuente = tipo.nombre
        end
        if( cantidad > 0 )
          tipos.push(oTipo)
        end
      end



      total = mujeres + hombres
      promedio_m = hombres.to_f / total *100
      promedio_f = mujeres.to_f / total *100
      oCiudad = {
        'nombre'=> ciudad[:nombre],
        'ciudad_id'=> ciudad[:ciudad_id],
        'M'=> hombres,
        'F'=> mujeres,
        'cant_emeregencias_en_manana'=>0,
        'cant_emeregencias_en_tarde'=>0,
        'cant_emeregencias_en_noche'=>0,
        'promedio_m'=> promedio_m,
        'promedio_f'=> promedio_f,
        'total_en_ciudad' => total,
        'tipos_de_emegencias_ocurridas'=> tipos,
        'cant_tipo_de_emergencia_max_frecuente'=>cant_tipo_de_emergencia_max_frecuente,
        'tipo_de_emergencia_max_frecuente'=>tipo_de_emergencia_max_frecuente
      }
      if(mujeres > 0 || hombres > 0)
        ciudades.push(oCiudad)
      end
    end
    sql = "SELECT p.sexo, COUNT(p.sexo) as pacientes , c.nombre
    # FROM ciudades as c
    # INNER JOIN personas as p
    # ON p.ciudad_id = c.id
    # GROUP BY  c.nombre,p.sexo;"
    # data = ActiveRecord::Base.connection.execute(sql)
    puts " ------- #{ciudades.to_json}"
    render json: ciudades.to_json
  end

  ## EMPLEADOS POR CARGOS
  def reportEmpleadosPorCargo
    sql = "SELECT emp.cargo_id, COUNT(emp.cargo_id) as empleados , c.nombre
    FROM cargos as c
    INNER JOIN users as emp
    ON emp.cargo_id = c.id
    GROUP BY  c.nombre,emp.cargo_id;"
    data = ActiveRecord::Base.connection.execute(sql)
    puts " ------- #{data.to_json}"
    render json: data.to_json
  end

  def reportEmergenciasPorDiaYPorNoche
    ciudades = []
    Ciudad.all.each do |ciudad, indix|
      sql_manana = "SELECT count(id) as cantidad FROM emergencia where created_at::time <= '12:00:00' AND ciudad_id=#{ciudad[:id]} and created_at between '#{params[:fecha1]}' and '#{params[:fecha2]}'"
      sql_tarde  = "SELECT count(id) as cantidad FROM emergencia where created_at::time >= '12:00:00' AND created_at::time <= '18:00:00' AND ciudad_id=#{ciudad[:id]} and created_at between '#{params[:fecha1]}' and '#{params[:fecha2]}'";
      sql_noche  = "SELECT count(id) as cantidad FROM emergencia where created_at::time >= '18:00:00' AND ciudad_id=#{ciudad[:id]} and created_at between '#{params[:fecha1]}' and '#{params[:fecha2]}'"
      query_1 = ActiveRecord::Base.connection.execute(sql_manana)[0]
      query_2 = ActiveRecord::Base.connection.execute(sql_tarde)[0]
      query_3 = ActiveRecord::Base.connection.execute(sql_noche)[0]
      data = { 'ciudad_id'=> ciudad[:id], 'ciudad'=> ciudad[:nombre], 'manana'=> query_1 , 'tarde' => query_2 ,'noche'=> query_3}
      ciudades.push(data)
    end
    render json: ciudades.to_json
  end

  ## EDAD PROMEDIO DE PACIENTES POR CIUDAD
  def reportEdadPromedioDePacientesPorCiudad
    sql = "SELECT c.NOMBRE, (SUM(p.edad))/(COUNT(p.id)) as edad_promedio, COUNT(p.id) as total_pacientes
    FROM ciudades as c
    INNER JOIN personas as p
    ON p.ciudad_id = c.id
    GROUP BY  c.nombre;"
    data = ActiveRecord::Base.connection.execute(sql)
    puts " ------- #{data.to_json}"
    render json: data.to_json
  end

  def lastRecordTables
    data = {}
    ActiveRecord::Base.connection.tables.each do |tabla|
      if tabla != "schema_migrations" and tabla != "ar_internal_metadata" and tabla != "users_roles"
        data[tabla] = ActiveRecord::Base.connection.exec_query("select * from #{tabla} order by id desc limit 1")
      end
    end

    # ActiveRecord::Base.subclasses.each{|x| puts "model(#{x.name}) => #{x.to_json.to_s}"}
    # puts "listTablas data => ", :json => data
    render json: data
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_emergencia
      @emergencia = Emergencia.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def reporte_params
      params.permit(:fecha1, :fecha2, :tipo)
    end
end
