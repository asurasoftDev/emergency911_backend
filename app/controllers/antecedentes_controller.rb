class AntecedentesController < ApplicationController
  before_action :set_antecedente, only: [:show, :update, :destroy]

  # GET /antecedentes
  def index
    @antecedentes = Antecedente.all

    render json: @antecedentes
  end

  # GET /antecedentes/1
  def show
    render json: @antecedente
  end

  # POST /antecedentes
  def create
    @antecedente = Antecedente.new(antecedente_params)

    if @antecedente.save
      render json: @antecedente, status: :created, location: @antecedente
    else
      render json: @antecedente.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /antecedentes/1
  def update
    if @antecedente.update(antecedente_params)
      render json: @antecedente
    else
      render json: @antecedente.errors, status: :unprocessable_entity
    end
  end

  # DELETE /antecedentes/1
  def destroy
    @antecedente.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_antecedente
      @antecedente = Antecedente.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def antecedente_params
      params.require(:antecedente).permit(:nombre, :activo)
    end
end
