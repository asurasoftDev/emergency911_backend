class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken

  # before_action :authenticate_user!, unless: :devise_controller?
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected
    def configure_permitted_parameters
      aData = [:name, :image, :email, :nickname, :password, :password_confirmation, :edad, :cedula, :nombres, :apellidos, :sexo, :telefono_1, :telefono_2, :numero_de_seguro]
      devise_parameter_sanitizer.permit(:sign_up, keys: aData)
      devise_parameter_sanitizer.permit(:account_update, keys: aData)
    end
end
