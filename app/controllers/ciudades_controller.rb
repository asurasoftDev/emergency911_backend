class CiudadesController < ApplicationController
  before_action :set_ciudad, only: [:show, :update, :destroy]

  # GET /ciudades
  def index
    @ciudades = Ciudad.all

    render json: @ciudades
  end

  # GET /ciudades/1
  def show
    render json: @ciudad
  end

  # POST /ciudades
  def create
    @ciudad = Ciudad.new(ciudad_params)

    if @ciudad.save
      render json: {msg: "Ciudad registrada correctamente."}
      # render json: @ciudad, status: :created, location: @ciudad
    else
      render json: {errors: @ciudad.errors, msg: "Error al  registrar ciudad."}, status: 401
      # render json: @ciudad.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /ciudades/1
  def update
    if @ciudad.update(ciudad_params)
      render json: {msg: "Ciudad actualizada correctamente."}
      # render json: @ciudad
    else
      render json: {errors: @ciudad.errors, msg: "Error al actulizar ciudad."}, status: 401
      # render json: @ciudad.errors, status: :unprocessable_entity
    end
  end

  # DELETE /ciudades/1
  def destroy
    # @ciudad.destroy

    if Persona.where({ciudad_id: @ciudad.id}).length > 0 || Emergencia.where({ciudad_id: @ciudad.id}).length > 0
      render json: {msg: "otros registro dependen de esta ciudad, no puede ser eliminado", status: :unprocessable_entity}, status: 401
    else
      if @ciudad.destroy
        render json: {msg: "Ciudad eliminada correctamente."}
      else
        render json: {error: "Error al eliminar ciudad."}, status: 401
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ciudad
      @ciudad = Ciudad.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ciudad_params
      params.require(:ciudad).permit(:nombre, :activo)
    end
end
