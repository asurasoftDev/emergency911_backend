class ProcedimientosEjecutadosController < ApplicationController
  before_action :set_procedimiento_ejecutado, only: [:show, :update, :destroy]

  # GET /procedimientos_ejecutados
  def index
    @procedimientos_ejecutados = ProcedimientosEjecutado.all

    render json: @procedimientos_ejecutados
  end

  # GET /procedimientos_ejecutados/1
  def show
    render json: @procedimiento_ejecutado
  end

  # POST /procedimientos_ejecutados
  def create
    @procedimiento_ejecutado = ProcedimientosEjecutado.new(procedimiento_ejecutado_params)

    if @procedimiento_ejecutado.save
      render json: @procedimiento_ejecutado, status: :created, location: @procedimiento_ejecutado
    else
      render json: @procedimiento_ejecutado.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /procedimientos_ejecutados/1
  def update
    if @procedimiento_ejecutado.update(procedimiento_ejecutado_params)
      render json: @procedimiento_ejecutado
    else
      render json: @procedimiento_ejecutado.errors, status: :unprocessable_entity
    end
  end

  # DELETE /procedimientos_ejecutados/1
  def destroy
    @procedimiento_ejecutado.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_procedimiento_ejecutado
      @procedimiento_ejecutado = ProcedimientosEjecutado.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def procedimiento_ejecutado_params
      params.require(:procedimiento_ejecutado).permit(:ficha_clinica_id, :procedimiento_id, :activo)
    end
end
