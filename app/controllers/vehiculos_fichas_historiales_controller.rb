class VehiculosFichasHistorialesController < ApplicationController
  before_action :set_vehiculo_ficha_historial, only: [:show, :update, :destroy]

  # GET /vehiculos_fichas_historiales
  def index
    @vehiculos_fichas_historiales = VehiculosFichasHistoriale.all

    render json: @vehiculos_fichas_historiales
  end

  # GET /vehiculos_fichas_historiales/1
  def show
    render json: @vehiculo_ficha_historial
  end

  # POST /vehiculos_fichas_historiales
  def create
    @vehiculo_ficha_historial = VehiculosFichasHistoriale.new(vehiculo_ficha_historial_params)

    if @vehiculo_ficha_historial.save
      render json: @vehiculo_ficha_historial, status: :created, location: @vehiculo_ficha_historial
    else
      render json: @vehiculo_ficha_historial.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /vehiculos_fichas_historiales/1
  def update
    if @vehiculo_ficha_historial.update(vehiculo_ficha_historial_params)
      render json: @vehiculo_ficha_historial
    else
      render json: @vehiculo_ficha_historial.errors, status: :unprocessable_entity
    end
  end

  # DELETE /vehiculos_fichas_historiales/1
  def destroy
    @vehiculo_ficha_historial.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehiculo_ficha_historial
      @vehiculo_ficha_historial = VehiculosFichasHistoriale.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def vehiculo_ficha_historial_params
      params.require(:vehiculo_ficha_historial).permit(:ficha_clinica_id, :turno_vehiculo_id, :activo)
    end
end
