class RolPermisoAccionesController < ApplicationController
  before_action :set_rol_permiso_accione, only: [:show, :update, :destroy]

  # GET /rol_permiso_acciones
  def index
    @rol_permiso_acciones = RolPermisoAccione.all

    render json: @rol_permiso_acciones
  end

  # GET /rol_permiso_acciones/1
  def show
    render json: @rol_permiso_accione
  end

  # POST /rol_permiso_acciones
  def create
    @rol_permiso_accione = RolPermisoAccione.new(rol_permiso_accione_params)

    if @rol_permiso_accione.save
      render json: @rol_permiso_accione, status: :created, location: @rol_permiso_accione
    else
      render json: @rol_permiso_accione.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /rol_permiso_acciones/1
  def update
    if @rol_permiso_accione.update(rol_permiso_accione_params)
      render json: @rol_permiso_accione
    else
      render json: @rol_permiso_accione.errors, status: :unprocessable_entity
    end
  end

  # DELETE /rol_permiso_acciones/1
  def destroy
    @rol_permiso_accione.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rol_permiso_accione
      @rol_permiso_accione = RolPermisoAccione.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def rol_permiso_accione_params
      params.require(:rol_permiso_accione).permit(:activo, :permiso_id, :role_id)
    end
end
