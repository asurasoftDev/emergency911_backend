class ExploracionesResultadosController < ApplicationController
  before_action :set_exploraciones_resultado, only: [:show, :update, :destroy]

  # GET /exploraciones_resultados
  def index
    @exploraciones_resultados = ExploracionesResultado.all

    render json: @exploraciones_resultados
  end

  # GET /exploraciones_resultados/1
  def show
    render json: @exploraciones_resultado
  end

  # POST /exploraciones_resultados
  def create
    @exploraciones_resultado = ExploracionesResultado.new(exploraciones_resultado_params)

    if @exploraciones_resultado.save
      render json: @exploraciones_resultado, status: :created, location: @exploraciones_resultado
    else
      render json: @exploraciones_resultado.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /exploraciones_resultados/1
  def update
    if @exploraciones_resultado.update(exploraciones_resultado_params)
      render json: @exploraciones_resultado
    else
      render json: @exploraciones_resultado.errors, status: :unprocessable_entity
    end
  end

  # DELETE /exploraciones_resultados/1
  def destroy
    @exploraciones_resultado.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_exploraciones_resultado
      @exploraciones_resultado = ExploracionesResultado.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def exploraciones_resultado_params
      params.require(:exploraciones_resultado).permit(:exploracion_id, :area_fisica_id, :ficha_clinica_id, :activo)
    end
end
