class EmergenciaController < ApplicationController
  before_action :set_emergencia, only: [:show, :update, :destroy]

  # GET /emergencia
  def index
    @emergencia = Emergencia.all

    render json: @emergencia
  end

  # GET /emergencia/1
  def show
    render json: @emergencia
  end


  def deleteEmergencia
    if Emergencia.find_by_id(params[:id]).destroy
      render json: { msg: "Registro borrado correctamente!"}
    else
      render json: @emergencia.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /emergencia/1
  def update
    self.updateEmergencia()
    # if emergency
    #   render json: emergency
    # else
    #   render json: emergency.errors, status: :unprocessable_entity
    # end 
    # if @emergencia.update(emergencia_params)
    #   render json: @emergencia
    # else
    #   render json: @emergencia.errors, status: :unprocessable_entity
    # end
  end

  def updateEmergencia
    # emergencia = params
    puts "params - - - -- - -- >#{params}"
    oEmergencia = {
      :id                 => params[:emergencia][:id],
      :activo             => params[:emergencia][:activo],
      :telefono_infomante => params[:emergencia][:telefono_infomante],
      :lugar_del_evento   => params[:emergencia][:lugar_del_evento],
      :descripcion        => params[:emergencia][:descripcion],
      :sector             => params[:emergencia][:sector],
      :ciudad_id          => params[:emergencia][:ciudad_id],
      :emergencia_tipo_id => params[:emergencia][:emergencia_tipo_id],
      :lat                => params[:emergencia][:lat]==nil ? 0 : params[:emergencia][:lat],
      :long               => params[:emergencia][:long]==nil ? 0 : params[:emergencia][:long]
    }

    params[:emergencia][:vehiculo_asignados_attributes].each do | vehiculo , indx |
      puts "========?>>>>#{vehiculo.to_json}"
      VehiculoAsignado.new( { emergencia_id:params[:emergencia][:id], turnos_vehiculo_id:vehiculo[:turnos_vehiculo_id] }).save
    end

    if Emergencia.where(id:params[:id]).update_all(oEmergencia)
      render json: @emergencia
    else
      render json: @emergencia.errors, status: :unprocessable_entity
    end
    # return emergency = Emergencia.where(id:params[:id]).update(oEmergencia)
  end

  def detalleDeEmergencia
    emergencys = []
    emergencias = Emergencia.all
    if(params[:id]!=nil)
      emergencias = Emergencia.where(id:params[:id])
    end
    emergencias.each do | emergency , indx |
      emergencia = Emergencia.find_by_id(emergency[:id])
      ciudad = Ciudad.find_by_id(emergency[:ciudad_id])
      oEmergencia = {
          "id"                 => emergencia[:id],
          "emergencia_id"      => emergencia[:id],
          "created_at"         => emergencia[:created_at],
          "updated_at"         => emergencia[:updated_at],
          "codigo"             => emergencia[:codigo],
          "activo"             => emergencia[:activo],
          "telefono_infomante" => emergencia[:telefono_infomante],
          "lugar_del_evento"   => emergencia[:lugar_del_evento],
          "descripcion"        => emergencia[:descripcion],
          "sector"             => emergencia[:sector],
          "ciudad_id"          => emergencia[:ciudad_id],
          "ciudad"             => ciudad,
          "emergencia_tipo_id" => emergencia[:emergencia_tipo_id],
          "emergencia_tipo"    => emergencia.emergencia_tipo.nombre,
          "lat"                => emergencia[:lat],
          "estado"             => emergencia[:estado],
          "long"               => emergencia[:long],
          "vehiculos"          => []
        }
      if(emergencia!=nil)
        vehiculos_asignados = VehiculoAsignado.where(emergencia_id:emergencia[:id])
        if(vehiculos_asignados.length>0)
          vehiculos_asignados.each do | vehiculo , indx |
            tv = TurnosVehiculo.find_by_id(vehiculo[:turnos_vehiculo_id])
            _vehiculo = Vehiculo.find_by_id(tv[:vehiculo_id])
            puts "|="*60
            # puts "==========>#{vehiculo.to_json}"
            # puts "==========>#{_vehiculo.to_json}"
            oVehiculo = _vehiculo
            oVehiculo = {
              'vehiculo_asignado_id' => vehiculo[:id],
              'vehiculo'             => _vehiculo,
              'turno_vehiculo_id'    => tv[:id],
              'pacientes'            => nil
            }
            # puts "||="*60
            # puts "#{oVehiculo}"
            # puts "|||="*60
            # puts "#{oEmergencia}"
            # puts "="*60
            aPacientes = []
            pacientes = FichaClinica.where(vehiculo_asignado:vehiculo.id)
            pacientes.each do | ficha |
              vfh = VehiculosFichasHistoriale.where(ficha_clinica_id:ficha[:id]).limit(1)[0]
              hospital = Hospital.find_by_id(ficha[:hospitals_id])
              paciente = Persona.find_by_id(ficha[:persona_id])
              
              oPaciente = {
                'entregado_completado'=>vfh[:entregado_completado],
                'ficha' => ficha,
                'hospital' => {nombre:hospital[:nombre],tipo_publico:hospital[:tipo_publico]},
                'persona' => paciente,
              }
              aPacientes.push(oPaciente)
            end
            oVehiculo['pacientes'] = aPacientes #FichaClinica.where(vehiculo_asignado:vehiculo.id)
            oEmergencia['vehiculos'].push(oVehiculo)
          end
        end
      end
      emergencys.push(oEmergencia)
    end
    if(params[:id]!=nil)
      render json: emergencys[0]
    else
      render json: emergencys
    end

  end

  def emergenciasActivasDeAmbulancia
    # user_id = 3
    user_id = current_user.id
    emergencias = []
    response_emergencias = []
    vehiculos_asignados = []
    turnos = Turno.where( { user_id: user_id } )
    puts "emergencias XX===> #{turnos.to_json}"
    turnos.each do | turno , indx |
      tv = TurnosVehiculo.where(turno_id:turno[:id])
      tv.each do |turno_v , index|
        _vehiculo_asignado = VehiculoAsignado.where({turnos_vehiculo_id:turno_v[:id],activo:true})
        if(_vehiculo_asignado.length > 0)
          vehiculos_asignados.push(_vehiculo_asignado)
        end
      end
    end
    emergencias = emergencias.uniq
    emergens = []
    pacientes = []
    vehiculos_asignados.each do | va , indi |
      emergencias.push(va[:emergencia_id])
      emergens.push({vehiculo_asignado_id:va[:id],data:va})
      _pacientes = FichaClinica.where({vehiculo_asignado_id:va[:id]})
      _pacientes.each do | paciente , indxx |
        vfh = VehiculosFichasHistoriale.where(ficha_clinica_id:paciente[:id]).limit(1)[0]
        hospital = Hospital.find_by_id(paciente[:hospitals_id])
        oPersona = Persona.find_by_id(paciente[:persona_id])
        pacientes.push({ vehiculo_asignado_id:va[:id], hospital: {nombre:hospital[:nombre],tipo_publico:hospital[:tipo_publico]},ficha:paciente , persona:oPersona , entregado_completado:vfh[:entregado_completado]})
      end
    end
    puts "=="*60
    puts "==#{emergencias.to_json}"
    puts "=="*60
    emergencias.each.with_index do |emergency, index|
      emergencia = Emergencia.where( { id:emergencias[index],activo:true } ).limit(1)[0]
      if(emergencia!=nil)
        # puts "emergens ===> #{emergencia.to_a}"
        aPacientes = []
        vehiculos_asignados.each do | va , indi |
          pacientes.each do | paciente , indxx |
            if(paciente[:vehiculo_asignado_id]==va[:id] ) ##&& emergencia[:id] == va[:emergencia_id])
              aPacientes.push(paciente)
            end
          end
        end
        ciudad  = Ciudad.find_by_id(emergencia[:ciudad_id])
        oEmergencia = {
          "vehiculo_asignado_id" => emergens[index][:vehiculo_asignado_id],
          "id"                   => emergencia[:id],
          "nombre"               => emergencia[:nombre],
          "codigo"               => emergencia[:codigo],
          "activo"               => emergencia[:activo],
          "telefono_infomante"   => emergencia[:telefono_infomante],
          "lugar_del_evento"     => emergencia[:lugar_del_evento],
          "descripcion"          => emergencia[:descripcion],
          "sector"               => emergencia[:sector],
          "ciudad_id"            => emergencia[:ciudad_id],
          "ciudad"               => ciudad,
          "emergencia_tipo_id"   => emergencia[:emergencia_tipo_id],
          "emergencia_tipo"      => emergencia.emergencia_tipo.nombre,
          "lat"                  => emergencia[:lat],
          "long"                 => emergencia[:long],
          "estado"               => emergencia[:estado],
          "pacientes"            => aPacientes
        }
        # vehiculo_asignado = VehiculoAsignado.where(turnos_vehiculo_id:turno_v[:id])
        response_emergencias.push(oEmergencia)
      end
    end
    render json: response_emergencias.to_json
  end

  def finalizarEmergencia
    emergencia_id = params[:id] 
    exite_algun_paciente_en_esta_emergencia = false
    vehiculos_asignados = VehiculoAsignado.where(emergencia_id:emergencia_id)
    cant_faltante_pacientes_por_entregar = 0
    if(vehiculos_asignados.length>0)
      vehiculos_asignados.each do | vehiculo , indx |
        pacientes = FichaClinica.where(vehiculo_asignado:vehiculo.id)
        if(pacientes.length > 0 )
          exite_algun_paciente_en_esta_emergencia = true
          pacientes.each do | paciente , indx |
            vfh = VehiculosFichasHistoriale.where(ficha_clinica_id:paciente[:id]).limit(1)[0]
            if(vfh[:entregado_completado]==false)
              cant_faltante_pacientes_por_entregar = cant_faltante_pacientes_por_entregar+1
            end
          end
        end
      end
    end
    if(cant_faltante_pacientes_por_entregar == 0)
      if exite_algun_paciente_en_esta_emergencia
        x = Emergencia.where(:id => emergencia_id).update_all("activo = false, estado='F'")
      else
        x = Emergencia.where(:id => emergencia_id).update_all("activo = false, estado='CANCELADA'")
      end

      vehiculos_asignados.each do | vehiculo , indx |
        # puts "turno_vehiculo_id =======>#{vehiculo[:turnos_vehiculo_id]}"
        VehiculoAsignado.where(turnos_vehiculo_id:vehiculo[:turnos_vehiculo_id]).update_all("activo = false")
      end
      puts "=="*60
      puts " ACTUALIZAR ESTADO DE LA EMREGENCIA"
      puts "#{x}"
      puts "=="*60
      render json:"Emeregencia completada!", status: 200
    else
      render json: "Existen pacientes sin entregar", status: 401
    end
  end

  def emergenciasActivas
    emergencias = Emergencia.where( { activo:true } )
  end

  def emergenciaFindByCodigo
    emergencia = Emergencia.where( { codigo:params[:codigo] } )
    render json: emergencia.to_json
  end

  def emergenciasPorFecha
    sql = "SELECT * FROM emergencia where created_at between '#{params[:fecha1]}' and '#{params[:fecha2]}'"
    query = ActiveRecord::Base.connection.execute(sql)
    render json: query.to_json
  end

  # POST /emergencia
  def create
    # 
    ## EMERGENCIA CON MENOS DE 30 MINUTOS DE SER REGISTRADAS 
    time = Time.new
    objToCreate = emergencia_params
    # puts "puede_registrar ==========>>> #{params[:emergencia][:ciudad_id].to_json}"
    ciudad_id                  = params[:emergencia][:ciudad_id]
    objToCreate['codigo'] = " #{time.year}#{time.month}#{time.day}#{emergencia_params[:codigo]}"
    telefono_infomante         = emergencia_params[:telefono_infomante]
    sql = "select * from emergencia  where ( created_at::time >= (current_timestamp::time - interval '30' minute) and created_at::DATE = current_date) and ciudad_id=#{ciudad_id} and telefono_infomante='#{telefono_infomante}'"
    puts "SQL = #{sql}"
    #======| AQUI VA LA VALIDACION |=======| ToDo |=====
    query = ActiveRecord::Base.connection.execute(sql)
    if(query.to_a.length > 0)
      puede_registrar = false
    else
      puede_registrar = true
    end
    #======| AQUI VA LA VALIDACION |=======| ToDo |=====| END|===
    if puede_registrar
      @emergencia = Emergencia.new(emergencia_params)
      @emergencia.codigo = objToCreate['codigo']
      puts "@emergencia ==========>>> 00", @emergencia.to_json
      puts "@emergencia ==========>>> 11", @emergencia.vehiculo_asignados.to_json
      if( @emergencia.vehiculo_asignados.length >0)
        puts "@emergencia ==========>>> 1.2", @emergencia.vehiculo_asignados[0]['turnos_vehiculo_id']
        puts "@emergencia ==========>>> 22", TurnosVehiculo.find_by_id(@emergencia.vehiculo_asignados[0]['turnos_vehiculo_id']).to_json
        # @emergencia.vehiculo_asignados = TurnosVehiculo.find_by_id(@emergencia.vehiculo_asignados[0]['turnos_vehiculo_id'])
      end
      # puts "@emergencia ==========>>> 33", @emergencia.vehiculo_asignados.to_json
      # @emergencia.vehiculo_asignados = emergencia_params[:vehiculo_asignados_attributes]
      # @emergencia = Emergencia.new(emergencia_params)
      if @emergencia.save
        render json: {emergencia: @emergencia, codigo: objToCreate['codigo'], msg: "emergencia registrada correctamente."}
      else
        puts "@emergencia.errors ==========>>> ", @emergencia.errors.to_json
        render json: @emergencia.errors, status: :unprocessable_entity, codigo:objToCreate['codigo']
      end
    else
      render json: {codigo:objToCreate['codigo'] ,msg: "Existe un registro con estas caractericas, muy reciente !"}, status: 401
      # render json: { "data"=>{ "msg"=> "Existe un registro con estas caractericas muy reciente !"}, "status"=>401  }, status: 401
    end
  end



  # DELETE /emergencia/1
  def destroy
    @emergencia.destroy
  end

  def vehiculosDisponiblesParaEmergencias
    #  VEHICULOS DISPONIBLES PARA EMERGENCIAS === EN PROCEDO ===| ToDo |===
    turnos_choferes = []
    hoy_es = I18n.t(Date.today.strftime("%A"), locale: :es)
    turno_del_dia = Dia.where("lower(nombre)=lower('#{hoy_es}')")[0]
    puts " 1000-----> #{turno_del_dia.to_json}"
    if(turno_del_dia!=nil)
      tanda_de_turno = Tanda.where({ dia_id:turno_del_dia[:id],  cargo_id: 3})
      puts " 1001-----> #{tanda_de_turno.to_json}"
      if(tanda_de_turno.length>0)
        tanda_de_turno.each do | t_t|
          Turno.where({ tanda_id: t_t[:id] }).each do | t |
            turnos_choferes.push( t)
          end
        end
        puts " 1002-----> #{turnos_choferes.to_json}"
      end
    end
    vehiculos_disponibles = []
    turnos_choferes.each do |chofer_turno, index|
      puts "chofer_turno => ", :json => chofer_turno
      if DateTime.now.to_s(:time)  >= chofer_turno.tanda.hora_inicio.to_s(:time) and DateTime.now.to_s(:time) <= chofer_turno.tanda.hora_fin.to_s(:time)
        vehiculo = TurnosVehiculo.where({turno_id: chofer_turno[:id], activo: true})
        puts "1000==> #{vehiculo.to_json}"
        if(vehiculo.length > 0)
          vehiculo_esta_disponible = VehiculoAsignado.where({turnos_vehiculo_id: vehiculo[0][:id], activo:true})
          unless(vehiculo_esta_disponible.length > 0)
            oVehiculoTemp= Vehiculo.find_by_id(vehiculo[0][:vehiculo_id])
            vehiculos_disponibles.push({
              'id': vehiculo[0][:id],
              'user_id': chofer_turno[:user_id],
              'tanda_id': chofer_turno[:tanda_id],
              'activo': chofer_turno[:activo],
              'created_at': chofer_turno[:created_at],
              'updated_at': chofer_turno[:updated_at],
              'alias': oVehiculoTemp.alias, 
              'placa': oVehiculoTemp.placa, 
              'tanda': chofer_turno.tanda,
              'user': chofer_turno.user,
            })
          end
        end
      end
    end

    # turno_chofer = Turno.where({ tanda_id:tanda_de_turno[:id]})
    
    
    # render json: turnos_choferes.to_json
    render json: vehiculos_disponibles
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_emergencia
      @emergencia = Emergencia.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def emergencia_params
      params.require(:emergencia).permit(:ciudad_id, :sector, :lugar_del_evento, :descripcion, :telefono_infomante, :activo, :codigo, :lat, :long, :emergencia_tipo_id, vehiculo_asignados_attributes: [:id, :turnos_vehiculo_id, :_destroy])
    end
end
