class ProductosConsumidosController < ApplicationController
  before_action :set_producto_cosumido, only: [:show, :update, :destroy]

  # GET /productos_cosumidos
  def index
    @productos_cosumidos = ProductosConsumido.all

    render json: @productos_cosumidos
  end

  # GET /productos_cosumidos/1
  def show
    render json: @producto_cosumido
  end

  # POST /productos_cosumidos
  def create
    @producto_cosumido = ProductosConsumido.new(producto_cosumido_params)

    if @producto_cosumido.save
      render json: @producto_cosumido, status: :created, location: @producto_cosumido
    else
      render json: @producto_cosumido.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /productos_cosumidos/1
  def update
    if @producto_cosumido.update(producto_cosumido_params)
      render json: @producto_cosumido
    else
      render json: @producto_cosumido.errors, status: :unprocessable_entity
    end
  end

  # DELETE /productos_cosumidos/1
  def destroy
    @producto_cosumido.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_producto_cosumido
      @producto_cosumido = ProductosConsumido.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def producto_cosumido_params
      params.require(:producto_cosumido).permit(:producto_vehiculo_id, :vehiculo_ficha_historial_id, :activo)
    end
end
