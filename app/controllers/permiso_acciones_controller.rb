class PermisoAccionesController < ApplicationController
  before_action :set_permiso_accione, only: [:show, :update, :destroy]

  # GET /permiso_acciones
  def index
    @permiso_acciones = PermisoAccione.all

    render json: @permiso_acciones
  end

  # GET /permiso_acciones/1
  def show
    render json: @permiso_accione
  end

  # POST /permiso_acciones
  def create
    @permiso_accione = PermisoAccione.new(permiso_accione_params)

    if @permiso_accione.save
      render json: @permiso_accione, status: :created, location: @permiso_accione
    else
      render json: @permiso_accione.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /permiso_acciones/1
  def update
    if @permiso_accione.update(permiso_accione_params)
      render json: @permiso_accione
    else
      render json: @permiso_accione.errors, status: :unprocessable_entity
    end
  end

  # DELETE /permiso_acciones/1
  def destroy
    @permiso_accione.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_permiso_accione
      @permiso_accione = PermisoAccione.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def permiso_accione_params
      params.require(:permiso_accione).permit(:nombre, :activo, :permiso_id)
    end
end
