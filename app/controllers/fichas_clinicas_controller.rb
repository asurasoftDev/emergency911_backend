class FichasClinicasController < ApplicationController
  before_action :set_ficha_clinica, only: [:show, :update, :destroy]

  # GET /fichas_clinicas
  def index
    @fichas_clinicas = FichasClinica.all

    render json: @fichas_clinicas
  end

  # GET /fichas_clinicas/1
  def show
    render json: @ficha_clinica
  end

  # POST /fichas_clinicas
  def create
    @ficha_clinica = FichaClinica.new(ficha_clinica_params)

    if @ficha_clinica.save
      render json: @ficha_clinica, status: :created, location: @ficha_clinica
    else
      render json: @ficha_clinica.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /fichas_clinicas/1
  def update
    if @ficha_clinica.update(ficha_clinica_params)
      render json: @ficha_clinica
    else
      render json: @ficha_clinica.errors, status: :unprocessable_entity
    end
  end

  # DELETE /fichas_clinicas/1
  def destroy
    @ficha_clinica.destroy
  end


  def createPacientesPertenencia(id)
    puts "===="*30
    puts "-> #{id}"
    puts "===="*30
    PacientesPertenencia.where(ficha_clinica_id: id).destroy_all
    params[:pertenencias].each do |pertenencia|
      PacientesPertenencia.create({descripcion: pertenencia[:nombre] ,ficha_clinica_id: id, quien_recibe: params[:quien_recibe]})
    end
    
  end
  
  def crear
    plantilla_llena
  end
  def actualizar
    plantilla_llena
  end

  def plantilla_llena
    persona_id             = params[:persona_id]
    vehiculo_asignado_id   = params[:vehiculo_asignado_id]
    medico_hospital        = params[:medico_hospital]
    hospital_id            = params[:hospital_id]
    diagnostico_presuntivo = params[:diagnostico_presuntivo]
    observacion            = params[:observacion]
    triage_color           = params[:triage_color]
    emergencia_id          = params[:emergencia_id] 
    accion                 = params[:accion]
    areas                  = params[:areas]
    entrega_completado     = params[:entregado]
    km_init                = params[:km_inicial_vehiculo]
    km_final               = params[:km_final_vehiculo]
    



    # validar si un paciente ya esta registrado en una emergencia
    if(accion == 'C')
      registros = FichaClinica.where({persona_id:persona_id,vehiculo_asignado_id:vehiculo_asignado_id})
      if(registros.length > 0)
        puts "EXISTE PACIENTE NOTIFICA ERROR =====> #{registros.length}"
        return render json: "Este paciente ya esta registrado para esta emergencia!", error:true, status:401
      end
    end


    #======= REGISTRAR HISTRIAL DE LA FICHA ========
    registros = []
    if(params[:productos_consumidos] != nil)
      productos_consumidos = params[:productos_consumidos]
      productos_consumidos.each do |producto_cosumido, indx|
        vehicle = VehiculoAsignado.find_by_id(vehiculo_asignado_id).turnos_vehiculo.vehiculo
        query = {vehiculo_id:vehicle[:id],producto_id:producto_cosumido[:producto_id]}
        puts "query ==>#{query.to_json}"
        registros = ProductosVehiculo.where(query)
        registros.each do |producto_existente|
          if(producto_existente[:producto_id] == producto_cosumido[:producto_id])
            cantidad_actual = producto_existente[:cantidad].to_f - producto_cosumido[:cantidad].to_f
            producto_existente.update(cantidad:cantidad_actual)
          end
        end
      end
    end
    #======= REGISTRAR HISTRIAL DE LA FICHA ========| END |===

    if(accion == 'C')
      emergencia_id = 1
      datos = {vehiculo_asignado_id:vehiculo_asignado_id,persona_id: persona_id, hospitals_id:  hospital_id, medico_hospital: medico_hospital, diagnostico_presuntivo: diagnostico_presuntivo, observacion: observacion, triage_color: triage_color, estado: 'A', activo:true}
      ficha = FichaClinica.create({vehiculo_asignado_id:vehiculo_asignado_id,persona_id: persona_id, hospitals_id:  hospital_id, medico_hospital: medico_hospital, diagnostico_presuntivo: diagnostico_presuntivo, observacion: observacion, triage_color: triage_color, estado: 'A', activo:true} )
      
      vfh = VehiculosFichasHistoriale.create({ficha_clinica_id:ficha[:id],entregado_completado:entrega_completado})
      productos_consumidos = params[:productos_consumidos]
      productos_consumidos.each do |producto_cosumido, indx|
        vehicle = VehiculoAsignado.find_by_id(vehiculo_asignado_id).turnos_vehiculo.vehiculo
        # registro = ProductosConsumido.where({vehiculo_id:vehicle[:id],producto_id:producto[:producto_id]}).limit(1)
        registros.each do |producto_existente|
          if(producto_existente[:producto_id] == producto_cosumido[:producto_id])
            puts "=====>"
            puts "producto_existente::#{producto_existente.to_json}"
            sql_insert =  "INSERT INTO public.productos_consumidos
            (producto_vehiculo_id, vehiculo_ficha_historial_id, activo, created_at, updated_at, cant_utilizada)
            VALUES(#{producto_existente[:id]}, #{vfh[:id]}, true, '#{DateTime.now}', '#{DateTime.now}', #{producto_cosumido[:cantidad]});
            "
            ActiveRecord::Base.connection.execute(sql_insert)

            # ProductosConsumido.create({
            #   vehiculo_ficha_historial_id:vfh[:id],
            #   producto_vehiculo_id:producto_existente[:id],
            #   cant_utilizada:producto_cosumido[:cantidad]
            # })
          end
        end
        puts "="*15
        puts "="*60
        # puts "registro: #{registro}"
        puts "="*60
        puts "="*15
        # registro.
      end

      unless ficha
        return render json:ficha.errors, status: :unprocessable_entity
      end
      createPacientesPertenencia(ficha.id)
      areas.each do |area, index|

        if( !area.key?(:sub_areas) ) # ESTA PARTE ES PARA CUANDO NO TIENE SUB_AREA
          area[:campos].each do |campo, indx|
            # puts "----------> ficha_clinica_params >> #{campo.key?(:data)}"
            if(campo.key?(:data) )
              if(campo[:input_type]=='string')
                sql = "INSERT INTO public.#{area[:tabla_destino]}
                ( #{area[:campos_necesarios_para_tabla_destino][0]},#{area[:campos_necesarios_para_tabla_destino][1]}, ficha_clinica_id, activo, created_at, updated_at)
                VALUES(#{campo[:id]}, #{campo[:data]}, #{ficha[:id]}, true, '#{DateTime.now}', '#{DateTime.now}');"
              elsif(campo[:input_type]=='check')
                sql = "INSERT INTO public.#{area[:tabla_destino]}
                ( #{area[:campos_necesarios_para_tabla_destino][0]}, ficha_clinica_id, activo, created_at, updated_at)
                VALUES(#{campo[:id]}, #{ficha[:id]}, true, '#{DateTime.now}', '#{DateTime.now}');"
              end
              puts "----------> ficha >> #{ficha.to_json}"
              puts "----------> campo >> #{campo.key?(:data)}"
              puts "---------XXX      -> ficha >> #{sql}"
              ActiveRecord::Base.connection.execute(sql)
              # puts "----------> ficha_clinica_params >> #{ ActiveRecord::Base.connection.execute(sql) }"
            end
          end
        else
          puts "----------> ARE CON SUB_AREAS >> #{area.to_json}"
          puts "----------> ARE CON ficha >> #{ficha.to_json}"
          area[:sub_areas].each do |sub_area, indi|
            sub_area[:sub_area][:campos].each do |campo, indx|
              puts "----------> ficha_clinica_params <<<< #{ficha.to_json}"
              if(campo.key?(:data) )
                if(campo[:input_type]=='string')
                #   sql = "INSERT INTO public.#{area[:tabla_destino]}
                #   ( #{area[:campos_necesarios_para_tabla_destino][0]},#{area[:campos_necesarios_para_tabla_destino][1]}, ficha_clinica_id, activo, created_at, updated_at)
                #   VALUES(#{campo[:id]}, #{campo[:data]}, #{ficha[:id]}, true, '#{DateTime.now}', '#{DateTime.now}');"
                elsif(campo[:input_type]=='check')
                  sql = "INSERT INTO public.exploraciones_resultados
                  ( exploracion_predefinida_id, ficha_clinica_id, activo, created_at, updated_at)
                  VALUES(#{campo[:id]}, #{ficha[:id]}, true, '#{DateTime.now}', '#{DateTime.now}');"
                end
                puts "----------> ficha_clinica_params >> #{ ActiveRecord::Base.connection.execute(sql) }"
              end
            end
          end
        end

      end
      retorno = {msg:"no sé que paso",status:200}
      render json:retorno
    elsif(accion == 'U')
      ficha_id = params[:id]==nil ?params[:ficha_clinica_id]:params[:id]
      ficha = FichaClinica.find_by_id(ficha_id)
      createPacientesPertenencia(params[:ficha_clinica_id])
      FichaClinica.where(id:ficha_id).update_all(" hospitals_id= #{hospital_id}, medico_hospital='#{medico_hospital}', diagnostico_presuntivo='#{diagnostico_presuntivo}', observacion='#{observacion}', triage_color='#{triage_color}'")
      vfh = VehiculosFichasHistoriale.where(ficha_clinica_id:ficha[:id]).limit(1)
      if(vfh.length > 0 )
        vfh.update_all ( "km_inicial_vehiculo =#{km_init},km_final_vehiculo=#{km_final} , entregado_completado=#{entrega_completado}")
        vfh = vfh[0]
      else
        vfh = VehiculosFichasHistoriale.create({ficha_clinica_id:ficha[:id],km_inicial_vehiculo:km_init,km_final_vehiculo:km_final, entregado_completado:entrega_completado})
      end
      if(params[:productos_consumidos] != nil)
        productos_consumidos = params[:productos_consumidos]
        puts "----------> PRODUCTOS_CONSUMIDOS >>#{productos_consumidos.to_json}"
        productos_consumidos.each do |producto_cosumido, indx|
          vehicle = VehiculoAsignado.find_by_id(vehiculo_asignado_id).turnos_vehiculo.vehiculo
          # registro = ProductosConsumido.where({vehiculo_id:vehicle[:id],producto_id:producto[:producto_id]}).limit(1)
          registros.each do |producto_existente, indici|
            # data_insert = { vehiculo_ficha_historial_id:vfh[:id],producto_vehiculo_id:producto_existente[:id],cant_utilizada:producto_cosumido[:cantidad],activo:true}
            if(producto_existente[:producto_id] == producto_cosumido[:producto_id])
              # ProductosConsumido.create(data_insert)
              puts "----------> DATA_INSERT >>#{vfh.to_json}"
              sql = "INSERT INTO public.productos_consumidos
              (producto_vehiculo_id, vehiculo_ficha_historial_id, activo, created_at, updated_at, cant_utilizada)
              VALUES(#{producto_existente[:id]}, #{vfh[:id]}, true, '#{DateTime.now}', '#{DateTime.now}', #{producto_cosumido[:cantidad]});"
              query = ActiveRecord::Base.connection.execute(sql)
            end
          end
          puts "="*15
          puts "="*60
          puts "registro: #{registros}"
          puts "="*60
          puts "="*15
          # registro.
        end
        end
      areas.each do |area, index|
        if( !area.key?(:sub_areas) ==true ) # ESTA PARTE ES PARA CUANDO NO TIENE SUB_AREA
          area[:campos].each do |campo, indx|

            # ======= PRIMERO COMPROBAR SI EL REGISTRO YA EXISTE
            sql = "SELECT * FROM #{area[:tabla_destino]} AS tabla WHERE #{area[:campos_necesarios_para_tabla_destino][0]}=#{campo[:id]} AND ficha_clinica_id=#{ficha[:id]};"
            query = ActiveRecord::Base.connection.execute(sql)
            puts "----------> CONSULTA >>#{query.to_a}"
            if(query.to_a.length > 0)
              puts "----------> REGISTRO SI EXISTE <<"
              # ======= RECONECER EL TIPO DE REGISTRO 
              # ======= ACTUALIZAR EL TIPO DE REGISTRO
              if(campo[:input_type]=='string')
                if(campo.key?(:data) && campo[:data]!='' )
                  sql = "UPDATE public.#{area[:tabla_destino]}
                  SET #{area[:campos_necesarios_para_tabla_destino][1]}='#{campo[:data]}'
                  WHERE ficha_clinica_id=#{ficha[:id]} AND #{area[:campos_necesarios_para_tabla_destino][0]}=#{campo[:id]};"
                else
                  sql = "DELETE FROM public.#{area[:tabla_destino]} WHERE ficha_clinica_id=#{ficha[:id]} AND #{area[:campos_necesarios_para_tabla_destino][0]}=#{campo[:id]};"
                  query = ActiveRecord::Base.connection.execute(sql)
                end
              elsif(campo[:input_type]=='check')
                puts "----------> campo >>#{campo.to_json}"
                if(!campo.key?(:data) || campo[:data]==false)
                  sql = "DELETE FROM public.#{area[:tabla_destino]} WHERE ficha_clinica_id=#{ficha[:id]} AND #{area[:campos_necesarios_para_tabla_destino][0]}=#{campo[:id]};"
                  query = ActiveRecord::Base.connection.execute(sql)
                end
              end
            else
              puts "----------> REGISTRO NO EXISTE >>"
              if(campo.key?(:data) )
                # ======= RECONECER EL TIPO DE REGISTRO 
                if(campo[:input_type]=='string')
                  
                  sql = "INSERT INTO public.#{area[:tabla_destino]}
                  ( #{area[:campos_necesarios_para_tabla_destino][0]},#{area[:campos_necesarios_para_tabla_destino][1]}, ficha_clinica_id, activo, created_at, updated_at)
                  VALUES(#{campo[:id]}, #{campo[:data]}, #{ficha[:id]}, true, '#{DateTime.now}', '#{DateTime.now}');"

                elsif(campo[:input_type]=='check')

                  sql = "INSERT INTO public.#{area[:tabla_destino]}
                  ( #{area[:campos_necesarios_para_tabla_destino][0]}, ficha_clinica_id, activo, created_at, updated_at)
                  VALUES(#{campo[:id]}, #{ficha[:id]}, true, '#{DateTime.now}', '#{DateTime.now}');"  

                end
                query = ActiveRecord::Base.connection.execute(sql)
                puts "----------> =========> #{sql}"
                puts "----------> REGISTRO INSERTADO <------------- >> "             
              end
            end

            # if(campo[:input_type]=='string')
            #   "UPDATE public.#{area[:tabla_destino]}
            #   SET #{area[:campos_necesarios_para_tabla_destino][0]}=1, ficha_clinica_id=#{ficha[:id]}, activo=true 
            #   WHERE ficha_clinica_id=#{ficha[:id]} AND #{area[:campos_necesarios_para_tabla_destino][0]}=#{campo[:id]};"
            # elsif(campo[:input_type]=='check')
            #   sql = "DELETE FROM public.#{area[:tabla_destino]}
            #   WHERE #{area[:campos_necesarios_para_tabla_destino][0]}=#{campo[:id]} AND ficha_clinica_id=#{ficha[:id]};"
            #   if(campo[:data]==nil)
                
            #   end
            # end
          end #ARE.EACH
        else # ESTA PARTE ES PARA CUANDO  TIENE SUB_AREA
          
          puts "-------SQL---> #{area[:sub_areas].to_json} <<"
          area[:sub_areas].each do |sub_area, indi|
            sub_area[:sub_area][:campos].each do |campo, indx|
              puts "----------> ficha_clinica_params <<<< #{campo.key?(:data)}"
              # ======= PRIMERO COMPROBAR SI EL REGISTRO YA EXISTE
              # sql = "SELECT * FROM #{area[:tabla_destino]} AS tabla WHERE #{area[:campos_necesarios_para_tabla_destino][0]}=#{campo[:id]} AND ficha_clinica_id=#{ficha[:id]};"
              sql = "SELECT * FROM #{sub_area[:tabla_destino]} AS tabla WHERE #{sub_area[:campos_necesarios_para_tabla_destino][0]}=#{campo[:id]} AND ficha_clinica_id=#{ficha[:id]};"
              query = ActiveRecord::Base.connection.execute(sql)
              if(query.to_a.length > 0)
                puts "----------> REGISTRO SI EXISTE <<"
                if(campo[:input_type]=='string')
                  if( campo.key?(:data) )
                    
                  end
                elsif(campo[:input_type]=='check')
                  if(campo[:data]==false )
                    sql = "DELETE FROM #{sub_area[:tabla_destino]} WHERE ficha_clinica_id=#{ficha[:id]} AND #{sub_area[:campos_necesarios_para_tabla_destino][0]}=#{campo[:id]};"
                    puts "----------> ficha_clinica_params >> #{ ActiveRecord::Base.connection.execute(sql) }"
                  end
                end
              else
                if( campo[:data] == true )
                sql = "INSERT INTO public.#{sub_area[:tabla_destino]}
                ( #{sub_area[:campos_necesarios_para_tabla_destino][0]}, ficha_clinica_id, activo, created_at, updated_at)
                VALUES(#{campo[:id]}, #{ficha[:id]}, true, '#{DateTime.now}', '#{DateTime.now}');"  
                # ======> INSERT NEW ROW 
                puts "----------> ficha_clinica_params >> #{ ActiveRecord::Base.connection.execute(sql) }"
                end
              end

            end # sub_area[:sub_area][:campos].each
          end # area[:sub_areas].each do

        end # IF 
      end #ARES.EACH
    end # ELSIF

  end


  def plantilla

    vehiculo_asignado_id = params[:vehiculo_asignado_id]
    # ============
      ultimo_km_final = 0
      ultima_ficha_registrada = 0 
      ultima_asignacion_vehicle = 0
      ultima_asignacion_vehicle = VehiculoAsignado.where(id:vehiculo_asignado_id).limit(1)[0]
      if(ultima_asignacion_vehicle!=nil)
        _query = FichaClinica.where(vehiculo_asignado_id:ultima_asignacion_vehicle[:id]).limit(1)
        if(_query.length>0)
          ultima_ficha_registrada = _query[0]
          ultimo_km_final  = VehiculosFichasHistoriale.where({ficha_clinica_id:ultima_ficha_registrada[:id]}).limit(1)[0][:km_final_vehiculo]
        end
        puts "ultima_asignacion_vehicle =>#{ultima_asignacion_vehicle.to_json}"
        puts "ultima_ficha_registrada =>#{ultima_ficha_registrada.to_json}"
        puts "ultimo_km_final =>#{ultimo_km_final.to_json}"
      end
    # ============
    
    tablas_de_plantilla = [
      { 'column_name_origen_id'=>'signo_vital_id','tabla_campos_origen' => 'signos_vitales','tabla_destino'=>'signos_encontrados','input_type'=>'string' , 'campos_necesarios_para_tabla_destino'=>'value'},
      { 'column_name_origen_id'=>'procedimiento_id','tabla_campos_origen' => 'procedimientos','tabla_destino'=>'procedimientos_ejecutados','input_type'=>'check'},
      { 'column_name_origen_id'=>'causa_clinica_id','tabla_campos_origen' => 'causas_clinicas','tabla_destino'=>'oringes_probables','input_type'=>'check'},
      { 'column_name_origen_id'=>'trauma_id','tabla_campos_origen' => 'traumas','tabla_destino'=>'causas_traumaticas','input_type'=>'check'},
      { 'column_name_origen_id'=>'antecedente_id','tabla_campos_origen' => 'antecedentes','tabla_destino'=>'antecedentes_encontrados','input_type'=>'check'}
    ]
    ficha_clinica_id = params[:ficha_clinica_id] != nil ? params[:ficha_clinica_id] : params[:id]
    plantilla = { 'km_inicial_vehiculo'=>ultimo_km_final,'km_final_vehiculo'=>0,'entregado'=>false,'ficha_clinica_id'=>nil,'accion'=>'C', 'persona_id'=>nil, 'emergencia_id'=>nil, 'vehiculo_asignado_id'=>nil, 'hospital_id'=>nil, 'medico_hospital'=>nil,'diagnostico_presuntivo'=>nil,'observacion'=>nil,'triage_color'=>nil, 'areas'=>[]}
    if(params[:id] != nil )
      ficha_clinica_id = params[:id]
      vfh = VehiculosFichasHistoriale.where(ficha_clinica_id:ficha_clinica_id).limit(1)
      # vfh = ActiveRecord::Base.connection.execute("SELECT * FROM vehiculos_fichas_historiales WHERE ficha_clinica_id=#{ficha_clinica_id} ") #VehiculosFichasHistoriale.where({ficha_clinica_id:ficha_clinica_id})
      _ficha = FichaClinica.find_by_id(ficha_clinica_id)
      vfh = vfh.to_a
      if(vfh.length>0)
        selet = "SELECT count(id), sum(cant_utilizada) as cant_utilizada, producto_vehiculo_id FROM productos_consumidos WHERE vehiculo_ficha_historial_id=#{vfh[0]['id']} group by producto_vehiculo_id"
        prod_consumidos = ActiveRecord::Base.connection.execute(selet)
        p_consumidos = []
        p_pertenecias = PacientesPertenencia.select("descripcion nombre, quien_recibe").where({ficha_clinica_id: ficha_clinica_id})
        quien_recibe = nil
        if(p_pertenecias.length>0)
          puts "----------> producto_c >>#{p_pertenecias.to_json}"
          quien_recibe = p_pertenecias[0][:quien_recibe]
        end
        prod_consumidos.each do |producto_c|
          pv = ProductosVehiculo.find_by_id(producto_c['producto_vehiculo_id'])
          producto = Producto.find_by_id(pv['producto_id'])

          oPrdo = {
            'id' => producto[:id],
            'nombre' => producto[:nombre],
            'cantidad' => producto_c['cant_utilizada']
          } 
          p_consumidos.push(oPrdo)
        end
        plantilla['entregado']              = vfh[0][:entregado_completado]
        plantilla['km_inicial_vehiculo']    = vfh[0][:km_inicial_vehiculo]
        plantilla['km_final_vehiculo']      = vfh[0][:km_final_vehiculo]
        plantilla['persona_id']             = _ficha[:persona_id]
        plantilla['emergencia_id']          = _ficha[:emergencia_id]
        plantilla['hospital_id']            = _ficha[:hospitals_id]
        plantilla['vehiculo_asignado_id']   = _ficha[:vehiculo_asignado_id]
        plantilla['medico_hospital']        = _ficha[:medico_hospital]
        plantilla['diagnostico_presuntivo'] = _ficha[:diagnostico_presuntivo]
        plantilla['observacion']            = _ficha[:observacion]
        plantilla['triage_color']           = _ficha[:triage_color]
        plantilla['productos_cosumidos']    = p_consumidos
        plantilla['pertenencias']    = p_pertenecias
        plantilla['quien_recibe']    =quien_recibe
      end
    end
    tablas_de_plantilla.each do |tabla, index|
      campos_necesarios_para_tabla_destino = [  ] #'ficha_clinica_id' 
      table_nombre = tabla['tabla_campos_origen']
      campos_necesarios_para_tabla_destino.push( tabla['column_name_origen_id'] )
      campos_necesarios_para_tabla_destino.push( tabla['campos_necesarios_para_tabla_destino'] )
      area = { 'area'=> tabla['tabla_campos_origen'] , 'tabla_destino'=> tabla['tabla_destino'] , 'campos_necesarios_para_tabla_destino'=> campos_necesarios_para_tabla_destino}
      area['campos'] = []
      # results = ActiveRecord::Base.connection.execute("SELECT column_name as label,data_type  as type FROM information_schema.columns    
      # WHERE table_schema = 'public'       
      # AND table_name   = '#{tabla['tabla_campos_origen']}'")
      
      mostrar = ActiveRecord::Base.connection.execute("SELECT id,nombre,activo FROM  #{tabla['tabla_campos_origen']}")
      # puts " XXXX- - - - - - - - >#{FichaClinica.find_by_id(ficha_clinica_id).to_json}"

      
      mostrar.each do |campo, indx|
        _campo = {}
        _campo = campo
        _campo['input_type'] = tabla['input_type']
        exite_ficha = FichaClinica.find_by_id(ficha_clinica_id)
        if(exite_ficha!=nil)
          plantilla['ficha_clinica_id']=ficha_clinica_id
          plantilla['accion']='U' #UPDATE

          selet = "SELECT * FROM #{tabla['tabla_destino']} WHERE ficha_clinica_id=#{ficha_clinica_id} AND #{tabla['column_name_origen_id']}=#{_campo['id']}"
          data = ActiveRecord::Base.connection.execute(selet)    
          if(_campo['input_type'] == 'check' && data.to_a.length > 0)
            puts " - - - DATA - - - - - >#{data.to_json} #{Array(data).length}"
            _campo['data'] = true
          elsif (_campo['input_type'] == 'string' && Array(data).length > 0)
            _campo['data'] = data[0]['value']
          end
        else
          # puts " ZZZZ- - - - - - - - >#{FichaClinica.find_by_id(ficha_clinica_id).to_json}"
          plantilla['accion']='C' #CREATE
        end
        if(_campo['nombre'])
          # puts " - - - DATA - ????| 100 |?? #{_campo['nombre']}?????? - - - - >",_campo.to_json
          area['campos'].push(_campo)
        else(_campo['id'])
          # puts " - - - DATA - ????| 101 |?? #{_campo['id']}?????? - - - - >",_campo.to_json
          area['campos'].push(_campo)
        end
      end
      # area['mostrar'] = mostrar
      plantilla['areas'].push(area)
    end

    # ================ SEGUNDA PARTE DE LA PLANTILLA =====
    areas_fisicas = { 'area'=>'exploracion', 'sub_areas'=>[]}
    area_fisica_actual = nil
    area = { 
      'id' => area_fisica_actual,
      'sub_area'=> { 'nombre'=> '', 'campos'=>[]},
      
    }
    ExploracionesPredefinida.all().each do |predefinida, indix|
      area_fisica = AreasFisica.find_by_id(predefinida[:area_fisica_id])
      puts " - - - - AREA_FISICA  - - - - >#{area_fisica.to_json}"
      if(area_fisica_actual != area_fisica[:id])
        area = { 
          'id' => area_fisica_actual,
          'tabla_destino' => 'exploraciones_resultados',
          'campos_necesarios_para_tabla_destino' => ['exploracion_predefinida_id'],
          'sub_area'=> { 
            'nombre'=> area_fisica[:nombre],
            'campos'=>[] 
          } 
        }
      end

      exploracion = Exploracion.find_by_id(predefinida[:exploracion_id])
      existe_registro = ExploracionesResultado.where({ficha_clinica_id:ficha_clinica_id,exploracion_predefinida_id:predefinida[:id]}).limit(1)
      oCampo = {
        'id' => predefinida[:id],
        'nombre' => exploracion[:nombre],
        'input_type' => 'check'

      }
      if(existe_registro.to_a.length > 0)
        oCampo['data'] = true
      end
      if( area_fisica[:id] == area_fisica_actual )
        area['sub_area']['campos'].push(oCampo)
      else
        area_fisica_actual = area_fisica[:id]
        area['sub_area']['campos'].push(oCampo)
        areas_fisicas['sub_areas'].push(area)
      end

    end
    plantilla['areas'].push(areas_fisicas)
    puts " - - - - - - - - >#{areas_fisicas.to_json}"
    render json:plantilla
    # render json:areas_fisicas
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ficha_clinica
      @ficha_clinica = FichaClinica.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ficha_clinica_params
      params.require(:ficha_clinica).permit(:persona_id, :emergencia_id, :hospital_id, :medico_hospital, :diagnostico_presuntivo, :observacion, :triage_color, :estado, :activo)
    end
end
