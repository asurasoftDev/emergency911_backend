class PacientesPertenenciaController < ApplicationController
  before_action :set_pacientes_pertenencium, only: [:show, :update, :destroy]

  # GET /pacientes_pertenencia
  def index
    @pacientes_pertenencia = PacientesPertenencium.all

    render json: @pacientes_pertenencia
  end

  # GET /pacientes_pertenencia/1
  def show
    render json: @pacientes_pertenencium
  end

  # POST /pacientes_pertenencia
  def create
    @pacientes_pertenencium = PacientesPertenencium.new(pacientes_pertenencium_params)

    if @pacientes_pertenencium.save
      render json: @pacientes_pertenencium, status: :created, location: @pacientes_pertenencium
    else
      render json: @pacientes_pertenencium.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /pacientes_pertenencia/1
  def update
    if @pacientes_pertenencium.update(pacientes_pertenencium_params)
      render json: @pacientes_pertenencium
    else
      render json: @pacientes_pertenencium.errors, status: :unprocessable_entity
    end
  end

  # DELETE /pacientes_pertenencia/1
  def destroy
    @pacientes_pertenencium.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pacientes_pertenencium
      @pacientes_pertenencium = PacientesPertenencium.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def pacientes_pertenencium_params
      params.require(:pacientes_pertenencium).permit(:ficha_clinica_id, :quien_recibe, :descripcion, :activo)
    end
end
