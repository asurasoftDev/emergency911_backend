class CausasTraumaticasController < ApplicationController
  before_action :set_causa_traumatica, only: [:show, :update, :destroy]

  # GET /causas_traumaticas
  def index
    @causas_traumaticas = CausasTraumatica.all

    render json: @causas_traumaticas
  end

  # GET /causas_traumaticas/1
  def show
    render json: @causa_traumatica
  end

  # POST /causas_traumaticas
  def create
    @causa_traumatica = CausasTraumatica.new(causa_traumatica_params)

    if @causa_traumatica.save
      render json: @causa_traumatica, status: :created, location: @causa_traumatica
    else
      render json: @causa_traumatica.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /causas_traumaticas/1
  def update
    if @causa_traumatica.update(causa_traumatica_params)
      render json: @causa_traumatica
    else
      render json: @causa_traumatica.errors, status: :unprocessable_entity
    end
  end

  # DELETE /causas_traumaticas/1
  def destroy
    @causa_traumatica.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_causa_traumatica
      @causa_traumatica = CausasTraumatica.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def causa_traumatica_params
      params.require(:causa_traumatica).permit(:trauma_id, :ficha_clinica_id, :descripcion, :causado_por, :activo)
    end
end
