class WelcomesController < ApplicationController
	# before_action :authenticate_usuario!

	def turnoGroupEmpleado
    empleadoList = []
    User.all.each do |user|
      unless Turno.find_by_user_id(user.id).nil?
        empleadoList.push(user)
      end
    end
    puts "empleadoList => ", :json => empleadoList
		render json: empleadoList
	end

  def list
    aUser = []
    usuariosList = User.all
    usuariosList.each do |usuario|
      oUser = {}
      oUser = usuario.attributes
      oUser['cargo'] = usuario.cargo
      oUser['persona'] = usuario.persona
      roles = []
      query_buscar_mis_roles = ActiveRecord::Base.connection.exec_query( "SELECT user_id, role_id FROM public.users_roles where user_id=#{usuario[:id]};" )
      if(query_buscar_mis_roles.length>0)
        query_buscar_mis_roles = ActiveRecord::Base.connection.exec_query( "SELECT user_id, role_id FROM public.users_roles where user_id=#{usuario[:id]};" )
        query_buscar_mis_roles.each  do |role|
          x = Role.find_by_id(role['role_id'])
          oRole = x.attributes
          oRole['permisos'] = []
          permisos = RolPermisoAccione.where(role_id:role['role_id'])
          permisos.each  do |permiso|
            oPermiso = permiso.attributes
            oPermiso['acciones'] = PermisoAccione.where(permiso[:permiso_id])
            oRole['permisos'].push(oPermiso)
          end
          roles.push(oRole)
        end
      end
      # roles = Us
      oUser['roles'] = roles
      aUser.push(oUser)
      puts "====| aUser |======"*12
      puts "#{usuario.to_json}"
      puts "=========="*12
    end
		render json: {data: aUser}
	end

  def getUsuario
    puts "params =>", :json => params["id"]
		usuario = User.find_by_id(params[:id])
		render json: usuario
	end

  def plantilla
    arr = []
    areas = []
    FichasClinica.reflections.keys.each  do |name|
      # puts "name =>", :json => name
      unless name == "persona" || name == "emergencia"
        puts "name => "+ name
        campos = []
        unless name == "exploraciones_resultados" 
          ActiveRecord::Base.connection.columns(name).each do |c|
            # puts "columnas => "+ c.name
            if c.name[(c.name.length-3)..(c.name.length)] == "_id" && c.name != "ficha_clinica_id"
              plurar = ""
              line = ""
              c.name[0..(c.name.length-4)].split("_").each do |padre|
                plurar += line+padre+(padre == "exploracion"? "es" : "s")
                line= "_"
              end
              # puts "padre => "+ plurar
              # puts "data...", :json => 
              ActiveRecord::Base.connection.exec_query("select * from #{plurar} where activo = true").each do |data|
                campos.push({label: data["nombre"], type: "boolean", value: false, id: data["id"]})
              end
              # area[:area] = plurar
              areas.push({area: plurar, campos: getArea(plurar)})
            end
            # area[:campos].push({label: c.name, type: c.type.to_s})
            # puts "- #{c.name}: #{c.type.to_s} #{c.limit.to_s}"
          end
          if campos.length == 0
            areas.push({area: name, campos: getArea(name)})
          end
        else
          areas.push({area: name, campos: getExploraciones()})
        end
          # areas.push(area)
      end
      # puts ActiveRecord::Base.connection.columns(name).each {|c| puts "- #{c.name}: #{c.type.to_s} #{c.limit.to_s}"} unless name == "persona" || name == "emergencia"
      # puts "data =>", :json => fc.class.reflect_on_association(name).klass#.new
    end 
    
    FichasClinica.columns.each do |column, index| 
      puts "#{column.name} (#{column.type})"
      if column.name[(column.name.length-2)..(column.name.length)] != "id" && column.name[(column.name.length-3)..(column.name.length)] != "_at" && column.name != "estado" && column.name != "activo" && column.name != "created_at" && column.name != "updated_at"
        tipo = column.type[0..(column.type.length)]
        arr.push({
          name: column.name,
          type: tipo,
          value: (tipo == "string" || tipo == "integer"? "":false)
        })
      end
      # if index == (FichasClinica.columns.length - 1)
    end
    areas.push({area: "ficha", campos: arr})
    puts "arr =>", :json => arr
    # arr.push({
    #   name: column.name,
    #   type: column.type
    # })
    render json: areas
  end
  
  def getArea(cNameTable)
    campos = []
    ActiveRecord::Base.connection.exec_query("select * from #{cNameTable} where activo = true").each do |data|
      campos.push({label: data["nombre"], type: "boolean", value: false, id: data["id"]})
    end
    return campos
  end

  def getExploraciones()
    areas_fisicas = []
    AreasFisica.where({activo: true}).each do |area|
      area_fisica = {label: area.nombre, type: "sub_area", campos: []}
      ExploracionesPredefinida.where({area_fisica_id: area.id}).each do |predefinida|
        area_fisica[:campos].push({label: predefinida.exploracion.nombre, type: "boolean", value: false, id: predefinida.id})
      end
      areas_fisicas.push(area_fisica)
    end

    return areas_fisicas
  end
  
end
