class DiaController < ApplicationController
  before_action :set_dium, only: [:show, :update, :destroy]

  # GET /dia
  def index
    @dia = Dia.all

    render json: @dia
  end

  # GET /dia/1
  def show
    render json: @dium
  end

  # GET /calendario
  def calendario
    calendario = []
    dates = ActiveRecord::Base.connection.exec_query("select i::date from generate_series((SELECT TO_CHAR(current_date, 'yyyy-mm-01')::date), (SELECT (date_trunc('month', current_date) + interval '1 month' - interval '1 day')::date), '1 day'::interval) i")
    # puts "dates =>", :json => dates
    # puts "getTurnosDay =>", :json => 
    getTurnosDay

    dates.each do |fecha|
      nameDia = I18n.t(fecha['i'].to_date.strftime("%A"), locale: :es)
      # puts "fecha => #{fecha} is #{nameDia}", :json => @turnosDias[nameDia.downcase]
      calendario.push({
        fecha: fecha['i'],
        dia: @turnosDias[nameDia.downcase]
      })
    end
    render json: calendario
  end

  def getTurnosDay
    @turnosDias = {}
    Dia.all.each do |dia|
      puts "nobre =>", :json => dia.nombre
      oDia = dia.attributes
      oDia['turnos'] = Turno.select("turnos.*").joins("inner join tandas t on t.id = turnos.tanda_id inner join dia d on d.id = t.dia_id").where("lower(d.nombre) = lower('#{dia.nombre}')")
      @turnosDias[dia.nombre.downcase] = oDia
    end
  end

  # POST /dia
  def create
    @dium = Dia.new(dium_params)

    if @dium.save
      render json: @dium, status: :created, location: @dium
    else
      render json: @dium.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /dia/1
  def update
    if @dium.update(dium_params)
      render json: @dium
    else
      render json: @dium.errors, status: :unprocessable_entity
    end
  end

  # DELETE /dia/1
  def destroy
    @dium.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dium
      @dium = Dia.find_by_id(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def dium_params
      params.require(:dium).permit(:nombre, :activo)
    end
end
