class ExploracionesPredefinidasController < ApplicationController
  before_action :set_exploraciones_predefinida, only: [:show, :update, :destroy]

  # GET /exploraciones_predefinidas
  def index
    @exploraciones_predefinidas = ExploracionesPredefinida.all

    render json: @exploraciones_predefinidas
  end

  # GET /exploraciones_predefinidas/1
  def show
    render json: @exploraciones_predefinida
  end

  # POST /exploraciones_predefinidas
  def create
    @exploraciones_predefinida = ExploracionesPredefinida.new(exploraciones_predefinida_params)

    if @exploraciones_predefinida.save
      render json: @exploraciones_predefinida, status: :created, location: @exploraciones_predefinida
    else
      render json: @exploraciones_predefinida.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /exploraciones_predefinidas/1
  def update
    if @exploraciones_predefinida.update(exploraciones_predefinida_params)
      render json: @exploraciones_predefinida
    else
      render json: @exploraciones_predefinida.errors, status: :unprocessable_entity
    end
  end

  # DELETE /exploraciones_predefinidas/1
  def destroy
    @exploraciones_predefinida.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_exploraciones_predefinida
      @exploraciones_predefinida = ExploracionesPredefinida.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def exploraciones_predefinida_params
      params.require(:exploraciones_predefinida).permit(:area_fisica_id, :exploracion_id, :activo)
    end
end
