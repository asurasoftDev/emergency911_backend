class SignosEncontradosController < ApplicationController
  before_action :set_signo_encontrado, only: [:show, :update, :destroy]

  # GET /signos_encontrados
  def index
    @signos_encontrados = SignosEncontrado.all

    render json: @signos_encontrados
  end

  # GET /signos_encontrados/1
  def show
    render json: @signo_encontrado
  end

  # POST /signos_encontrados
  def create
    @signo_encontrado = SignosEncontrado.new(signo_encontrado_params)

    if @signo_encontrado.save
      render json: @signo_encontrado, status: :created, location: @signo_encontrado
    else
      render json: @signo_encontrado.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /signos_encontrados/1
  def update
    if @signo_encontrado.update(signo_encontrado_params)
      render json: @signo_encontrado
    else
      render json: @signo_encontrado.errors, status: :unprocessable_entity
    end
  end

  # DELETE /signos_encontrados/1
  def destroy
    @signo_encontrado.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_signo_encontrado
      @signo_encontrado = SignosEncontrado.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def signo_encontrado_params
      params.require(:signo_encontrado).permit(:signo_vital_id, :ficha_clinica_id, :activo)
    end
end
