class CausasController < ApplicationController
  before_action :set_causa, only: [:show, :update, :destroy]

  # GET /causas
  def index
    @causas = Causa.all

    render json: @causas
  end

  # GET /causas/1
  def show
    render json: @causa
  end

  # POST /causas
  def create
    @causa = Causa.new(causa_params)

    if @causa.save
      render json: @causa, status: :created, location: @causa
    else
      render json: @causa.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /causas/1
  def update
    if @causa.update(causa_params)
      render json: @causa
    else
      render json: @causa.errors, status: :unprocessable_entity
    end
  end

  # DELETE /causas/1
  def destroy
    @causa.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_causa
      @causa = Causa.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def causa_params
      params.require(:causa).permit(:nombre, :activo)
    end
end
