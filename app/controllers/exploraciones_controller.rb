class ExploracionesController < ApplicationController
  before_action :set_exploracion, only: [:show, :update, :destroy]

  # GET /exploraciones
  def index
    @exploraciones = Exploracion.all

    render json: @exploraciones
  end

  # GET /exploraciones/1
  def show
    render json: @exploracion
  end

  # POST /exploraciones
  def create
    @exploracion = Exploracion.new(exploracion_params)

    if @exploracion.save
      render json: @exploracion, status: :created, location: @exploracion
    else
      render json: @exploracion.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /exploraciones/1
  def update
    if @exploracion.update(exploracion_params)
      render json: @exploracion
    else
      render json: @exploracion.errors, status: :unprocessable_entity
    end
  end

  # DELETE /exploraciones/1
  def destroy
    @exploracion.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_exploracion
      @exploracion = Exploracion.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def exploracion_params
      params.require(:exploracion).permit(:nombre, :tipo, :activo)
    end
end
