class AgentesCasualesController < ApplicationController
  before_action :set_agentes_casuale, only: [:show, :update, :destroy]

  # GET /agentes_casuales
  def index
    @agentes_casuales = AgentesCasuale.all

    render json: @agentes_casuales
  end

  # GET /agentes_casuales/1
  def show
    render json: @agentes_casuale
  end

  # POST /agentes_casuales
  def create
    @agentes_casuale = AgentesCasuale.new(agentes_casuale_params)

    if @agentes_casuale.save
      render json: @agentes_casuale, status: :created, location: @agentes_casuale
    else
      render json: @agentes_casuale.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /agentes_casuales/1
  def update
    if @agentes_casuale.update(agentes_casuale_params)
      render json: @agentes_casuale
    else
      render json: @agentes_casuale.errors, status: :unprocessable_entity
    end
  end

  # DELETE /agentes_casuales/1
  def destroy
    @agentes_casuale.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_agentes_casuale
      @agentes_casuale = AgentesCasuale.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def agentes_casuale_params
      params.require(:agentes_casuale).permit(:ficha_clinica_id, :causa_id, :activo)
    end
end
