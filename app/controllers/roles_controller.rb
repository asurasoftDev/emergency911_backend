class RolesController < ApplicationController
  # after_action :registrar_historia, only: [:create, :update, :destroy]
  before_action :set_rol, only: [:show, :update, :destroy]

  # GET /roles
  def index
    

    # puts "resultados roles => ", :json => role
    data = set_data()
    # puts "resultados roles => ", data

    render json: data
  end

  # GET /roles/1
  def show
    render json: set_data()
  end

  # POST /roles
  def create
    permisos = []
    permis = params[:role][:permisos]
    permis.each do | permi|
      permi[:acciones].each do |action|
        if(action[:check]!=nil && action[:check])
          permisos.push(action)
        end
      end
    end
    puts "roles params => ", :json => role_params['permisos']
    permiso_acciones = permisos
    # permiso_acciones = role_params['permisos']
    @role = Role.new(role_params.except('permisos'))
    @role.name = @role.name.upcase
    
    if @role.save
      permiso_acciones.each do |accion|
        puts "=="*60
        puts "#{role_params['permisos'].to_json}"
        puts "=="*60
        # permisos.each do |accion|
          # unless (accion[:check])
            rol_permiso_accione = RolPermisoAccione.new({role_id: @role.id, permiso_accione_id: accion['id']})
            unless rol_permiso_accione.save
              RolPermisoAccione.delete("role_id = #{@role.id}")
              @role.destroy
  
              return render json: {error: 'Rol no registrado.', status: :unprocessable_entity}, status: 401
            end
          # end
        # end
      end

      render json: {data: @role, status: :created, msg: "Rol registrado correctamente."}
    else
      render json: {error: @role.errors, status: :unprocessable_entity}, status: 401
    end
  end

  # PATCH/PUT /roles/1
  def update
    permisos = params[:role][:permisos]
    role = { 'id'=> params[:role][:id],  'name' => params[:role][:name]}
    rol = role
    rol['name'] = rol['name'].upcase
    
    if @role.update(rol)
      
      RolPermisoAccione.where({role_id: @role.id}).destroy_all
      permisos.each do |permiso|
        permiso[:acciones].each do |accion|
          puts "accion =>  ", :json => accion
          
          if accion['check']
            puts "**"*30
            puts "======== -->#{accion.to_json}"
            puts "**"*30
            rol_permiso_accione = RolPermisoAccione.new({role_id: @role.id, permiso_accione_id: accion['permiso_accione_id']})
            esta_save = rol_permiso_accione.save
            unless esta_save
              # return render json: {error: 'Accion no asociada a este Rol.', status: :unprocessable_entity}, status: 401
            end
          end

          # if accion['tipo'] == 'D'
          #   value = ActiveRecord::Base.connection.exec_query("DELETE FROM rol_permiso_acciones WHERE role_id = #{@role.id} and permiso_accione_id = #{accion['id']}")

            # unless value.length > 0
            #   return render json: {error: 'Accion asociada a este Rol, no pudo ser eliminada.', status: :unprocessable_entity}, status: 401
            # end
          # end

        end
      end

      render json: {data: @role, msg: "Rol actualizada correctamente."}
    else
      render json: {error: @role.errors, status: :unprocessable_entity}, status: 401
    end
  end

  # DELETE /roles/1
  def destroy
    # rol_permiso_acciones = RolPermisoAccione.where("role_id = #{@role.id}")
    usuarios_roles = ActiveRecord::Base.connection.exec_query("select * from usuarios_roles where role_id = #{@role.id}")

    # puts "rol destroy rol_permiso_acciones =>", :json => rol_permiso_acciones
    puts "rol destroy usuarios_roles =>", :json => usuarios_roles

    if usuarios_roles.length > 0
      render json: {msg: "otros registro dependen de este Rol, no puede ser eliminada", status: :unprocessable_entity}, status: 401
    else
      value = ActiveRecord::Base.connection.exec_query("DELETE FROM rol_permiso_acciones WHERE role_id = #{@role.id}")

      puts "eliminando....... =>", :json => value

      if @role.destroy
        render json: {msg: "Rol eliminado correctamente."}
      else
        render json: {error: "Error al eliminar el Rol"}, status: 401
      end
    end
  end

  def set_data
    lAll = false
    if params[:id].nil?
      lAll = true
      rolData = Role.all
    else
      rolData = Role.all.where("id = #{@role.id} ")
    end

    puts "all roles ..=>", :json => rolData

    role = []
    rolData.each do|rol|
      puts "=> id roles ..=>", :json => rol[:id]
      # rol_actiones = ActiveRecord::Base.connection.exec_query("select p.id, p.nombre, pa.nombre as accion from rol_permiso_acciones rpa inner join roles r on r.id = rpa.role_id inner join usuarios_roles ur on ur.role_id = r.id inner join permiso_acciones pa on pa.id = rpa.permiso_accione_id inner join permisos p on p.id = pa.permiso_id where ur.usuario_id = #{rol[:id]} and rpa.estado = 'A' group by rpa.id, p.id, pa.id")
      rol_actiones = ActiveRecord::Base.connection.exec_query("select p.id, p.nombre, p.activo from rol_permiso_acciones rpa inner join roles r on r.id = rpa.role_id inner join permiso_acciones pa on pa.id = rpa.permiso_accione_id inner join permisos p on p.id = pa.permiso_id where r.id = #{rol[:id]} and rpa.activo group by p.id")

      permisos = []
      rol_actiones.each do |action|
        aAcciones = ActiveRecord::Base.connection.exec_query("select rpa.*, pa.nombre as name from rol_permiso_acciones rpa inner join roles r on r.id = rpa.role_id inner join permiso_acciones pa on pa.id = rpa.permiso_accione_id inner join permisos p on p.id = pa.permiso_id where rpa.role_id = #{rol[:id]} and pa.permiso_id = #{action['id']} and rpa.activo group by rpa.id, pa.nombre")
        permisos.push({
          id: action['id'],
          name: action['nombre'],
          activo: action['activo'],
          acciones: aAcciones
        })
      end
      # misPermisos(permisos)

      role.push({
        id: rol[:id],
        name: rol[:name],
        descripcion: rol[:descripcion],
        # estado: rol[:estado],
        permisos: permisos
      })
    end

    return role if lAll;
    return role[0] unless lAll;
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rol
      @role = Role.find_by_id(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def role_params
      params.require(:role).permit(:name, :estado, :acciones, permisos: [:id, :tipo, :activo, :acciones])
    end
end
