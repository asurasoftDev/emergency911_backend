class TurnosVehiculosController < ApplicationController
  before_action :set_turno_vehiculo, only: [:show, :update, :destroy]

  # GET /turnos_vehiculos
  def index
    @turnos_vehiculos = TurnosVehiculo.all

    render json: @turnos_vehiculos
  end

  # GET /turnos_vehiculos/1
  def show
    render json: @turno_vehiculo
  end

  # POST /turnos_vehiculos
  def create
    if validar()
      validar(true)
    end
  end

  def validar(lCrear=false)
    turno_vehiculo_params[:turnos].each do |turno,index|
      unless lCrear
        if TurnosVehiculo.validar(turno)
          render json: {turno: Turno.find_by_id(turno), msg: "Este Turno ya esta en uso."}, status: 404
          return false
        end
      else
        turnoVehiculoTemp = TurnosVehiculo.new({
          activo: (turno_vehiculo_params[:activo] || true),
          turno_id: turno, 
          vehiculo_id: turno_vehiculo_params[:vehiculo_id]
        })

        if turnoVehiculoTemp.save
          if( turno_vehiculo_params[:turnos].length-1 == index)
            return render json: {msg: "Asignaciones de turnos a vehiculo registrada correctamente."}, status: 200
          end
        else
          return render json: {errors: turnoVehiculoTemp.errors, msg: "Error asignando turnos a vehiculo."}, status: 404
        end
      end
    end
  end

  # PATCH/PUT /turnos_vehiculos/1
  def update
    if @turno_vehiculo.update(turno_vehiculo_params)
      render json: @turno_vehiculo
    else
      render json: @turno_vehiculo.errors, status: :unprocessable_entity
    end
  end

  # DELETE /turnos_vehiculos/1
  def destroy
    @turno_vehiculo.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_turno_vehiculo
      @turno_vehiculo = TurnosVehiculo.find_by_id(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def turno_vehiculo_params
      params.require(:turno_vehiculo).permit(:vehiculo_id, :turno_id, :activo, turnos: [])
    end
end
