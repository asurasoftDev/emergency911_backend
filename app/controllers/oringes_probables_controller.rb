class OringesProbablesController < ApplicationController
  before_action :set_oringe_probable, only: [:show, :update, :destroy]

  # GET /oringes_probables
  def index
    @oringes_probables = OringesProbable.all

    render json: @oringes_probables
  end

  # GET /oringes_probables/1
  def show
    render json: @oringe_probable
  end

  # POST /oringes_probables
  def create
    @oringe_probable = OringesProbable.new(oringe_probable_params)

    if @oringe_probable.save
      render json: @oringe_probable, status: :created, location: @oringe_probable
    else
      render json: @oringe_probable.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /oringes_probables/1
  def update
    if @oringe_probable.update(oringe_probable_params)
      render json: @oringe_probable
    else
      render json: @oringe_probable.errors, status: :unprocessable_entity
    end
  end

  # DELETE /oringes_probables/1
  def destroy
    @oringe_probable.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_oringe_probable
      @oringe_probable = OringesProbable.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def oringe_probable_params
      params.require(:oringe_probable).permit(:causa_clinica_id, :ficha_clinica_id, :activo)
    end
end
