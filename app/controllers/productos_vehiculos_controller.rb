class ProductosVehiculosController < ApplicationController
  before_action :set_producto_vehiculo, only: [:show, :update, :destroy]

  # GET /productos_vehiculos
  def index
    # @productos_vehiculos = ProductosVehiculo.all
    # @productos_vehiculos = ProductosVehiculo.group(:vehiculo_id, :producto_id).sum(:cantidad)
    # @productos_vehiculos = ProductosVehiculo.includes(:vehiculo,:producto).joins(:vehiculo,:producto).group(:vehiculo_id, :producto_id)
    vehiculos = []
    Vehiculo.where({activo: true}).each do |vehiculo|
      oVehiculo = vehiculo.attributes
      oVehiculo['ProductosVehiculo'] = []
      ProductosVehiculo.select(:producto_id).where(vehiculo_id: vehiculo.id).group(:producto_id).each do |pv|
        oVehiculo['ProductosVehiculo'].push({
          cant_actual: ProductosVehiculo.where(vehiculo_id: vehiculo.id).sum(:cantidad), 
          producto: Producto.find_by_id(pv.producto_id)
        })
      end
      vehiculos.push(oVehiculo)
    end
    puts "data =>", :json => vehiculos
    render json: vehiculos
  end

  # GET /productos_vehiculos/1
  def show
    render json: @producto_vehiculo
  end


  def productos_de_mi_vehiculo
    vehiculo_asignado_id = params[:id]
    producto_v = VehiculoAsignado.find_by_id(vehiculo_asignado_id).turnos_vehiculo.vehiculo.productos_vehiculo
    productos = []
    producto_v.each do |vehiculo_p|
      product = Producto.find_by_id(vehiculo_p[:producto_id])
      productos.push(product)
    end
    render json: productos
  end

  # POST /productos_vehiculos
  def create
    vehiculo_id = producto_vehiculo_params[:vehiculo_id]
    params['producto_vehiculo']['productos'].each do |producto_vehiculo|
      producto = Producto.find_by_id(producto_vehiculo[:id])
      if producto_vehiculo[:cantidad].to_i > 0
        if producto.cantidad > 0 && producto.cantidad >= producto_vehiculo[:cantidad].to_i
          @producto_vehiculo = ProductosVehiculo.new({
            vehiculo_id: vehiculo_id, 
            producto_id: producto_vehiculo[:id], 
            cant_minima: producto_vehiculo[:cant_minima],
            cantidad: producto_vehiculo[:cantidad].to_i
          })
          unless @producto_vehiculo.save
            render json: {msg: "error asignando productos a vehiculos."}, status: 404
            return false
          end
          producto.change_cantidad((producto_vehiculo[:cantidad].to_i * -1))
          # updateProducto(@producto_vehiculo.producto_id, @producto_vehiculo.cantidad)
        else
          render json: {msg: "El producto #{producto.nombre} no tiene la cantidad solicitada."}, status: 404
          return false
        end
      end
    end
    Vehiculo.find_by_id(vehiculo_id).update({ultimo_restablecimiento_de_invetario: DateTime.now})
    render json: {msg: "Asignaciones de proudctos a vehiculo registrada correctamente."}
  end

  def updateProducto(id, cantidad)
    producto = Producto.find_by_id(id)
    if producto.cantidad > 0
      producto.update({cantidad: (producto.cantidad - cantidad.to_i )})
    end
  end

  # PATCH/PUT /productos_vehiculos/1
  def update
    if @producto_vehiculo.update(producto_vehiculo_params)
      render json: @producto_vehiculo
    else
      render json: @producto_vehiculo.errors, status: :unprocessable_entity
    end
  end

  # DELETE /productos_vehiculos/1
  def destroy
    @producto_vehiculo.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_producto_vehiculo
      @producto_vehiculo = ProductosVehiculo.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def producto_vehiculo_params
      params.require(:producto_vehiculo).permit(:vehiculo_id, :productos, :activo)
    end
end
