class EmergenciaTiposController < ApplicationController
  before_action :set_emergencia_tipo, only: [:show, :update, :destroy]

  # GET /emergencia_tipos
  def index
    @emergencia_tipos = EmergenciaTipo.all

    render json: @emergencia_tipos
  end

  # GET /emergencia_tipos/1
  def show
    render json: @emergencia_tipo
  end

  # POST /emergencia_tipos
  def create
    @emergencia_tipo = EmergenciaTipo.new(emergencia_tipo_params)

    if @emergencia_tipo.save
      render json: @emergencia_tipo, status: :created, location: @emergencia_tipo
    else
      render json: @emergencia_tipo.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /emergencia_tipos/1
  def update
    if @emergencia_tipo.update(emergencia_tipo_params)
      render json: @emergencia_tipo
    else
      render json: @emergencia_tipo.errors, status: :unprocessable_entity
    end
  end

  # DELETE /emergencia_tipos/1
  def destroy
    @emergencia_tipo.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_emergencia_tipo
      @emergencia_tipo = EmergenciaTipo.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def emergencia_tipo_params
      params.require(:emergencia_tipo).permit(:nombre, :descripcion, :activo)
    end
end
