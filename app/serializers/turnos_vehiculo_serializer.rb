class TurnosVehiculoSerializer < ActiveModel::Serializer
  attributes :id, :activo, :turno
  has_one :vehiculo
  has_one :turno
end
