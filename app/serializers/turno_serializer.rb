class TurnoSerializer < ActiveModel::Serializer
  attributes :id, :activo, :tanda, :user
  has_one :user
  # has_one :tanda
end
