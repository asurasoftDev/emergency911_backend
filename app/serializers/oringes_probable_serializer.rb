class OringesProbableSerializer < ActiveModel::Serializer
  attributes :id, :activo
  has_one :causa_clinica
  has_one :ficha_clinica
end
