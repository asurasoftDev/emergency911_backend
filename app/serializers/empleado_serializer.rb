class EmpleadoSerializer < ActiveModel::Serializer
  attributes :id, :activo
  has_one :user
  has_one :cargo
end
