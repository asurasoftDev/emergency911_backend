class PermisoAccioneSerializer < ActiveModel::Serializer
  attributes :id, :nombre, :activo
  has_one :permiso
end
