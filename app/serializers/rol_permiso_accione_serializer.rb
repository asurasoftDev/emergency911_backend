class RolPermisoAccioneSerializer < ActiveModel::Serializer
  attributes :id, :activo
  has_one :permiso
  has_one :role
end
