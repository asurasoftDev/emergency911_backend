class TandaSerializer < ActiveModel::Serializer
  attributes :id, :hora_inicio, :hora_fin, :activo, :descripcion, :dia
  has_one :cargo
  has_one :dia
end
