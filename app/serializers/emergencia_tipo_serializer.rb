class EmergenciaTipoSerializer < ActiveModel::Serializer
  attributes :id, :nombre, :descripcion, :activo
end
