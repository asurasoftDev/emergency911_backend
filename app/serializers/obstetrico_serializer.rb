class ObstetricoSerializer < ActiveModel::Serializer
  attributes :id, :trabajo_de_parto, :sangrado_vaginal, :fuente_rota, :tiempo_gestacion_en_semanas, :activo
  has_one :ficha_clinica
end
