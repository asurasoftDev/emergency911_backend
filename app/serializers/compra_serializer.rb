class CompraSerializer < ActiveModel::Serializer
  attributes :id, :costo, :cantidad, :producto
  has_one :producto
end
