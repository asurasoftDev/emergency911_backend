class PersonaSerializer < ActiveModel::Serializer
  attributes :id, :nombres, :apellidos, :cedula, :edad, :sexo, :telefono_1, :telefono_2, :numero_de_seguro, :activo, :ciudad, :user, :cargo, :username
  has_one :user
  # has_one :cargo , :through => :user

  def username
    user = User.find_by_id(object.id)
    puts "object.user => ", user.to_json
    unless user.nil?
      {username: user.nickname}
    end
  end
end
