class CausasTraumaticaSerializer < ActiveModel::Serializer
  attributes :id, :descripcion, :causado_por, :activo
  has_one :trauma
  has_one :ficha_clinica
end
