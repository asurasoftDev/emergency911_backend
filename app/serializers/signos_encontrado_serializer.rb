class SignosEncontradoSerializer < ActiveModel::Serializer
  attributes :id, :activo
  has_one :signo_vital
  has_one :ficha_clinica
end
