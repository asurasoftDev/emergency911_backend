class EmbarasoEstadoSerializer < ActiveModel::Serializer
  attributes :id, :nombre, :activo
end
