class AgentesCasualeSerializer < ActiveModel::Serializer
  attributes :id, :activo
  has_one :ficha_clinica
  has_one :causa
end
