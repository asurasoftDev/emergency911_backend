class PacientesPertenenciaSerializer < ActiveModel::Serializer
  attributes :id, :quien_recibe, :descripcion, :activo
  has_one :ficha_clinica
end
