class AntecedentesEncontradoSerializer < ActiveModel::Serializer
  attributes :id, :activo
  has_one :antecedente
  has_one :ficha_clinica
end
