class VehiculoSerializer < ActiveModel::Serializer
  attributes :id, :marca, :modelo, :placa, :anio, :alias, :ultimo_restablecimiento_de_invetario, :estado, :activo
end
