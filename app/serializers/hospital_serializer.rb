class HospitalSerializer < ActiveModel::Serializer
  attributes :id, :nombre, :tipo_publico, :activo
  has_one :ciudad
end
