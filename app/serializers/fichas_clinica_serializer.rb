class FichasClinicaSerializer < ActiveModel::Serializer
  attributes :id, :medico_hospital, :diagnostico_presuntivo, :observacion, :triage_color, :estado, :activo
  has_one :persona
  has_one :emergencia
end
