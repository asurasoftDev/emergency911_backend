class VehiculoAsignadoSerializer < ActiveModel::Serializer
  attributes :id, :activo, :ficha_clinica
  has_one :turno_vehiculo
  has_one :emergencia
end
