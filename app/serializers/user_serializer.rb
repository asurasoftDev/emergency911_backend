class UserSerializer < ActiveModel::Serializer
  attributes :id, :nickname, :email, :cargo_id, :cargo, :persona, :turnos, :tandas
  has_one :cargo
end
