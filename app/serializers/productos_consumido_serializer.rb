class ProductosConsumidoSerializer < ActiveModel::Serializer
  attributes :id, :activo
  has_one :producto_vehiculo
  has_one :vehiculo_ficha_historial
end
