class ProductosVehiculoSerializer < ActiveModel::Serializer
  attributes :id, :cant_minima, :cantidad, :activo
  has_one :vehiculo
  has_one :producto
end
