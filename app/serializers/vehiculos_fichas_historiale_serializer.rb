class VehiculosFichasHistorialeSerializer < ActiveModel::Serializer
  attributes :id, :activo
  has_one :ficha_clinica
  has_one :turno_vehiculo
end
