class CargoSerializer < ActiveModel::Serializer
  attributes :id, :nombre, :valor_del_dia_en_horas, :cant_dias_libres, :activo, :users, :tandas
end
