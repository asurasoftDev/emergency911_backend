class EstadosObstetricoSerializer < ActiveModel::Serializer
  attributes :id, :activo
  has_one :obstetrico
  has_one :embaraso_estado
end
