class EmergenciaSerializer < ActiveModel::Serializer
  attributes :id, :sector, :lugar_del_evento, :descripcion, :telefono_infomante, :activo, :codigo, :lat, :long, :vehiculo_asignados
  has_one :ciudad
  has_one :emergencia_tipo
end
