class ExploracionesResultadoSerializer < ActiveModel::Serializer
  attributes :id, :activo
  has_one :exploracion
  has_one :area_fisica
  has_one :ficha_clinica
end
