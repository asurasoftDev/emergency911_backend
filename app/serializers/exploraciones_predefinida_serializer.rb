class ExploracionesPredefinidaSerializer < ActiveModel::Serializer
  attributes :id, :activo
  has_one :area_fisica
  has_one :exploracion
end
